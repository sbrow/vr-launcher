# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://github.com/sbrow/vr-launcher-server/compare/v1.2.0...v1.3.0) (2019-07-13)


### Bug Fixes

* Server can be made ready again. ([4035f9b](https://github.com/sbrow/vr-launcher-server/commit/4035f9b))
* **assets/static.jpg:** Converted to 16x9 ratio ([1849167](https://github.com/sbrow/vr-launcher-server/commit/1849167))


### Build System

* **tsconfig.json:** Upgraded target to `es6`. ([141a5b2](https://github.com/sbrow/vr-launcher-server/commit/141a5b2))
* Added `@types/webpack`. ([c9cb390](https://github.com/sbrow/vr-launcher-server/commit/c9cb390))


### Features

* Added Status messages to video. ([bfb0bca](https://github.com/sbrow/vr-launcher-server/commit/bfb0bca))
* **StatusLine:** Added `className` prop. ([0c325cd](https://github.com/sbrow/vr-launcher-server/commit/0c325cd))
* **VideoStream:** Added background prop. ([a1f3736](https://github.com/sbrow/vr-launcher-server/commit/a1f3736))


### Tests

* **StatusLine:** Created. ([e60069f](https://github.com/sbrow/vr-launcher-server/commit/e60069f))
* Fixed brokend tests and skipped "template match" tests. ([244529d](https://github.com/sbrow/vr-launcher-server/commit/244529d))



## [1.2.0](https://github.com/sbrow/vr-launcher-server/compare/v1.0.1...v1.2.0) (2019-06-29)


### Bug Fixes

* **GameSelection:** Works again. ([edfa43a](https://github.com/sbrow/vr-launcher-server/commit/edfa43a))
* Compiler errors / Version number color. ([eb35e5a](https://github.com/sbrow/vr-launcher-server/commit/eb35e5a))
* Configured loggers not to print during tests. ([df6f219](https://github.com/sbrow/vr-launcher-server/commit/df6f219))
* PlayArea and PlayTime dropdowns now update as expected. ([d50cc89](https://github.com/sbrow/vr-launcher-server/commit/d50cc89))
* ServerApp now sends key events to client. ([035bb1e](https://github.com/sbrow/vr-launcher-server/commit/035bb1e))
* **assets:** Re-added game images. ([2cc2577](https://github.com/sbrow/vr-launcher-server/commit/2cc2577))
* **Clients:** Fixed a bug that caused `onInfo` changes to be written twice. ([b9de419](https://github.com/sbrow/vr-launcher-server/commit/b9de419))
* **ClientStream:** Passes props along. ([dbd81d5](https://github.com/sbrow/vr-launcher-server/commit/dbd81d5))
* **Connections:** Compiler errors introduced in commit (835b1e)[https://github.com/sbrow/vr-launcher ([13b037a](https://github.com/sbrow/vr-launcher-server/commit/13b037a))
* **LinkButton:** No longer redirects to unknown url. ([8bb29f4](https://github.com/sbrow/vr-launcher-server/commit/8bb29f4))
* **ServerStore:** Added missing call to `updateStreams` in `AddStream` handler. ([7d0d28f](https://github.com/sbrow/vr-launcher-server/commit/7d0d28f))
* **socket.onStream:** Only registers a callback if none has been registered previously. ([ffdddf2](https://github.com/sbrow/vr-launcher-server/commit/ffdddf2))


### Build System

* **vr-launcher-common:** Updated to `^3.4.0` ([9c7ca43](https://github.com/sbrow/vr-launcher-server/commit/9c7ca43))
* Fixing compiler errors. ([675b9ce](https://github.com/sbrow/vr-launcher-server/commit/675b9ce))
* **lint:** Updated prettier and tslint configs. ([2bd2599](https://github.com/sbrow/vr-launcher-server/commit/2bd2599))
* **scripts/lint:** matchers now cath `webpack.config.js`. ([8584e2d](https://github.com/sbrow/vr-launcher-server/commit/8584e2d))
* **vr-launcher-common:** Upgraded to 3.3.0 ([0204e25](https://github.com/sbrow/vr-launcher-server/commit/0204e25))
* Added preliminary config for `prettier`. ([4b7fd81](https://github.com/sbrow/vr-launcher-server/commit/4b7fd81))
* Compliler fixes. ([e0f3ac0](https://github.com/sbrow/vr-launcher-server/commit/e0f3ac0))
* **vr-launcher-common:** Updated to `^3.2.0` ([3c03a0a](https://github.com/sbrow/vr-launcher-server/commit/3c03a0a))
* Can now load `.jpg` files. ([3e30580](https://github.com/sbrow/vr-launcher-server/commit/3e30580))
* **upgrade:** @types/react. ([7fbb2ad](https://github.com/sbrow/vr-launcher-server/commit/7fbb2ad))
* **webpack:** Moved html template to new file. ([c996e00](https://github.com/sbrow/vr-launcher-server/commit/c996e00))
* Allowed `vr-launcher-comon` to retrieve more up-to-date versions. ([737d7bc](https://github.com/sbrow/vr-launcher-server/commit/737d7bc))
* Externalized linter configs. ([1099fa2](https://github.com/sbrow/vr-launcher-server/commit/1099fa2))
* Updated `typescript` `@types/node`. ([2eb2a07](https://github.com/sbrow/vr-launcher-server/commit/2eb2a07))
* **scripts:** Added `prettier` script and linked it to `lint` script. ([4d6eda1](https://github.com/sbrow/vr-launcher-server/commit/4d6eda1))
* **tslint:** Updated dependency. ([f1896cc](https://github.com/sbrow/vr-launcher-server/commit/f1896cc))
* **vr-launcher-common:** Updated to version `^2.0.0`. ([b9b053b](https://github.com/sbrow/vr-launcher-server/commit/b9b053b))
* **vr-launcher-common:** Upgraded to `^3.0.0`. ([a330a3a](https://github.com/sbrow/vr-launcher-server/commit/a330a3a))


### Features

* **BatteryStatus:** Displays percent ([5c917dd](https://github.com/sbrow/vr-launcher-server/commit/5c917dd))
* **Clients:** `remove`, `setActivity` and `update` now return whether or not a change occurred. ([a0b55cc](https://github.com/sbrow/vr-launcher-server/commit/a0b55cc))
* **Clients:** `setActivity`'s `active` paramater is now optional. ([408e242](https://github.com/sbrow/vr-launcher-server/commit/408e242))
* **Clients:** Added `getSocketID` which returns the key at which the given client is stored. ([30cde03](https://github.com/sbrow/vr-launcher-server/commit/30cde03))
* **Clients:** Streams are now bound directly to clients. ([91f2053](https://github.com/sbrow/vr-launcher-server/commit/91f2053))
* **ClientStream:** Now only redirects if on "/options" page. ([60e5439](https://github.com/sbrow/vr-launcher-server/commit/60e5439))
* **ClientStream:** Pings client for stream if none exists. ([c9e1782](https://github.com/sbrow/vr-launcher-server/commit/c9e1782))
* **CustomCard:** Added ClientStream ([7e9ebdb](https://github.com/sbrow/vr-launcher-server/commit/7e9ebdb))
* **CustomCard:** StatusLine matches fancy version ([5834e34](https://github.com/sbrow/vr-launcher-server/commit/5834e34))
* Server can now send keypresses to client. ([7469438](https://github.com/sbrow/vr-launcher-server/commit/7469438))
* **Actions:** Added "Disconnect" action. ([35b0d6a](https://github.com/sbrow/vr-launcher-server/commit/35b0d6a))
* **ClientStream:** Now pulls stream data by clientID. ([3e28e8a](https://github.com/sbrow/vr-launcher-server/commit/3e28e8a))
* **DesktopApp:** App that displays the desktop. ([3375df3](https://github.com/sbrow/vr-launcher-server/commit/3375df3))
* **DesktopApp:** Now connected to ServerApp. ([0907677](https://github.com/sbrow/vr-launcher-server/commit/0907677))
* **DesktopApp:** Now pulls stream from client. ([e05e1ac](https://github.com/sbrow/vr-launcher-server/commit/e05e1ac))
* **DesktopApp:** Streams are now stored in a temporary cache until matched with their client. ([6a1d7ac](https://github.com/sbrow/vr-launcher-server/commit/6a1d7ac))
* **DesktopCapturer:** Correctly reads server port number. ([4334d42](https://github.com/sbrow/vr-launcher-server/commit/4334d42))
* **LinkButton:** Now accepts children. ([3e89ea9](https://github.com/sbrow/vr-launcher-server/commit/3e89ea9))
* **LinkButton:** Size is now configurable. ([76c4169](https://github.com/sbrow/vr-launcher-server/commit/76c4169))
* **Nicknames:** set now returns whether a nickname changed. ([a9d93ce](https://github.com/sbrow/vr-launcher-server/commit/a9d93ce))
* **Server:** Removed "clients" namespace. ([6c04498](https://github.com/sbrow/vr-launcher-server/commit/6c04498))
* **ServerStore:** Now updates clients incrementily. ([0b303d0](https://github.com/sbrow/vr-launcher-server/commit/0b303d0))
* **Theme:** Installed themes from npm. ([3ea366c](https://github.com/sbrow/vr-launcher-server/commit/3ea366c))
* **VideoStream:** Includes bootstrap "responsive embed" (16x9). ([4b74ac7](https://github.com/sbrow/vr-launcher-server/commit/4b74ac7))
* **VideoStream:** Now accepts poster prop. ([e618adf](https://github.com/sbrow/vr-launcher-server/commit/e618adf))
* NoVNC working. ([749b359](https://github.com/sbrow/vr-launcher-server/commit/749b359))
* Server now recieves video from client. ([121b8ce](https://github.com/sbrow/vr-launcher-server/commit/121b8ce))
* User can now click on a video to make it full screen. ([b6a75e8](https://github.com/sbrow/vr-launcher-server/commit/b6a75e8))
* **VideoStream:** Added black background. ([f522ff0](https://github.com/sbrow/vr-launcher-server/commit/f522ff0))
* Implemented async compononent loading! ([ea125e9](https://github.com/sbrow/vr-launcher-server/commit/ea125e9))


### Tests

* **OptionsStore:** Added. ([f47229d](https://github.com/sbrow/vr-launcher-server/commit/f47229d))
* **PlayAreaDropdown:** got a valid `select` test. ([c7ece76](https://github.com/sbrow/vr-launcher-server/commit/c7ece76))
* **PlayTimeDropdown:** Added ([5741a22](https://github.com/sbrow/vr-launcher-server/commit/5741a22))
* **ServerStore:** added `setPlayTime` test. ([3706598](https://github.com/sbrow/vr-launcher-server/commit/3706598))
* Added `reducer.Bail`, `Clients.remove`, fixed `reducer.SetClientActive`. ([6258327](https://github.com/sbrow/vr-launcher-server/commit/6258327))
* Added `TestRender`, `TestRouter` and `TestServerProvider`. ([4506be7](https://github.com/sbrow/vr-launcher-server/commit/4506be7))
* Added render tests to compare with provided template. ([eda0b46](https://github.com/sbrow/vr-launcher-server/commit/eda0b46))
* Improvements. ([369e9cc](https://github.com/sbrow/vr-launcher-server/commit/369e9cc))
* Moved unit tests beside their definitions. ([74c6e05](https://github.com/sbrow/vr-launcher-server/commit/74c6e05))
* Updated frontend. ([0550dd0](https://github.com/sbrow/vr-launcher-server/commit/0550dd0))
* **Clients:** Added 'getSocketID' test. ([722ee06](https://github.com/sbrow/vr-launcher-server/commit/722ee06))
* **Clients:** Re-enabled "no-relative-imports". ([9b201ff](https://github.com/sbrow/vr-launcher-server/commit/9b201ff))
* **ClientStream:** Created test. ([4877ad6](https://github.com/sbrow/vr-launcher-server/commit/4877ad6))
* **DesktopApp:** Skipping test instead of forcing failure. ([4c171c8](https://github.com/sbrow/vr-launcher-server/commit/4c171c8))
* **DesktopApp:** WIP- Wrote most of test for clientID check. ([4a7d8db](https://github.com/sbrow/vr-launcher-server/commit/4a7d8db))
* **ServerAppStore:** Added "UpdateGames" handler test. ([6d564a4](https://github.com/sbrow/vr-launcher-server/commit/6d564a4))
* **ServerAppStore:** Added tests for "SetClientActive" and "SetNickname". ([2f40727](https://github.com/sbrow/vr-launcher-server/commit/2f40727))
* **ServerAppStore:** Updated `SetClientActive`. ([f571e17](https://github.com/sbrow/vr-launcher-server/commit/f571e17))
* **ServerStore:** Added tests for `AddStream`. ([3ad9849](https://github.com/sbrow/vr-launcher-server/commit/3ad9849))
* **ServerStore:** Reorganized "describe" blocks. ([9866cbc](https://github.com/sbrow/vr-launcher-server/commit/9866cbc))
* Fixed issues with test build. ([554bcbe](https://github.com/sbrow/vr-launcher-server/commit/554bcbe))
* Jest now collects coverage. ([f82fa2d](https://github.com/sbrow/vr-launcher-server/commit/f82fa2d))



## [1.1.0](https://github.com/sbrow/vr-launcher-server/compare/v1.0.1...v1.1.0) (2019-06-19)


### Bug Fixes

* **assets:** Re-added game images. ([2cc2577](https://github.com/sbrow/vr-launcher-server/commit/2cc2577))
* **Clients:** Fixed a bug that caused `onInfo` changes to be written twice. ([b9de419](https://github.com/sbrow/vr-launcher-server/commit/b9de419))
* **Connections:** Compiler errors introduced in commit (835b1e)[https://github.com/sbrow/vr-launcher ([13b037a](https://github.com/sbrow/vr-launcher-server/commit/13b037a))
* **socket.onStream:** Only registers a callback if none has been registered previously. ([ffdddf2](https://github.com/sbrow/vr-launcher-server/commit/ffdddf2))


### Build System

* **vr-launcher-common:** Updated to version `^2.0.0`. ([b9b053b](https://github.com/sbrow/vr-launcher-server/commit/b9b053b))
* **vr-launcher-common:** Upgraded to `^3.0.0`. ([a330a3a](https://github.com/sbrow/vr-launcher-server/commit/a330a3a))


### Features

* **CustomCard:** Added ClientStream ([7e9ebdb](https://github.com/sbrow/vr-launcher-server/commit/7e9ebdb))
* Implemented async compononent loading! ([ea125e9](https://github.com/sbrow/vr-launcher-server/commit/ea125e9))
* **Actions:** Added "Disconnect" action. ([35b0d6a](https://github.com/sbrow/vr-launcher-server/commit/35b0d6a))
* **Clients:** `setActivity`'s `active` paramater is now optional. ([408e242](https://github.com/sbrow/vr-launcher-server/commit/408e242))
* **Clients:** Added `getSocketID` which returns the key at which the given client is stored. ([30cde03](https://github.com/sbrow/vr-launcher-server/commit/30cde03))
* **Clients:** Streams are now bound directly to clients. ([91f2053](https://github.com/sbrow/vr-launcher-server/commit/91f2053))
* **ClientStream:** Now pulls stream data by clientID. ([3e28e8a](https://github.com/sbrow/vr-launcher-server/commit/3e28e8a))
* **ClientStream:** Pings client for stream if none exists. ([c9e1782](https://github.com/sbrow/vr-launcher-server/commit/c9e1782))
* **DesktopApp:** App that displays the desktop. ([3375df3](https://github.com/sbrow/vr-launcher-server/commit/3375df3))
* **DesktopApp:** Now pulls stream from client. ([e05e1ac](https://github.com/sbrow/vr-launcher-server/commit/e05e1ac))
* **DesktopApp:** Streams are now stored in a temporary cache until matched with their client. ([6a1d7ac](https://github.com/sbrow/vr-launcher-server/commit/6a1d7ac))
* **DesktopCapturer:** Correctly reads server port number. ([4334d42](https://github.com/sbrow/vr-launcher-server/commit/4334d42))
* **Server:** Removed "clients" namespace. ([6c04498](https://github.com/sbrow/vr-launcher-server/commit/6c04498))
* **ServerStore:** Now updates clients incrementily. ([0b303d0](https://github.com/sbrow/vr-launcher-server/commit/0b303d0))
* **VideoStream:** Includes bootstrap "responsive embed" (16x9). ([4b74ac7](https://github.com/sbrow/vr-launcher-server/commit/4b74ac7))
* NoVNC working. ([749b359](https://github.com/sbrow/vr-launcher-server/commit/749b359))
* Server now recieves video from client. ([121b8ce](https://github.com/sbrow/vr-launcher-server/commit/121b8ce))
* **Theme:** Installed themes from npm. ([3ea366c](https://github.com/sbrow/vr-launcher-server/commit/3ea366c))


### Tests

* **Clients:** Added 'getSocketID' test. ([722ee06](https://github.com/sbrow/vr-launcher-server/commit/722ee06))
* **ServerAppStore:** Added "UpdateGames" handler test. ([6d564a4](https://github.com/sbrow/vr-launcher-server/commit/6d564a4))
* **ServerAppStore:** Added tests for "SetClientActive" and "SetNickname". ([2f40727](https://github.com/sbrow/vr-launcher-server/commit/2f40727))
* **ServerAppStore:** Updated `SetClientActive`. ([f571e17](https://github.com/sbrow/vr-launcher-server/commit/f571e17))
* Fixed issues with test build. ([554bcbe](https://github.com/sbrow/vr-launcher-server/commit/554bcbe))



### [1.0.1](https://github.com/sbrow/vr-launcher-server/compare/v1.0.0...v1.0.1) (2019-06-12)


### Bug Fixes

* Pings client when they connect. ([30935f8](https://github.com/sbrow/vr-launcher-server/commit/30935f8))
* **CustomCard:** Fixed status ([d35c994](https://github.com/sbrow/vr-launcher-server/commit/d35c994))



## [1.0.0](https://github.com/sbrow/vr-launcher-server/compare/v0.24.0...v1.0.0) (2019-06-12)


### Bug Fixes

* Renamed 'pub' script to 'release. ([862f0bc](https://github.com/sbrow/vr-launcher-server/commit/862f0bc))
* Reverted application name. ([9a0359c](https://github.com/sbrow/vr-launcher-server/commit/9a0359c))
* **package.json:** Typo in "repository" ([37d310b](https://github.com/sbrow/vr-launcher-server/commit/37d310b))
* **scripts:** Build now works properly. ([56b3e3c](https://github.com/sbrow/vr-launcher-server/commit/56b3e3c))
* **scripts/start:** Updated to work on windows. ([74ea6ef](https://github.com/sbrow/vr-launcher-server/commit/74ea6ef))


### Build System

* Updated `vr-launcher-common`. ([2df2a34](https://github.com/sbrow/vr-launcher-server/commit/2df2a34))
* **bin/make.js:** Creates 'app' directory if not exists. ([1913275](https://github.com/sbrow/vr-launcher-server/commit/1913275))
* **electron-builder:** Removed BinTray provider. ([f8607ee](https://github.com/sbrow/vr-launcher-server/commit/f8607ee))
* **package.json:** Added "bin" to "directories" ([9e0b8ee](https://github.com/sbrow/vr-launcher-server/commit/9e0b8ee))
* Added 'cz-conventional-changelog' ([8f3d51d](https://github.com/sbrow/vr-launcher-server/commit/8f3d51d))


### Features

* **package.json:** Made private. ([c5b1aa1](https://github.com/sbrow/vr-launcher-server/commit/c5b1aa1))
* **scripts:** Added preversion script ([6996670](https://github.com/sbrow/vr-launcher-server/commit/6996670))
* Added 'gameOptions.json' to `.gitignore`, `scripts/clean` ([d21b6c2](https://github.com/sbrow/vr-launcher-server/commit/d21b6c2))
