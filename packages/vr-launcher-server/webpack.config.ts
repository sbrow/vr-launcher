// tslint:disable no-console

import Chalk from "chalk";
import CopyPlugin from "copy-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { join, resolve } from "path";
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import { Configuration, EnvironmentPlugin } from "webpack";
import nodeExternals from "webpack-node-externals";

process.env.NODE_ENV = process.env.NODE_ENV || "development";
const mode = process.env.NODE_ENV;
const outDir = resolve(__dirname, "app");

console.log(`NODE_ENV=${Chalk.yellow(process.env.NODE_ENV)}`);
console.log(`Building for ${Chalk.yellow(mode)}...`);

/**
 * Creates the package.json file for `app/`.
 */
function transformPackageJson(content: Buffer, path: string): string {
    const {
        name,
        version,
        author,
        description,
        repository,
        dependencies,
    } = JSON.parse(content.toString());

    const shortName = name.replace("vr-launcher-", "");
    const transformed = {
        name: shortName,
        author,
        version,
        description,
        repository,
        main: "electron.js",
        dependencies,
    };

    return JSON.stringify(transformed, null, 2);
}

const baseConfig: Configuration = {
    // @ts-ignore
    mode,
    node: {
        __dirname: false,
    },
    module: {
        rules: [
            {
                exclude: [/app/, /build/, /dist/, /node_modules/],
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    reportFiles:
                        mode === "development"
                            ? "!**/*.test.{ts,tsx}"
                            : "!**/*.{ts,tsx}",
                    onlyCompileBundledFiles: true,
                },
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            includePaths: ["node_modules", "../node_modules"],
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"],
            },
        ],
    },
    plugins: [
        new EnvironmentPlugin(["NODE_ENV"]),
        new CopyPlugin([
            { from: "package.json", transform: transformPackageJson },
            "games.yml",
        ]),
    ],
    resolve: {
        plugins: [new TsconfigPathsPlugin()],
        extensions: [".js", ".ts", ".tsx", ".jsx", ".json"],
    },
    output: {
        path: outDir,
        filename: "[name].js",
    },
};

const configs: Configuration[] = [
    {
        ...baseConfig,
        target: "electron-main",
        entry: {
            electron: join(__dirname, "src", "backend", "electron-main.ts"),
        },
        externals: [
            nodeExternals({
                whitelist: /^(?!socket\.io(-client)?$).*$/,
            }),
        ],
    },
    {
        ...baseConfig,
        target: "electron-renderer",
        entry: { main: join(__dirname, "src", "frontend", "index.tsx") },
        plugins: [
            new HtmlWebpackPlugin({
                filename: "index.html",
                template: "index.template.html",
            }),
        ],
    },
];

export default configs;
