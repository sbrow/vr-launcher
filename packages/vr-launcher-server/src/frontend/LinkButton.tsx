import Octicon, { Icon } from "@githubprimer/octicons-react";
import { History, Location } from "history";
import React, { ReactChild, ReactChildren } from "react";
import { Button } from "react-bootstrap";
import { ButtonProps } from "react-bootstrap";
import { match, withRouter } from "react-router";
import { RedirectTo } from "types";

export interface LinkButtonProps {
    to?: RedirectTo;
    match: match;
    location: Location;
    history: History;
    size?: ButtonProps["size"];
    icon?: Icon;
    children?: ReactChild | ReactChildren;
}

function LnButton(props: LinkButtonProps) {
    const render = props.icon ? <Octicon icon={props.icon} /> : <></>;
    const to =
        typeof props.to === "object"
            ? `${props.to.pathname}?${props.to.search}`
            : props.to;

    function onClick() {
        props.history.push("/");
    }

    return (
        <Button
            onClick={onClick}
            role="link"
            aria-label="home"
            size={props.size}
        >
            {render}
            {props.children}
        </Button>
    );
}

export const LinkButton = withRouter(LnButton);
