import React, {
    createContext,
    Dispatch,
    ReactElement,
    useReducer,
} from "react";

import { Logger } from "backend/Logger";
import { RedirectAction } from "frontend/actions";
import { RedirectTo } from "types";

export type GameSelectionAction = RedirectAction;

export class GameSelectionStore {
    public game: string;
    public redirect: RedirectTo | undefined;
    constructor(props: { game: string; redirect?: RedirectTo } = { game: "" }) {
        this.game = props.game;
        this.redirect = props.redirect;
    }
}

export function reducer(
    state: GameSelectionStore,
    action: GameSelectionAction,
): GameSelectionStore {
    const newState = new GameSelectionStore({ ...state });
    if (action instanceof RedirectAction) {
        if (typeof action.to === "string" && !action.to.match(/^\//)) {
            Logger.error(`"${action.to}" is not a valid redirect.`);
        }
        newState.redirect = action.to;
    }
    return newState;
}

export const GameSelectionContext = createContext<
    [GameSelectionStore, Dispatch<GameSelectionAction>]
>([
    new GameSelectionStore(),
    () => {
        throw new Error("No reducer has been set.");
    },
]);

export function GameSelectionProvider(props: { children: any }): ReactElement {
    const [state, dispatch] = useReducer(reducer, new GameSelectionStore());
    return (
        <GameSelectionContext.Provider value={[state, dispatch]}>
            {props.children}
        </GameSelectionContext.Provider>
    );
}
