import React, { ReactElement, useContext } from "react";
import { Card, CardDeck, Carousel, Col, Container, Row } from "react-bootstrap";
import { Redirect } from "react-router";

import attackOnKingsLanding from "assets/attackOnKingsLanding.png";
import zombieMadness from "assets/zombieMadness.png";
import { RedirectAction } from "frontend/actions";
import {
    GameSelectionContext,
    GameSelectionProvider,
} from "frontend/GameSelection/GameSelectionStore";

export function GameSelectionApp(): ReactElement {
    return (
        <GameSelectionProvider>
            <GameSelection />
        </GameSelectionProvider>
    );
}

function GameSelection(): ReactElement {
    const [state, dispatch] = useContext(GameSelectionContext);
    const source = GameSelection.name;

    if (state.redirect) {
        return <Redirect push to={state.redirect} />;
    }

    return (
        <Container>
            <Row className="align-items-center h-100">
                <Col>
                    <GameCarousel>
                        <Game src={zombieMadness} name="Zombie Madness" />
                        <Game
                            src={attackOnKingsLanding}
                            name="Attack on King's Landing"
                        />
                    </GameCarousel>
                </Col>
            </Row>
        </Container>
    );
}

function GameCarousel(props: { children: ReactElement[] }) {
    const pageSize = 3;
    const controls = props.children.length / pageSize > 1;
    const indicators = controls;

    return (
        <Carousel
            controls={controls}
            keyboard={true}
            interval={undefined}
            indicators={indicators}
        >
            <Carousel.Item>
                <CardDeck className="text-white">{...props.children}</CardDeck>
            </Carousel.Item>
        </Carousel>
    );
}

function Game(props: {
    src?: string;
    name: string;
    text?: string;
}): ReactElement {
    const [state, dispatch] = useContext(GameSelectionContext);
    const title = props.src ? undefined : props.name;
    let source = Game.name;

    function onClick() {
        source = `${source}.${onClick.name}`;
        dispatch(
            new RedirectAction({
                to: {
                    pathname: "/options",
                    search: new URLSearchParams({
                        game: props.name,
                    }).toString(),
                },
                source,
            }),
        );
    }

    return (
        <Card onClick={onClick}>
            <Card.Img src={props.src} />
            <Card.ImgOverlay>
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text>{props.text}</Card.Text>
                </Card.Body>
            </Card.ImgOverlay>
        </Card>
    );
}
