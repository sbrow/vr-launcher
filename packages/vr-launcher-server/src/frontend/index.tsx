import React from "react";
import ReactDOM from "react-dom";

import "frontend/index.scss";
import { Router } from "frontend/Router";

ReactDOM.render(<Router />, document.querySelector("#root"));
