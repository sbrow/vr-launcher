import React, { ReactElement, ReactNode } from "react";

import { ClientStream } from "frontend/DesktopCapturer/ClientStream";

export function FancyCard(): ReactElement {
    return (
        <div className="cut-corner mb-sm-3 col-md-5">
            <div className="border-bottom border-custom row">
                <div className="col-md-12">
                    <h2 className="h6 text-center py-2 my-0">
                        <span className="text-success">connected</span>
                    </h2>
                </div>
            </div>
            <div className="pt-3 row">
                <div className="text-center col-lg-8 col-md-12 col-sm-12 col-12">
                    <ClientStream id="1" />
                </div>
                <div className="border-bottom border-custom pt-3 pt-lg-0 text-center text-white col-lg-4 col-md-12 col-sm-12 col-12">
                    <span className="fa-stack fa-2x">
                        <i className="fa fa-circle-thin accent fa-stack-2x"></i>
                        <i
                            className="fa fa-stack-1x fa-inverse"
                            style={{
                                fontFamily: "inherit",
                                fontWeight: "bold",
                            }}
                        >
                            1
                        </i>
                    </span>
                    <div className="clearfix pt-xl-2 pt-lg-2 pt-md-2"></div>
                    <i className="fa fa-battery-2 accent"></i>
                    <span className="percentage accent">93%</span>
                </div>
            </div>
            <div className="row">
                <div className="col-md-1"></div>
                <div className="text-center text-white py-2 col-md-10">
                    <form>
                        <label htmlFor="player-one">Player Name</label>
                        <div className="clearfix"></div>
                        <input className="border custom-border text-center field-background form-control" />
                    </form>
                </div>
                <div className="col-md-1"></div>
            </div>
            <div className="clearfix"></div>
        </div>
    );
}

export function FancyConnections(props: { card: any }): ReactElement {
    return (
        <div className="col-md-8">
            <div className="row">
                {/*
                <div className="col-md-12 border border-custom text-white text-center py-1">
                    Server: <span className="text-danger">disconnected</span>
                </div>
                */}
                <props.card />
            </div>
            <div className="d-sm-flex content-stretch my-3 row">
                <props.card />
                <div className="col-md-2"></div>
                <props.card />
            </div>
            <div className="d-sm-flex content-stretch my-3 row">
                <props.card />
                <div className="col-md-2"></div>
                <props.card />
            </div>
        </div>
    );
}

export function FancyPlayAreaDropdown(): ReactElement {
    return (
        <div className="d-flex align-items-center row">
            <div className="text-md-center text-right col-md-4 col-sm-4 col-4">
                <i className="fa fa-globe fa-fw fa-2x text-white"></i>
            </div>
            <div className="text-left col-md-6 col-sm-6 col-6">
                <select className="custom-select">
                    <option selected>3</option>
                    <option>3</option>
                    <option>5</option>
                    <option>7</option>
                    <option>10</option>
                </select>
            </div>
        </div>
    );
}

export function FancyApp(props: { card: any }): ReactElement {
    return <FancyConnections {...props} />;
}
