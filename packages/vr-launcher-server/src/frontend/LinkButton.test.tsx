import { cleanup, fireEvent } from "@testing-library/react";
import React, { Dispatch } from "react";

import { Home } from "@githubprimer/octicons-react";
import { Client } from "frontend/Clients";
import { LinkButton } from "frontend/LinkButton";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import { createMemoryHistory } from "history";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;
let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    state = new ServerStore();
    dispatch = jest.fn();
    client = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "127.0.0.1",
        id: "1",
        status: "connected",
    };
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <LinkButton to="/" />,
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: { state, dispatch },
    };
    state.clients.set(someSocketID, client);
});

afterEach(() => {
    cleanup();
});
describe("ClientStream", () => {
    it("renders icon", () => {
        props.child = () => <LinkButton to="/" icon={Home} />;
        const { getByRole } = TestRender(TestRouter(props), options);
        expect(getByRole("img")).toMatchInlineSnapshot(`
            <svg
              aria-hidden="true"
              class="octicon"
              height="16"
              role="img"
              style="display: inline-block; fill: currentColor; user-select: none; vertical-align: text-bottom;"
              viewBox="0 0 16 16"
              width="16"
            >
              <path
                d="M16 9l-3-3V2h-2v2L8 1 0 9h2l1 5c0 .55.45 1 1 1h8c.55 0 1-.45 1-1l1-5h2zm-4 5H9v-4H7v4H4L2.81 7.69 8 2.5l5.19 5.19L12 14z"
                fill-rule="evenodd"
              />
            </svg>
        `);
    });
    describe("onClick", () => {
        it("redirects to props.to", () => {
            const { getByRole } = TestRender(TestRouter(props), options);

            fireEvent.click(getByRole("link"));
            expect(props.history).toBeDefined();
            expect((props.history as History).location.pathname).toBe("/");
        });
    });
});
