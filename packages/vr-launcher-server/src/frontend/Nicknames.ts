import { ClientID } from "vr-launcher-common";

/**
 * Any numeric `ClientID`.
 */
export type Nickable = Exclude<ClientID, "SERVER">;

export class Nicknames {
    public one: string;
    public two: string;
    public three: string;
    public four: string;

    constructor(
        one = "Player 1",
        two = "Player 2",
        three = "Player 3",
        four = "Player 4",
    ) {
        this.one = one;
        this.two = two;
        this.three = three;
        this.four = four;
    }

    public set(data: { key: Nickable; value: string }): boolean {
        const { key, value } = data;

        switch (key) {
            case "1":
                if (this.one !== value) {
                    this.one = value;
                    return true;
                }
            case "2":
                if (this.two !== value) {
                    this.two = value;
                    return true;
                }
            case "3":
                if (this.three !== value) {
                    this.three = value;
                    return true;
                }
            case "4":
                if (this.four !== value) {
                    this.four = value;
                    return true;
                }
        }
        return false;
    }

    public get(id: ClientID): string | undefined {
        switch (id) {
            case "1":
                return this.one;
            case "2":
                return this.two;
            case "3":
                return this.three;
            case "4":
                return this.four;
            case "SERVER":
            default:
                return undefined;
        }
    }
}
