import { Logger } from "backend/Logger";
import { getAvailableStreams } from "frontend/ServerApp/streams";
import { ClientID, Info } from "vr-launcher-common";

export interface Client extends Info {
    name?: string;
    active?: boolean;
    stream?: MediaStream;
}

export interface ClientStream {
    socketID: string;
    streamID: string;
}

export class Clients {
    public keys: { [socketID: string]: Client };

    constructor(props: { [key: string]: Client } = {}) {
        this.keys = {};
        for (const key of Object.keys(props)) {
            this.keys[key] = props[key];
        }
    }

    /**
     * `delete`s the client with the given `socketID`.
     *
     * @param socketID The id of the client to remove.
     * @returns Whether or not a client was removed.
     */
    public remove(socketID: string): boolean {
        if (socketID in this.keys) {
            delete this.keys[socketID];
            return true;
        }
        return false;
    }

    public get(id: ClientID): Client | undefined {
        for (const key of Object.keys(this.keys)) {
            const value = this.keys[key];
            if (value.id === id) {
                return value;
            }
        }
        return undefined;
    }

    /**
     * @returns The `socketID` of the client with `ClientID` id.
     */
    public getSocketID(id: ClientID): string | undefined {
        for (const key of Object.keys(this.keys)) {
            const value = this.keys[key];
            if (value.id === id) {
                return key;
            }
        }
        return undefined;
    }

    public getActive(): ClientID[] {
        const activeClients: ClientID[] = [];
        for (const key of Object.keys(this.keys)) {
            const client = this.keys[key];
            if (client.active === true) {
                activeClients.push(client.id);
            }
        }
        return activeClients;
    }

    public getRunning(): ClientID[] {
        const running: ClientID[] = [];
        for (const key of Object.keys(this.keys)) {
            const client = this.keys[key];
            if (client.status === "running") {
                running.push(client.id);
            }
        }
        return running;
    }

    public set(socketID: string, value: Client) {
        this.keys[socketID] = value;
        this.keys[socketID].active =
            this.keys[socketID].active ||
            this.keys[socketID].status === "running";
    }

    /**
     * Like `set`, but instead of replacing the client,
     * it merges the client with `value`.
     *
     * @returns Whether or not the client changed.
     */
    public update(socketID: string, value: Partial<Client>): boolean {
        let changed = false;
        if (!(socketID in this.keys)) {
            if (
                value.localIP !== undefined &&
                value.id !== undefined &&
                value.status !== undefined
            ) {
                const { localIP, id, status } = value;
                const newClient: Client = {
                    ...value,
                    // tslint:disable-next-line: object-literal-sort-keys
                    localIP,
                    id,
                    status,
                    name: "",
                };

                this.keys[socketID] = newClient;
                changed = true;
            } else {
                Logger.error(
                    `"${JSON.stringify(value)}" is not a valid Client.`,
                );
                return false;
            }
        } else {
            const keys = Object.keys(value) as Array<keyof Client>;
            for (const k of keys) {
                const v = value[k];
                const client = this.keys[socketID];
                if (client[k] !== v) {
                    // @ts-ignore
                    client[k] = v;
                    changed = true;
                }
            }
        }
        if (this.keys[socketID].active === undefined) {
            this.keys[socketID].active =
                this.keys[socketID].status === "running";
            changed = true;
        }
        return changed;
    }

    public setActivity(id: ClientID, active?: boolean): boolean {
        for (const key of Object.keys(this.keys)) {
            if (this.keys[key].id === id) {
                if (this.keys[key].active !== active) {
                    this.keys[key].active = active || !this.keys[key].active;
                    return true;
                }
                break;
            }
        }
        return false;
    }

    public equals(object: Clients) {
        return JSON.stringify(this.keys) === JSON.stringify(object.keys);
    }

    /**
     * Looks through all the keys of `streams` and returns a list
     * of clients to bind.
     *
     * @returns An array of clients and streams to bind together.
     */
    public updateStreams(): ClientStream[] {
        const source = `${Clients.name}.${this.updateStreams.name}`;
        const streams: ClientStream[] = [];
        for (const socketID of Object.keys(this.keys)) {
            if (this.keys[socketID].stream !== undefined) {
                break;
            }
            for (const streamID of getAvailableStreams()) {
                if (this.keys[socketID].streamID === streamID) {
                    streams.push({ socketID, streamID });
                    break;
                }
            }
        }
        return streams;
    }
}
