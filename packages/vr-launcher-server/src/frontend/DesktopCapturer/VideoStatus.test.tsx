import { cleanup, render, RenderResult } from "@testing-library/react";
import { VideoStatus } from "frontend/DesktopCapturer/VideoStatus";
import "@testing-library/jest-dom/extend-expect";
import React from "react";

describe(VideoStatus.name, () => {
    let container: RenderResult["container"];
    let getByText: RenderResult["getByText"];

    const tests: Array<[boolean, string, string]> = [
        [true, "Online", "text-success"],
        [false, "Offline", "text-danger"],
    ];
    for (const [online, text, color] of tests) {
        describe(`props.online = ${online}`, () => {
            beforeEach(() => {
                ({ container, getByText } = render(VideoStatus({ online })));
            });
            afterEach(cleanup);
            it(`has text "${text}"`, () => {
                const element = container.firstChild;
                expect(element).not.toBeNull();
                expect(element).toHaveTextContent(text);
            });
            it(`has color "${color}"`, () => {
                const span = getByText(text);
                expect(span).toBeTruthy();
                expect(span).toHaveClass(color);
            });
        });
    }
});
