import { cleanup, fireEvent } from "@testing-library/react";
import { Dispatch } from "react";

import { Client } from "frontend/Clients";
import { DesktopApp } from "frontend/DesktopCapturer/DesktopApp";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import { createMemoryHistory } from "history";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";
import { Info } from "vr-launcher-common";

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;

beforeEach(() => {
    const id = "1";
    state = new ServerStore();
    dispatch = jest.fn();
    client = {
        localIP: "127.0.0.1",
        id,
        status: "connected",
    } as Info;
    state.clients.set(someSocketID, client);
});

afterEach(() => {
    cleanup();
    jest.clearAllMocks();
});

describe("DesktopApp", () => {
    describe("onClick", () => {
        // it("should ");
        it.only("should have clientID = 1", async () => {
            const props: TestRouterProps = {
                path: "/options/viewer",
                history: createMemoryHistory({ initialEntries: ["/"] }),
                child: DesktopApp,
            };
            const options: TestRenderOptions = {
                // tslint:disable-next-line: object-literal-sort-keys
                wrapper: TestContextProvider,
                wrapperArgs: { state, dispatch },
            };
            const { getByText } = TestRender(TestRouter(props), options);

            expect(props.history).toBeDefined();
            expect(props.history.location).toBeDefined();
            const prev = props.history.location.pathname as string;
            fireEvent.click(getByText("×"));
            const current = props.history.location.pathname as string;
            expect(current).not.toBe(prev);

            // @todo Implement way to see which clientID is active.
        });
    });
});
