import React, { ReactElement } from "react";

interface VideoStatusProps {
    online: boolean;

    className?: string;
}

export function VideoStatus(props: VideoStatusProps): ReactElement {
    const [status, color] = props.online
        ? ["Online", "success"]
        : ["Offline", "danger"];
    return (
        <h2>
            <span className={`text-${color} text-shadow`}>{status}</span>
        </h2>
    );
}
