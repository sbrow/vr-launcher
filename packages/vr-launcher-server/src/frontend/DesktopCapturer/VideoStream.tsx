import React, { ReactElement } from "react";
export interface VideoStreamProps {
    className?: string;
    stream?: MediaStream;
    poster?: string;
    /**
     * Whether or not to allow elements to be rendered on top of the video.
     */
    background?: boolean;
    onClick?: (event: React.MouseEvent<HTMLVideoElement, MouseEvent>) => void;
}

/**
 * A stateless `<video/>` element that plays the given stream.
 *
 * @label stateless
 * @param props
 * @returns
 */
export function VideoStream(props: VideoStreamProps): ReactElement {
    const className = `${props.className ||
        ""} embed-responsive-item bg-black`.trimLeft();
    function setVideo(video: HTMLVideoElement | null): void {
        if (video) {
            if (video.srcObject !== props.stream) {
                if (props.stream === undefined) {
                    video.srcObject = null;
                } else {
                    video.srcObject = props.stream;
                }
            }
        }
    }

    const wrapperClasses = `embed-responsive embed-responsive-16by9 ${
        props.background ? "background" : ""
    }`.trimRight();
    return (
        <div className={wrapperClasses} aria-hidden={true}>
            <video
                autoPlay
                ref={setVideo}
                className={className}
                onClick={props.onClick}
                poster={props.poster}
                role="none"
            />
        </div>
    );
}
