import React, { ReactElement, useContext, useEffect } from "react";
import { Button, Container } from "react-bootstrap";
import { ClientID } from "vr-launcher-common";

import ClientStream from "frontend/DesktopCapturer/ClientStream";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { EmitSendKeys } from "frontend/ServerApp/socket";
import { History, Location } from "history";
import { withRouter } from "react-router";

export function DesktopApp(props: {
    location: Location;
    history: History;
}): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    let source = DesktopApp.name;

    function onClick() {
        source = `${source}.onClick`;
        props.history.push("/options");
    }

    const idParam = new URLSearchParams(props.location.search).get("clientId");
    let id: ClientID = "1";
    switch (idParam) {
        case "1":
        case "2":
        case "3":
        case "4":
        case "SERVER":
            id = idParam;
    }
    const socketID = state.clients.getSocketID(id);

    useEffect(() => {
        const listener = (ev: KeyboardEvent) => {
            if (ev.key && ev.key.match(/[1-4]/)) {
                EmitSendKeys(ev.key, socketID);
            }
        };

        window.addEventListener("keyup", listener);
        return () => window.removeEventListener("keyup", listener);
    });

    return (
        <Container fluid>
            <Button
                className="close text-white"
                aria-label="close"
                onClick={onClick}
            >
                <span>&times;</span>
            </Button>
            <ClientStream id={id} />
        </Container>
    );
}

export default withRouter(DesktopApp);
