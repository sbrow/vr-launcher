import { History } from "history";
import React, { ReactElement, useContext, useEffect } from "react";
import { match, withRouter } from "react-router";
import { ClientID } from "vr-launcher-common";

import { VideoStream } from "frontend/DesktopCapturer/VideoStream";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { EmitGetStream } from "frontend/ServerApp/socket";

export interface ClientStreamProps {
    id: ClientID;
    location: Location;
    match: match;
    history: History;
    poster?: string;
    className?: string;
}

export function ClientStream(props: ClientStreamProps): ReactElement {
    let source = `${ClientStream.name}`;
    const [state, dispatch] = useContext(ServerContext);

    const client = state.clients.get(props.id);
    const stream = client ? client.stream : undefined;

    function onClick() {
        source = `${source}.${onClick.name}`;

        if (client !== undefined && stream !== undefined) {
            if (!props.location.pathname.match(/\/viewer(\?.*)?$/)) {
                props.history.push(
                    `${props.location.pathname}/viewer?clientId=${client.id}`,
                );
            }
        }
    }

    useEffect(() => {
        if (client && client.stream === undefined) {
            const socketID = state.clients.getSocketID(props.id);
            if (socketID !== undefined) {
                EmitGetStream(socketID);
            }
        }
    }, [client]);

    const { id, ...videoProps } = props;
    return <VideoStream stream={stream} onClick={onClick} {...videoProps} />;
}

export default withRouter(ClientStream);
