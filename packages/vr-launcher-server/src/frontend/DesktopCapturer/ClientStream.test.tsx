import { cleanup, fireEvent } from "@testing-library/react";
import React, { Dispatch } from "react";

import { Client } from "frontend/Clients";
import ClientStream from "frontend/DesktopCapturer/ClientStream";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import { createMemoryHistory } from "history";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";

class MediaStream {}

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;
let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    state = new ServerStore();
    dispatch = jest.fn();
    client = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "127.0.0.1",
        id: "1",
        status: "connected",
        stream: new MediaStream(),
    };
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <ClientStream id="1" />,
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: { state, dispatch },
    };
    state.clients.set(someSocketID, client);
});

afterEach(() => {
    cleanup();
    jest.clearAllMocks();
});
describe("ClientStream", () => {
    describe("onClick", () => {
        it('should redirect when path = "/options"', () => {
            const { getByRole } = TestRender(TestRouter(props), options);

            fireEvent.click(getByRole("none"));
            expect(props.history).toBeDefined();
            expect((props.history as History).location).toBeDefined();
            const location = props.history.location as Location;
            expect(location.pathname).toBe("/options/viewer");
        });
        it('should not redirect when path = "/options/viewer"', () => {
            props.path += "/viewer";
            const { getByRole } = TestRender(TestRouter(props), options);

            fireEvent.click(getByRole("none"));
            expect(props.history).toBeDefined();
            const history = props.history as History;
            expect(history.index).toBe(1);
            expect(history.entries).toHaveLength(2);
        });
    });
});
