import React, { ReactElement } from "react";
import { Container, Navbar } from "react-bootstrap";
// @ts-ignore
import NoVNC from "react-novnc";

interface DesktopAppProps {
    connectionName: string;
    password: string;
    actionsBar?: (props: any) => ReactElement;
}

export function DesktopAppVNC(): ReactElement {
    return (
        <VNC
            connectionName="127.0.0.1:5950"
            password="password"
            actionsBar={ActionsBar}
        />
    );
}

function ActionsBar(): ReactElement {
    return (
        <Navbar>
            <Navbar.Brand>Navbar</Navbar.Brand>
        </Navbar>
    );
}

function VNC(props?: DesktopAppProps): ReactElement {
    if (props === undefined) {
        props = {
            connectionName: "127.0.0.1",
            password: "password",
        };
    }
    return (
        <Container fluid>
            <NoVNC
                connectionName={props.connectionName}
                password={props.password}
                actionsBar={props.actionsBar}
            />
        </Container>
    );
}
