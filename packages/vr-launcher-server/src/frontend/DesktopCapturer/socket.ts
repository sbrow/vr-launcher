import { port } from "backend/port";
import io from "socket.io-client";
import P2P from "socket.io-p2p";

const socket = io(`http://localhost:${port()}`);
const p2p = new P2P(socket);

/**
 * Whether or not onStream has been called already.
 *
 * @remarks
 * `P2P` doesn't have an `off` function for event handlers,
 * so we must make sure we don't bind the handler more than once.
 */
let hasCallback: boolean = false;

export function onStream(callback: (stream: MediaStream) => void) {
    if (!hasCallback) {
        p2p.on("stream", callback);
        hasCallback = true;
    }
}
