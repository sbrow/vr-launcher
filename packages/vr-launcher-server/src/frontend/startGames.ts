import { ClientID, Start } from "vr-launcher-common";

import { EmitStart } from "frontend/ServerApp/socket";

interface ArgumentsByClient {
    [clientID: string]: {
        [key: string]: string;
    };
}
/**
 * Controls the firing of {@link vr-launcher-common:Start} events.
 *
 * @remarks
 * Waits `timeout` milliseconds after starting the server
 * before starting the clients.
 *
 * @param game The name of the game to start.
 * @param maps A map of `ClientID`s to commandline arguments.
 * @param timeout How long to wait after starting the game on the server
 * before starting the clients.
 */
export function startGames(
    game: string,
    maps: ArgumentsByClient,
    timeout: number = 10000,
) {
    const clients = Object.keys(maps);

    function newEvent(id: ClientID): Start {
        return new Start({
            name: game,
            // tslint:disable-next-line: object-literal-sort-keys
            keys: maps[id],
            id,
        });
    }

    function startPlayers() {
        if (process.env.NODE_ENV !== "production") {
            // tslint:disable-next-line: no-console
            console.log("Starting game for players.");
        }
        const order: ClientID[] = ["1", "2", "3", "4"];
        for (const client of order) {
            if (clients.includes(client)) {
                EmitStart(newEvent(client));
            }
        }
    }

    if (clients.includes("SERVER")) {
        EmitStart(newEvent("SERVER"));
        setTimeout(startPlayers, timeout);
    }
}
