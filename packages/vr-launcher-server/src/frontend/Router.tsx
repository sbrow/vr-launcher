import React, { ReactElement } from "react";
import { Route } from "react-router";
import { HashRouter } from "react-router-dom";
import { VersionNumber } from "vr-launcher-common";

import { AsyncApp } from "frontend/AsyncApp";
import { ServerStoreProvider } from "frontend/ServerApp/ServerStoreProvider";
import packageJson from "package.json";

export function Router(): ReactElement {
    return (
        <>
            <HashRouter>
                <Route exact path="/" component={AsyncApp("GameSelection")} />
                <ServerStoreProvider>
                    <Route
                        exact
                        path="/options"
                        component={AsyncApp("ServerApp/ServerApp")}
                    />
                    <Route
                        path="/options/viewer"
                        component={AsyncApp("DesktopCapturer/DesktopApp")}
                    />
                </ServerStoreProvider>
            </HashRouter>
            <VersionNumber
                version={packageJson.version}
                className="text-white"
            />
        </>
    );
}
