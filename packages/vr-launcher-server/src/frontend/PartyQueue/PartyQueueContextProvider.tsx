import {
    PartyQueueStore,
    PartyQueueStoreAction,
    reducer,
} from "frontend/PartyQueue/PartyQueueStore";
import React, { createContext, Dispatch, ReactNode, useReducer } from "react";

/**
 * Provides {@link PartyQueue} access to a {@link PartyQueueStore}.
 */
export const PartyQueueContext = createContext<
    [PartyQueueStore, Dispatch<PartyQueueStoreAction>]
>([
    new PartyQueueStore(),
    () => {
        throw new Error("no reducer has been set");
    },
]);

/**
 * Wrapper component that grants children access to its context through `react.useContext(PartyQueueContext)`.
 */
export function PartyQueueContextProvider(props: { children?: ReactNode }) {
    const context = useReducer(reducer, new PartyQueueStore());
    return (
        <PartyQueueContext.Provider value={context}>
            {props.children}
        </PartyQueueContext.Provider>
    );
}
