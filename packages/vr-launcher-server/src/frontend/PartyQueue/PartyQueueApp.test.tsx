import { cleanup, fireEvent } from "@testing-library/react";
import React from "react";
import { someParty, TestContextProvider, TestRender } from "testUtils";

import {
    PartyQueueApp,
    StatefulPartyQueue,
} from "frontend/PartyQueue/PartyQueueApp";
import { PartyQueueContext } from "frontend/PartyQueue/PartyQueueContextProvider";
import { PartyQueueStore } from "frontend/PartyQueue/PartyQueueStore";
import { Action } from "vr-launcher-common";

describe(PartyQueueApp.name, () => {
    const state = new PartyQueueStore([someParty]);
    const dispatch = jest.fn();

    afterEach(cleanup);
    it('triggers "onClick"', () => {
        const { getByText } = TestRender(<StatefulPartyQueue />, {
            // @ts-ignore
            wrapper: TestContextProvider,
            wrapperArgs: { state, dispatch, context: PartyQueueContext },
        });
        expect(fireEvent.click(getByText("×"))).toBeTruthy();

        const want: Action = {
            type: "onClick",
            source: StatefulPartyQueue.name,
            payload: someParty.name,
        };
        expect(dispatch).toHaveBeenCalledWith(want);
    });
});
