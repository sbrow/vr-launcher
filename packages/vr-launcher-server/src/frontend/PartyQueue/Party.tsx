import React, { ReactElement } from "react";
import { Toast } from "react-bootstrap";

/**
 * A group of players.
 */
export interface Party {
    name: string;
    players: 1 | 2 | 3 | 4;
}

export interface PartyProps {
    party: Party;
    onClick?: (name: string) => void;
}

export function Party(props: PartyProps): ReactElement {
    const { name, players } = props.party;
    const onClick = props.onClick || ((name: string) => {});

    return (
        <Toast onClose={() => onClick(name)}>
            <Toast.Header>
                <span className="mr-auto">
                    {name} - {players}
                </span>
            </Toast.Header>
        </Toast>
    );
}
