import {
    PartyQueueStore,
    PartyQueueStoreAction,
    reducer,
} from "frontend/PartyQueue/PartyQueueStore";
import { someParty, source } from "testUtils";

describe(PartyQueueStore.name, () => {
    it('Handles "onClick"', () => {
        const action: PartyQueueStoreAction = {
            source,
            type: "onClick",
            payload: someParty.name,
        };
        const state = new PartyQueueStore([someParty]);

        const newState = reducer(state, action);
        expect(newState).not.toBe(state);
        expect(newState.parties).toHaveLength(0);
    });
});
