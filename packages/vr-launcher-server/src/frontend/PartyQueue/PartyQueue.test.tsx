import "@testing-library/jest-dom/extend-expect";
import { cleanup, render } from "@testing-library/react";

import { PartyQueue, PartyQueueProps } from "frontend/PartyQueue/PartyQueue";

describe(PartyQueue.name, () => {
    const props: PartyQueueProps = {
        parties: [{ name: "party", players: 2 }],
    };
    type Render = ReturnType<typeof render>;
    let rendered: Render;
    let elements: any;

    beforeAll(() => {
        rendered = render(PartyQueue(props));
        const { getByText } = rendered;
        elements = {
            name: getByText(props.parties[0].name, { exact: false }),
            players: getByText(String(props.parties[0].players), {
                exact: false,
            }),
        };
    });
    afterAll(cleanup);
    it("renders correctly", () => {
        const { container } = rendered;
        expect(container.firstChild).toMatchSnapshot();
    });
    it("renders party name", async () => {
        expect(elements.name).toBeInTheDocument();
    });
    it("renders party size", () => {
        expect(elements.players).toBeInTheDocument();
    });
    it("renders party name and size in one element", () => {
        expect(elements.players).toBe(elements.name);
    });
});
