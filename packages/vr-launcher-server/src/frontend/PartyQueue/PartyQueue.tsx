import React, { ReactElement } from "react";

import { Party } from "frontend/PartyQueue/Party";

export interface PartyQueueProps {
    parties: Party[];
    onClose?: any;
}

export function PartyQueue(props: PartyQueueProps): ReactElement {
    return (
        <>
            {props.parties.map(party => (
                <Party key={party.name} party={party} onClick={props.onClose} />
            ))}
        </>
    );
}
