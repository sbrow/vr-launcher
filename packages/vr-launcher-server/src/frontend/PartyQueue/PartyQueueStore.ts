import { createContext, Dispatch } from "react";
import { Action } from "vr-launcher-common";

import { Party } from "frontend/PartyQueue/Party";

type GetElementType<T extends Array<any>> = T extends Array<infer U>
    ? U
    : never;

/**
 * Action types that are accepted by the {@link PartyQueueStore}.
 */
export interface PartyQueueStoreAction extends Action {
    type: "onClick";
    payload: GetElementType<PartyQueueStore["parties"]>["name"];
}

/**
 * Model-Controller for {@link PartyQueue}
 */
export class PartyQueueStore {
    constructor(public parties: Party[] = []) {}

    /**
     * Removes the party with name equal to `action.payload` from `this.parties`.
     *
     * @returns Whether `this.parties` changed.
     */
    public handleAction(action: PartyQueueStoreAction): boolean {
        const newParties = this.parties.filter(
            value => value.name !== action.payload,
        );
        if (newParties.length !== this.parties.length) {
            this.parties = newParties;
            return true;
        }
        return false;
    }
}

/**
 * Updates state dependant on action.
 * @returns newState.
 */
export function reducer(
    state: PartyQueueStore,
    action: PartyQueueStoreAction,
): PartyQueueStore {
    const newState = new PartyQueueStore(state.parties);

    const changed = newState.handleAction(action);
    if (changed) {
        return newState;
    }
    return state;
}
