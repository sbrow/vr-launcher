import React, { useContext } from "react";

import { PartyQueue } from "frontend/PartyQueue/PartyQueue";
import {
    PartyQueueContext,
    PartyQueueContextProvider,
} from "frontend/PartyQueue/PartyQueueContextProvider";

export function PartyQueueApp() {
    return (
        <PartyQueueContextProvider>
            <StatefulPartyQueue />
        </PartyQueueContextProvider>
    );
}

export function StatefulPartyQueue() {
    const [state, dispatch] = useContext(PartyQueueContext);
    const onClose = (key: string) => {
        dispatch({
            type: "onClick",
            source: StatefulPartyQueue.name,
            payload: key,
        });
    };
    return <PartyQueue parties={state.parties} onClose={onClose} />;
}
