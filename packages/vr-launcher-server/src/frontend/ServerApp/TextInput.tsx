import React, { ReactElement, useContext } from "react";
import { Form, FormControlProps } from "react-bootstrap";
import { BsPrefixProps, ReplaceProps } from "react-bootstrap/helpers";
import { ClientID } from "vr-launcher-common";

import { SetNickname } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export function TextInput(props: {
    clientID: ClientID;
    nickname: string | undefined;
}): ReactElement {
    const source = "TextInput";
    const dispatch = useContext(ServerContext)[1];
    const disabled = props.nickname === undefined;
    const onChange = (
        event: React.FormEvent<
            ReplaceProps<"input", BsPrefixProps<"input"> & FormControlProps>
        >,
    ): void => {
        if (props.clientID !== "SERVER") {
            const action = new SetNickname(
                source,
                props.clientID,
                // @ts-ignore
                event.target.value,
            );
            dispatch(action);
        }
    };
    const placeholder = disabled ? "N/A" : "Nickname";

    return (
        <Form>
            <Form.Group>
                <Form.Label htmlFor="player-one">Player Name</Form.Label>
                <div className="clearfix" />
                <Form.Control
                    type="text"
                    placeholder={placeholder}
                    value={props.nickname}
                    onChange={onChange}
                    disabled={disabled}
                    className="border custom-border text-center field-background"
                />
            </Form.Group>
        </Form>
    );
}
