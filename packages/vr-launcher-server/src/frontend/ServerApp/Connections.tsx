import React, { ReactElement } from "react";
import { Col, Row } from "react-bootstrap";

import { CustomCard } from "frontend/ServerApp/CustomCard";
import { ColProps } from "react-bootstrap";
import { ClientID } from "vr-launcher-common";

interface ConnectionRowProps {
    className?: string;
    clients: [ClientID, ClientID];
    innerMargin?: ColProps["md"];
}

function ConnectionRow(props: ConnectionRowProps): ReactElement {
    const className = props.className || "";
    const m = props.innerMargin || 2;

    return (
        <Row className={className}>
            <CustomCard
                className="cut-corner mb-sm-3"
                clientID={props.clients[0]}
            />
            <Col md={m} />
            <CustomCard
                className="cut-corner mb-sm-3"
                clientID={props.clients[1]}
            />
        </Row>
    );
}

export function Connections(): ReactElement {
    const margin: number = 3;
    const classes = `d-sm-flex content-stretch my-${margin}`;

    return (
        <Col md={8}>
            <Row>
                <CustomCard clientID="SERVER" />
            </Row>
            <ConnectionRow className={classes} clients={["1", "2"]} />
            <ConnectionRow className={classes} clients={["3", "4"]} />
        </Col>
    );
}
