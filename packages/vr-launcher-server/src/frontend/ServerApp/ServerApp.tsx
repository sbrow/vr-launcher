import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { History, Location } from "history";
import React, { ReactElement, useContext, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";

import { AddStream, SelectGame } from "frontend/actions";
import { onStream } from "frontend/DesktopCapturer/socket";
import { LinkButton } from "frontend/LinkButton";
import { PlayAreaDropdown } from "frontend/options/PlayAreaDropdown";
import { PlayTimeDropdown } from "frontend/options/PlayTimeDropdown";
import { Connections } from "frontend/ServerApp";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { Start } from "frontend/ServerApp/Start";
import { Stop } from "frontend/ServerApp/Stop";
import { TitleBar } from "frontend/ServerApp/TitleBar";
import { withRouter } from "react-router";

export function Server(props: {
    location: Location;
    history: History;
}): ReactElement {
    const [state, dispatch] = useContext(ServerContext);

    const classes = ["d-sm-flex", "align-items-center"];
    const title =
        new URLSearchParams(props.location.search).get("game") || "Launcher";

    let source = Server.name;
    if (title !== "Launcher" && title !== state.game) {
        dispatch(new SelectGame(title, source));
    }

    useEffect(() => {
        source = `${source}.useEffect`;

        const streamCallback = (stream: MediaStream): void => {
            source = `${source}.${streamCallback.name}`;
            dispatch(new AddStream({ source, stream }));
        };
        onStream(streamCallback);
    });

    const mid = 8;
    const side = (12 - mid) / 2;

    return (
        <Container>
            <TitleBar title={title} />
            <Row className={classes.join(" ")}>
                <Col md={2} className="text-center pb-3 pb-md-0 pt-md-3">
                    <LinkButton to="/" size="lg">
                        <FontAwesomeIcon icon={faChevronLeft} />
                    </LinkButton>
                </Col>
                <Connections />
                <Col md={2} className="pt-4">
                    <PlayAreaDropdown small />
                    <PlayTimeDropdown small />
                </Col>
            </Row>
            <Row>
                <Col xs={0} sm={0} md={4} lg={4} xl={5}></Col>
                <Col sm={6} md={2} lg={2} xl={1}>
                    <Start size="medium" variant="success" />
                </Col>
                <Col sm={6} md={2} lg={2} xl={1}>
                    <Stop size="medium" variant="danger" />
                </Col>
                <Col xs={0} sm={0} md={4} lg={4} xl={5}></Col>
            </Row>
        </Container>
    );
}

export const ServerApp = withRouter(Server);
export default ServerApp;
