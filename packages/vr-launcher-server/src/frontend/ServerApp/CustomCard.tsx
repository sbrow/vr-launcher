import { faCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { ReactElement, useContext } from "react";
import { Col, Row } from "react-bootstrap";
import {
    /*BatteryStatus,*/ ClientID /*, StatusLine*/,
} from "vr-launcher-common";

import offlineImage from "assets/static.jpg";
import { SetClientActive } from "frontend/actions";
// import { AsyncComponent } from "frontend/AsyncApp";
import { Client } from "frontend/Clients";
import ClientStream from "frontend/DesktopCapturer/ClientStream";
import { VideoStatus } from "frontend/DesktopCapturer/VideoStatus";
import { BatteryStatus } from "frontend/ServerApp/BatteryStatus";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { StatusLine } from "frontend/ServerApp/StatusLine";
import { TextInput } from "frontend/ServerApp/TextInput";

export interface CustomCardProps {
    className?: string;
    clientID: ClientID;
}

export function CustomCard(props: CustomCardProps): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    const classes = props.className !== undefined ? props.className : undefined;
    const source = CustomCard.name;

    const defaultClient: Client = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "",
        id: props.clientID,
        status: "disconnected",
        name: `Player ${props.clientID}`,
        active: false,
    };
    const client: Client = state.clients.get(props.clientID) || defaultClient;
    const nickname = state.nicknames.get(props.clientID);
    const { active, battery } = client;
    function getStatus() {
        switch (client.status) {
            case "connected":
                if (active) {
                    return "ready";
                }
                break;
            case "ready":
                if (!active) {
                    return "connected";
                }
                break;
        }
        return client.status;
    }
    const status = getStatus();

    const onClick = () => {
        if (client && client.status !== "disconnected") {
            const action = new SetClientActive(source, props.clientID);
            dispatch(action);
        }
    };

    if (props.clientID === "SERVER") {
        return (
            <Col
                md={12}
                className={`${classes +
                    " "} border border-custom text-white text-center`.trimLeft()}
                    onClick={onClick}
            >
                Server: <StatusLine status={status} battery={battery} />
            </Col>
        );
    }
    return (
        <Col md={5} className={classes}>
            <Row className="border-bottom border-custom">
                <Col md={12}>
                    <h2 className="h6 text-center text-white py-2 my-0">
                        <StatusLine status={status} />
                    </h2>
                </Col>
            </Row>
            <Row className="pt-3">
                <Col xs={12} sm={12} md={12} lg={8} className="text-center">
                    <Row className="foreground align-items-center">
                        <Col>
                            <VideoStatus
                                online={!(status === "disconnected")}
                            />
                        </Col>
                    </Row>
                    {/* <AsyncComponent
                        path="frontend/DesktopCapturer/ClientStream"
                        {...{ id: props.clientID }}
                    /> */}
                    <ClientStream
                        id={props.clientID}
                        poster={offlineImage}
                        className="border border-custom"
                        background
                    />
                </Col>
                <Col
                    xs={12}
                    sm={12}
                    md={12}
                    lg={4}
                    className="border-bottom border-custom text-center text-white pt-3 pt-lg-0"
                >
                    <span className="fa-layers fa-4x" onClick={onClick}>
                        <FontAwesomeIcon
                            icon={faCircle}
                            size="1x"
                            className="accent"
                            style={{ display: "inline-block" }}
                        />
                        <span
                            className="fa fa-fw fa-xs fa-layers-text fa-inverse"
                            style={{
                                fontFamily: "inherit",
                                fontWeight: "bold",
                            }}
                        >
                            {props.clientID}
                        </span>
                    </span>
                    <div className="clearfix pt-xl-2 pt-lg-2 pt-md-2" />
                    <BatteryStatus battery={battery} />
                </Col>
            </Row>
            <Row>
                <Col md={1} />
                <Col md={10} className="text-center text-white py-2">
                    <TextInput clientID={props.clientID} nickname={nickname} />
                </Col>
                <Col md={1} />
            </Row>
            <div className="clearfix" />
        </Col>
    );
}
