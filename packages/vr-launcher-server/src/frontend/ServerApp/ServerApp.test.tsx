import { cleanup, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import "@testing-library/jest-dom/extend-expect";
import React, { Dispatch } from "react";
import { Info } from "vr-launcher-common";

import { Client } from "frontend/Clients";
import { FancyApp } from "frontend/FancyStuff";
import ServerApp from "frontend/ServerApp/ServerApp";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";

jest.mock("frontend/ServerApp/CustomCard", () => ({
    CustomCard: () => <div className="custom-card" />,
}));

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;
let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    const id = "1";
    state = new ServerStore();
    dispatch = jest.fn();
    client = {
        localIP: "127.0.0.1",
        id,
        status: "connected",
    } as Info;
    state.clients.set(someSocketID, client);
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <ServerApp />,
    };
    options = {
        // @ts-ignore
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: { state, dispatch },
    };
});

afterEach(() => {
    cleanup();
});

describe(ServerApp.name, () => {
    it.skip("renders correctly", () => {
        const { container } = TestRender(TestRouter(props), options);

        const want = render(
            <FancyApp card={() => <div className="custom-card" />} />,
        ).container;
        expect(container).toStrictEqual(want);
    });
});
