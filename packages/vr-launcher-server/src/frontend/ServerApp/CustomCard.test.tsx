import { cleanup, render, waitForElement } from "@testing-library/react";
import { createMemoryHistory } from "history";
import "@testing-library/jest-dom/extend-expect";
import React, { Dispatch } from "react";
import { Info } from "vr-launcher-common";

import { Client } from "frontend/Clients";
import { FancyCard } from "frontend/FancyStuff";
import { CustomCard } from "frontend/ServerApp/CustomCard";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";

jest.mock("assets/static.jpg", () => ({
    default: "path.jpg",
}));

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;
let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    const id = "1";
    state = new ServerStore();
    state.path = `/options/viewer?clientId=${id}`;
    dispatch = jest.fn();
    client = {
        localIP: "127.0.0.1",
        id,
        status: "connected",
    } as Info;
    state.clients.set(someSocketID, client);
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => (
            <CustomCard className="cut-corner mb-sm-3" clientID={id} />
        ),
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: { state, dispatch },
    };
});

afterEach(() => {
    cleanup();
    jest.clearAllMocks();
});

describe(CustomCard.name, () => {
    it.skip("Matches template", async () => {
        const { container, getByRole } = TestRender(TestRouter(props), options);
        const video = await waitForElement(() => getByRole("none"));
        expect(video).toBeInTheDocument();

        const want = render(<FancyCard />).container;
        expect(container.firstChild).toBe(want.firstChild);
    });
});
