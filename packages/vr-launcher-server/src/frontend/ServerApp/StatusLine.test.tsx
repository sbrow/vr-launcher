import { cleanup, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { ClientStatus } from "vr-launcher-common";

import { StatusLine } from "frontend/ServerApp/StatusLine";

describe(StatusLine.name, () => {
    let container: HTMLElement;
    let getByText: ReturnType<typeof render>["getByText"];
    let rerender: ReturnType<typeof render>["rerender"];
    let span: HTMLElement;
    let status: ClientStatus;

    describe('When status = "connected"', () => {
        status = "connected";

        beforeAll(() => {
            ({ container, getByText, rerender } = render(
                <StatusLine status={status} />,
            ));
            const spanTarget = getByText(status);
            expect(spanTarget).toBeInstanceOf(HTMLElement);
            span = spanTarget as HTMLElement;
        });
        afterAll(cleanup);

        it("has correct class", () => {
            expect((span as HTMLElement).className).toBe("text-white");
        });
        it("renders correctly", () => {
            expect(container.firstChild).toMatchInlineSnapshot(`
                <span
                  class="text-white"
                >
                  connected
                </span>
            `);
        });

        const customClass = "text-shadow";
        describe(`When className = "${customClass}"`, () => {
            beforeAll(() =>
                rerender(
                    <StatusLine status={status} className={customClass} />,
                ),
            );

            it("has correct classes", () => {
                expect(span).not.toBeNull();
                const classes = (span as HTMLElement).className.split(" ");
                expect(classes).toMatchObject(
                    expect.arrayContaining(["text-white", customClass]),
                );
            });
        });
    });
});
