import {
    AddStream,
    Disconnect,
    InfoAction,
    SetClientActive,
    SetNickname,
    UpdateGames,
} from "frontend/actions";
import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { Client } from "frontend/Clients";
import { Nickable } from "frontend/Nicknames";
import {
    reducer,
    ServerStore,
    ServerStoreAction,
} from "frontend/ServerApp/ServerStore";
import { getStream } from "frontend/ServerApp/streams";
import { games, someSocketID, source } from "testUtils";

class MediaStream {
    constructor(public id: string) {
        this.id = id;
    }
}

let state: ServerStore;
let someClient: Client;

beforeEach(() => {
    state = new ServerStore();
    someClient = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "127.0.0.1",
        id: "1",
        status: "connected",
        name: "",
    };
});

describe("reducer", () => {
    describe('when action = { type: "none" }', () => {
        it.each([{ source, type: "none" }])(
            "should bail when action = %p",
            (action: ServerStoreAction) => {
                const newState = reducer(state, action);
                expect(newState).toBe(state);
            },
        );
    });
    describe("when action instanceof AddStream", () => {
        const streamID = "12345";
        let stream: MediaStream;
        let action: AddStream;
        beforeEach(() => {
            stream = new MediaStream(streamID);
            action = new AddStream({ source, stream, id: streamID });
        });
        it("should add action.stream to the cache", () => {
            const newState = reducer(state, action);
            expect(newState).toBe(state);

            const got = getStream(streamID);
            expect(got).toMatchObject(stream);
        });
        it("should attach `action.stream` to the corresponding client", () => {
            someClient.streamID = streamID;
            state.clients.set(someSocketID, someClient);

            const newState = reducer(state, action);
            expect(newState).not.toBe(state);

            const got = someClient.stream;
            expect(got).toStrictEqual(stream);
        });
    });
    describe("when action instanceof InfoAction", () => {
        it.each([{}, undefined])(
            "should bail if payload = %p",
            (payload?: InfoAction["payload"]) => {
                state.clients.set(someSocketID, someClient);
                const action = new InfoAction(source, someSocketID, payload);
                const newState = reducer(state, action);
                expect(newState).toBe(state);
            },
        );
    });
    describe("when action instanceof Disconnect", () => {
        it("should remove client", () => {
            const clients = state.clients;
            clients.set(someSocketID, someClient);

            expect(clients.keys[someSocketID]).toMatchObject(someClient);

            const newState = reducer(
                state,
                new Disconnect(source, someSocketID),
            );
            expect(newState).not.toBe(state);
            const newClients = newState.clients;
            expect(newClients.keys[someSocketID]).toBeUndefined();
        });
    });
    describe("when action instanceof OptionsStoreAction", () => {
        describe("when action.type = setPlayTime", () => {
            it("should set time = 20", () => {
                const newState = reducer(
                    state,
                    new OptionsStoreAction(source, "setPlayTime", 20),
                );
                expect(newState).not.toBe(state);
                expect(newState.options.playTime).toBe(20);
            });
        });
    });
    describe("when action instanceof SetNickname", () => {
        it("should update name when input is valid", () => {
            const want: { id: Nickable; name: string } = {
                id: "3",
                name: "nick",
            };

            const action = new SetNickname(source, want.id, want.name);

            const newState = reducer(state, action);
            expect(newState).not.toBe(state);
            const got = newState.nicknames.get(want.id);
            expect(got).toEqual(want.name);
        });
    });
    describe("when action instanceof SetClientActive", () => {
        // tslint:disable-next-line: mocha-no-side-effect-code
        it.each([undefined, false])(
            'should set "active" to "true" if activity = %p',
            (active: boolean | undefined) => {
                someClient.active = active;
                state.clients.set(someSocketID, someClient);

                const action = new SetClientActive(source, someClient.id);
                const newState = reducer(state, action);
                expect(newState).not.toBe(state);

                const newClient = newState.clients.get(someClient.id);
                expect(newClient).not.toBeUndefined();
                expect((newClient as Client).active).toEqual(true);
            },
        );
    });
    describe("when action instanceof UpdateGames", () => {
        it("should update games", () => {
            const action = new UpdateGames(source, games);
            expect(action instanceof UpdateGames).toBe(true);
            const want = action.games;
            const newState = reducer(state, action);
            const got = newState.games;
            expect(got).toEqual(want);
            expect(newState).not.toBe(state);
        });
        it("should bail if games haven't changed", () => {
            state.games = games;
            const action = new UpdateGames(source, games);
            expect(action instanceof UpdateGames).toBe(true);
            const newState = reducer(state, action);
            expect(newState).toBe(state);
        });
    });
});
