import React, { ReactElement, useEffect, useReducer } from "react";
import { Info, ipcOff, ipcOn } from "vr-launcher-common";

import { AddStream, Disconnect, InfoAction } from "frontend/actions";
import { onStream } from "frontend/DesktopCapturer/socket";
import {
    reducer,
    ServerContext,
    ServerStore,
} from "frontend/ServerApp/ServerStore";
import { EmitGetInfo } from "frontend/ServerApp/socket";

export function ServerStoreProvider(props: { children?: any }): ReactElement {
    const [state, dispatch] = useReducer(reducer, new ServerStore());
    let source = ServerStoreProvider.name;

    function streamCallback(stream: MediaStream): void {
        source = `${source}.${streamCallback.name}`;
        dispatch(new AddStream({ source, stream }));
    }

    function onInfo(event: any, data: Partial<Info>, id: string) {
        const action = new InfoAction(source, id, data);
        dispatch(action);
    }

    function onDisconnect(event: any, id: string) {
        source = `${source}.${onDisconnect}`;
        dispatch(new Disconnect(source, id));
    }

    useEffect(() => {
        source = `${source}.useEffect`;

        onStream(streamCallback);
        ipcOff("disconnect", onDisconnect);
        ipcOn("disconnect", onDisconnect);
        ipcOff("info", onInfo);
        ipcOn("info", onInfo);
        return () => {
            ipcOff("info", onInfo);
            ipcOff("disconnect", onDisconnect);
        };
    });

    EmitGetInfo();

    return (
        <ServerContext.Provider value={[state, dispatch]}>
            {props.children}
        </ServerContext.Provider>
    );
}
