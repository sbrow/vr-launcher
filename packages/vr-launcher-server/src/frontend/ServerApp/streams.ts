import { Logger } from "backend/Logger";
import { AddStream } from "frontend/actions";

/**
 * Temporary storage for MediaStreams from clients,
 * while we wait to find out where they came from.
 */
const streams: { [id: string]: MediaStream } = {};

/**
 * @param id the id of the stream to get.
 * @returns the stream with the corresponding id, if it exists.
 */
export function getStream(id: string): MediaStream | undefined {
    return streams[id];
}

/**
 * The same as {@link getStream}, except it deletes the stream from the
 * cache afterwards.
 */
export function removeStream(id: string): MediaStream | undefined {
    const stream = getStream(id);
    if (stream !== undefined) {
        delete streams[id];
    }
    return stream;
}

/**
 * Adds the given stream to the cache.
 *
 * @remarks
 * Won't add the stream if a stream with `id` already exists.
 *
 * @returns whether or not the stream was added.
 */
export function addStream(stream: MediaStream, id?: string): boolean {
    if (id === undefined) {
        const tracks = stream.getTracks();
        if (tracks.length > 0) {
            id = tracks[0].id;
        } else {
            return false;
        }
    }
    if (streams[id] === undefined) {
        streams[id] = stream;
        return true;
    }
    return false;
}

/**
 * Adds `action.stream` to the cache.
 * @param action
 * @returns Whether `action.stream` was added or not.
 */
export function handleAddStream(action: AddStream): boolean {
    const success = addStream(action.stream, action.id);
    if (!success) {
        Logger.error(`Couldn't add stream ${action}.`);
    } else {
        Logger.debug(`stream added.`);
        return true;
    }
    return false;
}

/**
 * Returns a random stream, or undefined if the cache is empty.
 *
 * @internal Only suitable for testing.
 */
export function getAnyStream(): MediaStream | undefined {
    for (const streamID of Object.keys(streams)) {
        return streams[streamID];
    }
    return undefined;
}

/**
 * @returns An array of all the cached streams' ids.
 */
export function getAvailableStreams(): string[] {
    return Object.keys(streams);
}
