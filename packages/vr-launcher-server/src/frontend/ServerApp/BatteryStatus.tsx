import { faBatteryHalf } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { ReactElement } from "react";

export function BatteryStatus(props: {
    battery: number | undefined;
}): ReactElement {
    return (
        <>
            <FontAwesomeIcon icon={faBatteryHalf} className="accent" />
            <span className="percentage accent">
                {" " + (props.battery || "?")}%
            </span>
        </>
    );
}
