import Octicon, { PrimitiveSquare } from "@githubprimer/octicons-react";
import React, { ReactElement, useContext } from "react";
import { Button, ButtonProps } from "react-bootstrap";

import { Stop as StopAction } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export interface StopProps {
    size: number | Octicon.Size;
    className?: string;
    variant: ButtonProps["variant"];
}

export function Stop(
    props: StopProps = { size: "small", variant: "primary" },
): ReactElement {
    const dispatch = useContext(ServerContext)[1];
    function onClick() {
        dispatch(new StopAction(`${Stop.name}.onClick`));
    }
    const className = props.className || "";

    return (
        <Button
            variant={props.variant}
            onClick={onClick}
            size="lg"
            className={className}
        >
            <Octicon icon={PrimitiveSquare} size={props.size} />
        </Button>
    );
}
