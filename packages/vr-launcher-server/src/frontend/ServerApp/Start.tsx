import Octicon, { TriangleRight } from "@githubprimer/octicons-react";
import React, { ReactElement, useContext } from "react";
import { Button, ButtonProps } from "react-bootstrap";

import { Start as StartAction } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export interface StartProps {
    size: number | Octicon.Size;
    className?: string;
    variant: ButtonProps["variant"];
}

export function Start(
    props: StartProps = { size: "small", variant: "primary" },
): ReactElement {
    const dispatch = useContext(ServerContext)[1];
    function onClick() {
        dispatch(new StartAction(`${Start.name}.onClick`));
    }

    const className = props.className || "";

    return (
        <Button
            variant={props.variant}
            onClick={onClick}
            size="lg"
            className={className}
        >
            <Octicon icon={TriangleRight} size={props.size} />
        </Button>
    );
}
