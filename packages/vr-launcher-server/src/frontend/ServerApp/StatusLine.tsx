import React, { ReactElement } from "react";
import { ClientStatus } from "vr-launcher-common";

// @todo merge into vr-launcher-common?
export function StatusLine(props: {
    status: ClientStatus;
    className?: string;
    battery?: number;
}): ReactElement {
    const color = ((status: ClientStatus) => {
        switch (status) {
            case "ready":
                return "success";
            case "disconnected":
                return "danger";
            default:
                return "white";
        }
    })(props.status);
    const battery = props.battery ? (
        <span className="text-white">`(${props.battery}%)`</span>
    ) : (
        undefined
    );
    const className = `${props.className || ""} text-${color}`.trimLeft();

    return (
        <>
            <span className={className}>{props.status}</span>
            {battery}
        </>
    );
}
