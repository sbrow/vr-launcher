import { cleanup, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import React, { Dispatch } from "react";

import { Client } from "frontend/Clients";
import { FancyConnections } from "frontend/FancyStuff";
import { Connections } from "frontend/ServerApp/Connections";
import { ServerStore, ServerStoreAction } from "frontend/ServerApp/ServerStore";
import { createMemoryHistory } from "history";
import {
    someSocketID,
    TestContextProvider,
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
} from "testUtils";
import { Info } from "vr-launcher-common";

jest.mock("frontend/ServerApp/CustomCard", () => ({
    CustomCard: () => <div className="custom-card" />,
}));

let state: ServerStore;
let dispatch: jest.Mock<Dispatch<ServerStoreAction>>;
let client: Client;
let props: TestRouterProps;
let options: TestRenderOptions;

// fn();

beforeEach(() => {
    const id = "1";
    state = new ServerStore();
    dispatch = jest.fn();
    client = {
        localIP: "127.0.0.1",
        id,
        status: "connected",
    } as Info;
    state.clients.set(someSocketID, client);
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <Connections />,
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: { state, dispatch },
    };
});

afterEach(() => {
    cleanup();
});

describe("Connections", () => {
    it("render", async () => {
        const { container } = TestRender(TestRouter(props), options);

        const want = render(
            <FancyConnections card={() => <div className="custom-card" />} />,
        ).container;
        expect(container).toStrictEqual(want);
    });
});
