import { ClientID, ipcSendAsync, Start } from "vr-launcher-common";

export function connected(): true {
    return true;
}

export function EmitGetInfo(): void {
    ipcSendAsync("get info");
}

export function EmitStop(client?: ClientID): void {
    ipcSendAsync("stop", client);
}

export function EmitStart(event?: Start): void {
    ipcSendAsync("start", event);
}

export function EmitSendKeys(keys: string, id: ClientID): void {
    ipcSendAsync("sendKeys", keys, id);
}

export function EmitGetStream(socketID: string): void {
    ipcSendAsync("get stream", socketID);
}
