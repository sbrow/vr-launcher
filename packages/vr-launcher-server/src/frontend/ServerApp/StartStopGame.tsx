import React, { ReactElement, useContext } from "react";
import { Button, Col, Row } from "react-bootstrap";

import { ToggleRunning as ToggleRunningAction } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export function StartStopGame(): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    const style: {
        text: string;
        variant: "primary" | "danger";
    } = state.running
        ? {
              text: "Stop Game",
              variant: "danger",
          }
        : {
              text: "Start Game",
              variant: "primary",
          };
    const onClick = () => {
        dispatch(new ToggleRunningAction("startGame.onClick"));
    };
    return (
        <Row>
            <Col>
                <Button
                    variant={style.variant}
                    className="mt-5 mb-2"
                    size="lg"
                    onClick={onClick}
                >
                    {style.text}
                </Button>
            </Col>
        </Row>
    );
}
