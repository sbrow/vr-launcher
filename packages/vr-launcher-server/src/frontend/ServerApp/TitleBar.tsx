import { History, Location } from "history";
import React, { ReactElement } from "react";
import { Col, Navbar } from "react-bootstrap";
import { withRouter } from "react-router";

import { Options } from "frontend/options";

export interface TitleBarProps {
    location?: Location;
    history?: History;
    title?: string;
}
export function _TitleBar(props: TitleBarProps): ReactElement {
    return (
        <Navbar>
            <Navbar.Brand>
                <Options />
            </Navbar.Brand>
            <Navbar.Text className="py-2 text-white text-sm-left">
                {/* <Title title={title} className="text-center" /> */}
                <h1>{props.title || ""}</h1>
            </Navbar.Text>
            <Col></Col>
        </Navbar>
    );
}

export const TitleBar = withRouter(_TitleBar);
