import { createContext, Dispatch } from "react";
import { ClientID } from "vr-launcher-common";

import {
    AddStream,
    Disconnect,
    InfoAction,
    RedirectAction,
    SelectGame,
    SetClientActive,
    SetClients,
    SetNickname,
    Start,
    Stop,
    ToggleElement,
    ToggleRunning,
    UpdateGames,
} from "frontend/actions";
import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { Clients } from "frontend/Clients";
import { Nicknames } from "frontend/Nicknames";
import { OptionsStore } from "frontend/options/OptionsStore";
import { EmitStop } from "frontend/ServerApp/socket";
import { handleAddStream, removeStream } from "frontend/ServerApp/streams";
import { startGames } from "frontend/startGames";
import { RedirectTo } from "types";

export type ServerStoreAction =
    | AddStream
    | Disconnect
    | InfoAction
    | RedirectAction
    | SelectGame
    | SetClientActive
    | SetClients
    | SetNickname
    | Start
    | Stop
    | ToggleRunning
    | OptionsStoreAction
    | UpdateGames;

function debug(message: any) {
    if (process.env.NODE_ENV === "development") {
        // tslint:disable-next-line: no-console
        console.debug(message);
    }
}

function getMaps(
    client: ClientID,
    options: OptionsStore,
    nicknames: Nicknames,
): { [key: string]: string } {
    const maps: { [key: string]: string } = {
        $AREA_SIZE: String(options.areaSize),
        $PLAY_TIME: String(options.playTime),
        $ROOM_NAME: "ROOM",
    };
    const [address, port] =
        options.hostIP !== undefined
            ? options.hostIP.split(":")
            : [undefined, undefined];
    const nickname = nicknames.get(client);

    maps.$HOST_ADDRESS = address || "0.0.0.0";
    maps.$HOST_PORT = port || "3000";
    maps.$NICK_NAME = nickname || "John Doe";
    return maps;
}

export class ServerStore {
    public options: OptionsStore;
    public running: boolean;
    public clients: Clients;
    public nicknames: Nicknames;
    public games: string[];
    public game: string | undefined;
    public showOptions: boolean;
    public redirect: RedirectTo | undefined;

    constructor(args?: ServerStore) {
        // @ts-ignore
        const defaultArgs: ServerStore = {
            options: new OptionsStore(),
            running: false,
            // tslint:disable-next-line: object-literal-sort-keys
            clients: new Clients(),
            nicknames: new Nicknames(),
            games: [],
            game: undefined,
            showOptions: false,
            redirect: undefined,
        };
        if (args === undefined || args.options === undefined) {
            defaultArgs.options.load();
        }
        const {
            options,
            running,
            clients,
            nicknames,
            games,
            game,
            showOptions,
            redirect,
        } = args || defaultArgs;
        this.options = options;
        this.running = running;
        this.clients = clients;
        this.nicknames = nicknames;
        this.games = games;
        this.showOptions = showOptions;
        this.redirect = redirect;

        if (game === undefined) {
            this.game = games[0];
        } else {
            this.game = game;
        }
    }

    /**
     * Accepts an action from the {@link reducer} and passes it
     * to the appropriate {@link ActionHandler handler} function.
     * @param action
     * @returns Whether or not `this` changed.
     */
    public handleAction(action: ServerStoreAction): boolean {
        debug(action);

        if (action instanceof AddStream) {
            const added = handleAddStream(action);
            return this.updateStreams();
        } else if (action instanceof Disconnect) {
            return this.handleDisconnect(action);
        } else if (action instanceof InfoAction) {
            const { id, payload } = action;
            const changed = this.clients.update(id, payload);
            return this.updateStreams() || changed;
        } else if (action instanceof ToggleElement) {
            switch (action.name) {
                case "showOptions":
                    if (action.on !== undefined) {
                        if (action.on !== this.showOptions) {
                            this.showOptions = action.on;
                            return true;
                        }
                    } else {
                        this.showOptions = !this.showOptions;
                        return true;
                    }
                    break;
            }
            return false;
        } else if (action instanceof SelectGame) {
            if (this.game !== action.game) {
                this.game = action.game;
                return true;
            }
            return false;
        } else if (action instanceof RedirectAction) {
            if (this.redirect !== action.to) {
                this.redirect = action.to;
                return true;
            }
            return false;
        } else if (action instanceof Start) {
            const active = this.clients.getActive();
            const maps: { [clientID: string]: { [key: string]: string } } = {};

            for (const client of active) {
                maps[client] = getMaps(client, this.options, this.nicknames);
            }
            if (this.game !== undefined) {
                startGames(this.game, maps);
            }
            return false;
        } else if (action instanceof Stop) {
            const active = this.clients.getActive();
            const running = new Set(this.clients.getRunning());
            const targets = new Set(active.filter(x => running.has(x)));
            for (const client of Array.from(targets)) {
                EmitStop(client);
            }
            return false;
        } else if (action instanceof SetClients) {
            if (!Object.is(this.clients, action.clients)) {
                this.clients = action.clients;
                return true;
            }
            return false;
        } else if (action instanceof SetClientActive) {
            return this.clients.setActivity(action.id, action.active);
        } else if (action instanceof SetNickname) {
            return this.nicknames.set({
                key: action.id,
                value: action.name,
            });
        } else if (action instanceof ToggleRunning) {
            return false;
            // return !this.running
            //     ? this.handleStart(action)
            //     : this.handleStop(action);
        } else if (action instanceof OptionsStoreAction) {
            return this.options.handleAction(action);
        } else if (action instanceof UpdateGames) {
            if (action.games === this.games) {
                return false;
            }
            this.games = action.games;
            if (this.game === undefined) {
                this.game = this.games[0];
            }
            if (!this.games.includes(this.game)) {
                this.game = this.games[0];
            }
            return true;
        } else {
            return false;
        }
    }

    private handleDisconnect(action: Disconnect): boolean {
        return this.clients.remove(action.id);
    }

    // @todo combine
    private updateStreams(): boolean {
        const clientStreams = this.clients.updateStreams();
        for (const clientStream of clientStreams) {
            const { socketID, streamID } = clientStream;
            const stream = removeStream(streamID);
            this.clients.update(socketID, { stream });
        }
        return clientStreams.length > 0;
    }
}

export const ServerContext = createContext<
    [ServerStore, Dispatch<ServerStoreAction>]
>([
    new ServerStore(),
    () => {
        throw new Error("no reducer has been set");
    },
]);

export function reducer(
    state: ServerStore,
    action: ServerStoreAction,
): ServerStore {
    const newState = new ServerStore(state);

    if (newState.handleAction(action) === true) {
        return newState;
    }
    return state;
}
