import { Action } from "vr-launcher-common";

export class Disconnect implements Action {
    public source: string;
    public type: "disconnect";
    public id: string;

    constructor(source: string, id: string) {
        this.source = source;
        this.type = "disconnect";
        this.id = id;
    }
}
