import { Action } from "vr-launcher-common";

type OptionsStoreActionType = "setAreaSize" | "setHostIP" | "setPlayTime";

export class OptionsStoreAction implements Action {
    public source: string;
    public type: OptionsStoreActionType;
    public payload: any;
    constructor(source: string, type: OptionsStoreActionType, payload?: any) {
        this.source = source;
        this.type = type;
        this.payload = payload;
    }
}
