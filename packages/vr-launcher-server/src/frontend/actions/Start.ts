import { Action } from "vr-launcher-common";

export class Start implements Action {
    public type = "start";
    public source: string;

    constructor(source: string) {
        this.source = source;
    }
}
