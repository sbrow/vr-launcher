import { Action } from "vr-launcher-common";

export class UpdateGames implements Action {
    public source: string;
    public type: "updateGames";
    public games: string[];

    constructor(source: string, games?: string[]) {
        this.source = source;
        this.type = "updateGames";
        this.games = games || [];
    }
}
