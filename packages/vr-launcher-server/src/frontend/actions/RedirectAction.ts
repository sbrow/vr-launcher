import { RedirectTo } from "types";
import { Action } from "vr-launcher-common";

export class RedirectAction implements Action {
    public type = "Redirect";
    public to: RedirectTo;
    public source: string;

    constructor(props: { to: RedirectTo; source: string }) {
        this.to = props.to;
        this.source = props.source;
    }
}
