import { Action, ClientID } from "vr-launcher-common";

import { Nickable } from "frontend/Nicknames";

export class SetNickname implements Action {
    public source: string;
    public type: "setNick";
    public id: Nickable;
    public name: string;

    constructor(source: string, id: Nickable, nickname: string) {
        this.source = source;
        this.type = "setNick";
        this.id = id;
        this.name = nickname;
    }
}
