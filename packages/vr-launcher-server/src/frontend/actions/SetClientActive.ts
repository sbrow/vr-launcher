import { Action, ClientID } from "vr-launcher-common";

export class SetClientActive implements Action {
    public source: string;
    public type: "setClientActive";
    public id: ClientID;
    public active?: boolean;

    constructor(source: string, id: ClientID, active?: boolean) {
        this.source = source;
        this.type = "setClientActive";
        this.id = id;
        this.active = active;
    }
}
