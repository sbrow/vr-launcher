import { Clients } from "frontend/Clients";

export class SetClients {
    public type = "setClients";
    public source: string;
    public clients: Clients;

    constructor(source: string = "?", clients: Clients = new Clients()) {
        this.source = source;
        this.clients = clients;
    }
}
