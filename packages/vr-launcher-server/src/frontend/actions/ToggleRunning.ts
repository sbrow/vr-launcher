export class ToggleRunning {
    public source: string;
    public type = "toggleRunning";

    constructor(source: string = "?") {
        this.source = source;
    }
}
