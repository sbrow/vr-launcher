import { Action, Info } from "vr-launcher-common";

export class InfoAction implements Action {
    public source: string;
    public type: "info";
    public payload: Partial<Info>;
    public id: string;

    constructor(source: string, id: string, payload?: Partial<Info>) {
        this.source = source;
        this.type = "info";
        this.id = id;
        this.payload = payload || {};
    }
}
