import { Action } from "vr-launcher-common";

export class SelectGame implements Action {
    public source: string;
    public type = "selectGame";
    public game: string;

    constructor(game: string, source: string) {
        this.game = game;
        this.source = source;
    }
}
