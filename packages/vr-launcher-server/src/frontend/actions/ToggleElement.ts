import { Action } from "vr-launcher-common";

export class ToggleElement implements Action {
    public type = "toggleElement";
    public name = "showOptions";
    public source: string;
    public on: boolean | undefined;

    constructor(source: string = "", on?: boolean) {
        this.source = source;
        this.on = on;
    }
}
