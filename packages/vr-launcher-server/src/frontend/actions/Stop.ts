import { Action } from "vr-launcher-common";

export class Stop implements Action {
    public type = "stop";
    public source: string;

    constructor(source: string) {
        this.source = source;
    }
}
