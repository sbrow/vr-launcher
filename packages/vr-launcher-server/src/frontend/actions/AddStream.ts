import { Action } from "vr-launcher-common";

export class AddStream implements Action {
    public source: string;
    public type = "AddStream";
    public id: string;
    public stream: MediaStream;

    constructor(props: { source: string; stream: MediaStream; id?: string }) {
        this.source = props.source;
        this.stream = props.stream;
        if (props.id) {
            this.id = props.id;
        } else {
            const tracks = props.stream.getVideoTracks();
            if (tracks.length > 0) {
                this.id = tracks[0].id;
            } else {
                this.id = "";
            }
        }
    }
}
