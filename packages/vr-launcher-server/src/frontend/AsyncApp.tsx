import loadable from "@loadable/component";

export const AsyncApp = (path: string) =>
    loadable(() => import(`frontend/${path}`));

export const AsyncComponent = loadable((props: { path: string }) =>
    import(`frontend/${props.path}`),
);
