import React, { ReactElement, useContext } from "react";
import { Button } from "react-bootstrap";

import { ToggleElement } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

/**
 * Toggles the visibility of the Options Menu.
 *
 * @param props Button content.
 */
export function ToggleOptions(props: {
    children: ReactElement | ReactElement[];
}): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    let source = ToggleOptions.name;
    function onClick() {
        source = `${source}.${onClick.name}`;
        dispatch(new ToggleElement(source, !state.showOptions));
    }
    return <Button onClick={onClick}>{props.children}</Button>;
}
