import { cleanup, fireEvent, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import "@testing-library/jest-dom/extend-expect";
import React from "react";

import { FancyPlayAreaDropdown } from "frontend/FancyStuff";
import { PlayAreaDropdown } from "frontend/options/PlayAreaDropdown";
import { ServerStoreProvider } from "frontend/ServerApp/ServerStoreProvider";
import { unlinkSync } from "fs";
import { dirname, join } from "path";
import {
    TestRender,
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
    TestContextProvider,
} from "testUtils";

jest.mock("frontend/ServerApp/CustomCard", () => ({
    CustomCard: () => <div className="custom-card" />,
}));

let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <PlayAreaDropdown />,
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: {},
    };
    unlinkSync(join(dirname(__dirname), "gameOptions.json"));
});

afterEach(() => {
    cleanup();
});

describe("PlayAreaDropdown", () => {
    describe("render", () => {
        it.skip("matches <FancyPlayAreaDropdown>", () => {
            const { container } = TestRender(TestRouter(props), options);

            const want = render(<FancyPlayAreaDropdown />);
            expect(container).toStrictEqual(want.container);
        });
    });
    it("updates play area size", () => {
        const { getByText, getByLabelText } = render(TestRouter(props), {
            wrapper: ServerStoreProvider,
        });
        const button = getByLabelText("Play Area Size");
        expect(button).toHaveTextContent("12");
        fireEvent.click(button);
        fireEvent.click(getByText("20"));
        expect(button).toHaveTextContent("20");
    });
});
