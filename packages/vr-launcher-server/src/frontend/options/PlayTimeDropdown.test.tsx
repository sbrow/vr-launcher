import { cleanup, fireEvent, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import "@testing-library/jest-dom/extend-expect";
import React from "react";

import { ServerStoreProvider } from "frontend/ServerApp/ServerStoreProvider";
import { unlinkSync } from "fs";
import { dirname, join } from "path";
import {
    TestRenderOptions,
    TestRouter,
    TestRouterProps,
    TestContextProvider,
} from "testUtils";
import { PlayTimeDropdown } from "frontend/options/PlayTimeDropdown";

jest.mock("frontend/ServerApp/CustomCard", () => ({
    CustomCard: () => <div className="custom-card" />,
}));

let props: TestRouterProps;
let options: TestRenderOptions;

beforeEach(() => {
    props = {
        path: "/options",
        history: createMemoryHistory({ initialEntries: ["/"] }),
        child: () => <PlayTimeDropdown />,
    };
    options = {
        // tslint:disable-next-line: object-literal-sort-keys
        wrapper: TestContextProvider,
        wrapperArgs: {},
    };
    unlinkSync(join(dirname(__dirname), "gameOptions.json"));
});

afterEach(() => {
    cleanup();
});

describe("PlayTimeDropdown", () => {
    it("updates play time", () => {
        const { getByText, getByLabelText } = render(TestRouter(props), {
            wrapper: ServerStoreProvider,
        });
        const button = getByLabelText("Play Time");
        expect(button).toHaveTextContent("3");
        fireEvent.click(button);
        fireEvent.click(getByText("10"));
        expect(button).toHaveTextContent("10");
    });
});
