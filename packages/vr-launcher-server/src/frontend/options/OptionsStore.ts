import { existsSync, readFileSync, writeFileSync } from "fs";
import { dirname, join } from "path";

import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";

export class OptionsStore {
    public static filename = join(dirname(__dirname), "gameOptions.json");
    public hostIP: string | undefined;
    public playTime: number;
    public areaSize: number;

    constructor(playTime: number = 3, areaSize: number = 12, hostIP?: string) {
        this.playTime = playTime;
        this.areaSize = areaSize;
        this.hostIP = hostIP;

        if (!existsSync(OptionsStore.filename)) {
            this.save();
        }
    }

    public setAreaSize(size: number): void {
        this.areaSize = size;
    }

    public setHostIP(ip?: string): void {
        this.hostIP = ip;
    }

    public setPlayTime(time: number): void {
        this.playTime = time;
    }

    public save(): void {
        const data = { ...this };
        writeFileSync(OptionsStore.filename, JSON.stringify(data));
    }

    public load(): void {
        const fileData = JSON.parse(
            readFileSync(OptionsStore.filename).toString(),
        );
        const defaults = new OptionsStore();
        const { areaSize, playTime, hostIP } = fileData || defaults;
        this.areaSize = areaSize;
        this.hostIP = hostIP;
        this.playTime = playTime;
    }

    public handleAction(action: OptionsStoreAction): boolean {
        let changed = false;

        switch (action.type) {
            case "setAreaSize":
                this.setAreaSize(action.payload);
                changed = true;
                break;
            case "setHostIP":
                this.setHostIP(action.payload);
                changed = true;
                break;
            case "setPlayTime":
                this.setPlayTime(action.payload);
                changed = true;
                break;
        }
        if (changed) {
            this.save();
        }
        return changed;
    }
}
