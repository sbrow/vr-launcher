import React, { ReactElement, useContext } from "react";
import { Col, Form, FormControlProps, Row } from "react-bootstrap";
import { BsPrefixProps, ReplaceProps } from "react-bootstrap/helpers";

import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export function LocalServerIPForm(): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    const onChange = (
        event: React.FormEvent<
            ReplaceProps<"input", BsPrefixProps<"input"> & FormControlProps>
        >,
    ) => {
        const source = `${LocalServerIPForm.name}.onChange`;
        const action: OptionsStoreAction = {
            source,
            type: "setHostIP",
            // @ts-ignore
            // tslint:disable-next-line: object-literal-sort-keys
            payload: event.target.value,
        };
        dispatch(action);
    };
    return (
        <Form>
            <Form.Group
                as={Row}
                className="justify-content-center align-items-center"
            >
                <Form.Label column className="text-right">
                    Local Server IP
                </Form.Label>
                <Col>
                    <Form.Control
                        type="text"
                        value={state.options.hostIP}
                        onChange={onChange}
                    />
                </Col>
            </Form.Group>
        </Form>
    );
}
