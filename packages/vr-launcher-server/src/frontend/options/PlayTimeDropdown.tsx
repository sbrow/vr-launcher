import { faClock } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, {
    ReactChildren,
    ReactElement,
    useContext,
    ReactChild,
} from "react";
import { LabeledDropdown } from "vr-launcher-common";

import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export interface PlayTimeDropdownProps {
    small?: boolean;
}

export function PlayTimeDropdown(
    props: PlayTimeDropdownProps = { small: false },
): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    const setPlayTime = (
        eventKey: string,
        event: React.SyntheticEvent<{}, Event>,
    ) => {
        const source = `${PlayTimeDropdown.name}.onSelect`;
        const match = eventKey.match(/[^-]*$/);
        const payload = match !== null ? match[0] : undefined;
        if (payload !== undefined) {
            dispatch(new OptionsStoreAction(source, "setPlayTime", payload));
        }
    };
    const childProps: {
        label?: string;
        children?: ReactChild | ReactChildren;
    } = {};
    if (props.small === true) {
        childProps.children = (
            <FontAwesomeIcon
                icon={faClock}
                size="2x"
                fixedWidth
                className="text-white"
            />
        );
    } else {
        childProps.label = "Play Time";
    }
    return (
        <LabeledDropdown
            title={state.options.playTime}
            options={[3, 5, 7, 10]}
            onSelect={setPlayTime}
            labelSizes={{ xs: 4, sm: 4, md: 4 }}
            dropdownSizes={{ xs: 6, sm: 6, md: 6 }}
            className="pt-2"
            {...childProps}
        />
    );
}
