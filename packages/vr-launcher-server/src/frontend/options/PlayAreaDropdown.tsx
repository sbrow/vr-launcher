import { Globe, Icon } from "@githubprimer/octicons-react";
import React, {
    ReactChildren,
    ReactElement,
    useContext,
    ReactChild,
} from "react";
import { LabeledDropdown } from "vr-launcher-common";

import { faGlobe } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export interface PlayAreaDropdownProps {
    small?: boolean;
}

export function PlayAreaDropdown(
    props: PlayAreaDropdownProps = { small: false },
): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    const setAreaSize = (
        eventKey: string,
        event: React.SyntheticEvent<{}, Event>,
    ) => {
        const source = `${PlayAreaDropdown.name}.onSelect`;
        const match = eventKey.match(/[^-]*$/);
        const payload = match !== null ? match[0] : undefined;
        if (payload !== undefined) {
            const action = new OptionsStoreAction(
                source,
                "setAreaSize",
                payload,
            );
            if (process.env.NODE_ENV !== "production") {
                // tslint:disable-next-line: no-console
                console.log(action);
            }
            dispatch(action);
        }
    };
    const childProps: {
        label?: string;
        children?: ReactChild | ReactChildren;
    } = {};

    if (props.small === true) {
        childProps.children = (
            <FontAwesomeIcon
                icon={faGlobe}
                size="2x"
                fixedWidth
                className="text-white"
            />
        );
    } else {
        childProps.label = "Play Area Size";
    }
    return (
        <LabeledDropdown
            title={state.options.areaSize}
            options={[12, 16, 20]}
            onSelect={setAreaSize}
            labelSizes={{ xs: 4, sm: 4, md: 4 }}
            dropdownSizes={{ xs: 6, sm: 6, md: 6 }}
            className=""
            {...childProps}
        />
    );
}
