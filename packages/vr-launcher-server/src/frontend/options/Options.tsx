import Octicon, { Gear } from "@githubprimer/octicons-react";
import React, { ReactElement, useContext } from "react";
import { Container, Modal } from "react-bootstrap";

import { ToggleElement } from "frontend/actions";
import { GameDropdown } from "frontend/options/GameDropdown";
import { LocalServerIPForm } from "frontend/options/LocalServerIPForm";
import { PlayAreaDropdown } from "frontend/options/PlayAreaDropdown";
import { PlayTimeDropdown } from "frontend/options/PlayTimeDropdown";
import { ToggleOptions } from "frontend/options/ToggleOptions";
import { ServerContext } from "frontend/ServerApp/ServerStore";

export function Options(): ReactElement {
    const [state, dispatch] = useContext(ServerContext);

    const onHide = () => {
        dispatch(
            new ToggleElement(`${Options.name}.onHide`, !state.showOptions),
        );
    };

    return (
        <>
            <ToggleOptions>
                <Octicon icon={Gear} size="medium" ariaLabel="Options" />
            </ToggleOptions>
            <Modal show={state.showOptions} size="lg" onHide={onHide}>
                <Modal.Header closeButton />
                <Modal.Body>
                    <Container>
                        <LocalServerIPForm />
                        <GameDropdown />
                        <PlayTimeDropdown />
                        <PlayAreaDropdown />
                    </Container>
                </Modal.Body>
            </Modal>
        </>
    );
}
