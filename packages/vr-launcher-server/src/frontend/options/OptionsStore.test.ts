import { OptionsStoreAction } from "frontend/actions/OptionsStoreAction";
import { Client } from "frontend/Clients";
import { OptionsStore } from "frontend/options/OptionsStore";
import { source } from "testUtils";

class MediaStream {
    constructor(public id: string) {
        this.id = id;
    }
}

let state: OptionsStore;
let someClient: Client;

beforeEach(() => {
    state = new OptionsStore(3, 12);
    someClient = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "127.0.0.1",
        id: "1",
        status: "connected",
        name: "",
    };
});

describe("OptionsStore", () => {
    describe("handleAction", () => {
        it("updates areaSize", () => {
            const action = new OptionsStoreAction(source, "setAreaSize", 20);
            const got = state.handleAction(action);
            expect(state.areaSize).toBe(20);
            expect(got).toBe(true);
        });
    });
});
