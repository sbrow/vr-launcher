import React, { ReactElement, useContext, useEffect } from "react";
import { ipcSend, LabeledDropdown } from "vr-launcher-common";

import { SelectGame, UpdateGames as UpdateGamesAction } from "frontend/actions";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { RouteComponentProps, withRouter } from "react-router";

export function gameDropdown(props: RouteComponentProps): ReactElement {
    const [state, dispatch] = useContext(ServerContext);
    let source = GameDropdown.name;
    useEffect(() => {
        const updateGames = (event: any, games: string[]) => {
            if (JSON.stringify(state.games) !== JSON.stringify(games)) {
                const action = new UpdateGamesAction(source, games);
                dispatch(action);
            }
        };
        ipcSend("games.request", {
            callback: updateGames,
            event: "games.response",
        });
    });
    const onSelect = (
        eventKey: string,
        event: React.SyntheticEvent<{}, Event>,
    ) => {
        source = `${source}.selectGame`;
        const match = eventKey.match(/[^-]*$/);
        const payload = match !== null ? match[0] : undefined;
        if (payload !== undefined) {
            props.history.push({
                pathname: "/options",
                search: `?game=${payload}`,
            });
        }
    };
    const title = state.game || "game_1";
    return (
        <LabeledDropdown
            label="Game"
            title={title}
            options={state.games}
            onSelect={onSelect}
        />
    );
}

export const GameDropdown = withRouter(gameDropdown);
