import { Client, Clients } from "frontend/Clients";
import { someOtherSocketID, someSocketID } from "testUtils";

describe("Clients", () => {
    let clients = new Clients();
    let someClient: Client = {
        // tslint:disable-next-line: object-literal-sort-keys
        localIP: "127.0.0.1",
        id: "1",
        status: "connected",
        name: "",
    };
    beforeEach(() => {
        clients = new Clients();
        someClient = {
            // tslint:disable-next-line: object-literal-sort-keys
            localIP: "127.0.0.1",
            id: "1",
            status: "connected",
            name: "",
        };
    });
    describe("getSocketID", () => {
        it("should return correct socket id when passed a valid ClientID", () => {
            const want = "12";
            clients.set(want, someClient);

            const got = clients.getSocketID(someClient.id);
            expect(got).toBe(want);
        });
    });
    describe("remove", () => {
        it("should not remove client when they don't exist", () => {
            clients.set(someOtherSocketID, someClient);
            const changed = clients.remove(someSocketID);
            expect(changed).toBe(false);

            const got = clients.get(someClient.id);
            expect(got).toBeDefined();
        });
        it("should remove client when they exist", () => {
            clients.set(someSocketID, someClient);
            const changed = clients.remove(someSocketID);
            expect(changed).toBe(true);

            const got = clients.get(someClient.id);
            expect(got).toBeUndefined();
        });
    });
    describe("update", () => {
        it("Should return true on creating a new client", () => {
            const changed = clients.update(someClient.localIP, someClient);
            expect(changed).toBe(true);
        });
        it("should create a new client if none exists", () => {
            clients.update(someClient.localIP, someClient);
            const got = clients.keys[someClient.localIP];

            expect(got).toMatchObject(someClient);
        });
    });
});
