///<reference types="socket.io" />

declare module "*.png" {
    const path: string;
    export default path;
}
declare module "*.jpg" {
    const path: string;
    export default path;
}

declare module "socket.io-p2p-server" {
    export function Server(
        socket: SocketIO.Socket,
        fn: (err?: any) => void,
    ): void;
}

declare namespace NodeJS {
    interface RInfo {
        address: string;
        family: string;
        port: string;
        size: string;
    }
}

// From Octicon
declare namespace Octicon {
    export type Size = "small" | "medium" | "large";
}

declare interface Window {
    process?: {
        type?: string;
    };
}
