/**
 * Test specific code- including constants, Mocks and custom renderers.
 */

/**
 *
 */
import { render, RenderOptions, RenderResult } from "@testing-library/react";
import React, { Dispatch, ReactElement } from "react";
import { Redirect, Route, RouteComponentProps, Router } from "react-router-dom";

import { Party } from "frontend/PartyQueue/Party";
import { ServerContext } from "frontend/ServerApp/ServerStore";
import { createMemoryHistory, MemoryHistory } from "history";

export const someSocketID = "12";
export const someOtherSocketID = "123";
export const source = "test";
export const games = ["game 1", "game 2"];
export const someParty: Party = { name: "Some Party", players: 4 };

export interface TestRouterProps {
    path: string;
    history?: MemoryHistory<any>;
    child:
        | React.ComponentType<RouteComponentProps<any>>
        | React.ComponentType<any>;
}

/**
 * Renders a router whose default path redirects to `props.path`.
 * @param props The component to render and the path at which to render it.
 */
export function TestRouter(props: TestRouterProps): ReactElement {
    function Home(): ReactElement {
        if (props.history !== undefined) {
            if (props.history.index > 0) {
                return <p>Welcome!</p>;
            }
        }
        return <Redirect push to={props.path} />;
    }
    return (
        <Router history={props.history || createMemoryHistory()}>
            <Route exact path="/" component={Home} />
            <Route path={props.path} component={props.child} />
        </Router>
    );
}

export function TestContextProvider(props: {
    children?: any;
    context?: React.Context<any>;
    state: any;
    dispatch: Dispatch<any>;
}): ReactElement {
    const TestContext = props.context || ServerContext;
    return (
        <TestContext.Provider value={[props.state, props.dispatch]}>
            {...props.children}
        </TestContext.Provider>
    );
}

export interface TestRenderOptions extends RenderOptions {
    wrapperArgs: any;
}

/**
 * Like {@link @testing-library/react:render}, but the wrapper parameter can be
 * passed arguments.
 */
export function TestRender(
    component: ReactElement,
    options: TestRenderOptions,
): RenderResult {
    if ("wrapperArgs" in options) {
        if ("wrapper" in options && options.wrapper !== undefined) {
            const args = {
                children: component,
                ...options.wrapperArgs,
            };
            return render(options.wrapper(args));
        }
    }
    return render(component, options);
}
