import { WebContents } from "electron";
import { Info } from "vr-launcher-common";

import { Logger } from "backend/Logger";
import { Client } from "frontend/Clients";

export interface ClientData {
    socketID: string;
    client: Client | undefined;
}

/**
 * @returns A partial Info object containing the fields in `dat`
 * that are not present in `client`.
 *
 * @todo Move checks to backend- part of changing to incremental updates.
 */
function filterInfo(client: Client | undefined, dat: Info): Partial<Info> {
    if (client === undefined) {
        return dat;
    }

    const ret: Partial<Info> = {};
    const keys = Object.keys(dat) as Array<keyof Info>;
    for (const key of keys) {
        if (dat[key] !== undefined && client[key] !== dat[key]) {
            // @ts-ignore
            ret[key] = dat[key];
        }
    }
    return ret;
}

/**
 * Keeps track of client statuses and emits events
 * when they change.
 *
 * @remarks
 * Client uniqueness is checked on insertion.
 */
export class ClientManager {
    public window?: WebContents;
    private clients: ClientData[];

    constructor(window?: WebContents) {
        this.clients = [];
        this.window = window;
    }

    public setMain(window: WebContents) {
        this.window = window;
    }

    /**
     * Adds a client with the given `socketID` to the manager,
     * unless another client with `socketID` already exists.
     */
    public add(socketID: string, client?: Client): void {
        const newClient: ClientData = {
            socketID,
            client,
        };
        for (const c of this.clients) {
            if (c.socketID === socketID) {
                Logger.error(
                    `Can't add client with id "${socketID}" to ClientManager: \
client with id already exists`,
                );
            }
        }
        this.clients.push(newClient);
    }

    /**
     * Removes the client with id `socketID` from the manager.
     *
     * @returns Whether or not a client was removed.
     */
    public remove(socketID: string): boolean {
        let removed: boolean = false;
        this.clients.forEach((item, index) => {
            if (item.socketID === socketID) {
                this.clients.splice(index, 1);
                removed = true;
                return;
            }
        });
        if (this.window !== undefined) {
            Logger.debug(`emitting: "disconnect(${socketID})"`);
            this.window.send("disconnect", socketID);
        }
        return removed;
    }

    /**
     * Updates client information for the client with the given `socketID`.
     *
     * @remarks
     * No action is taken if a matching client isn't found.
     *
     * @param socketID The id of the client to update.
     * @param info The `Info` event that triggered the update.
     */
    public update(socketID: string, info: Info) {
        const [client, index] = this.getWithIndex(socketID);

        if (client !== undefined) {
            const source = `${ClientManager.name}.${this.update.name}`;

            const payload = filterInfo(client.client, info);
            if (index !== undefined) {
                const keys = Object.keys(payload) as Array<keyof Partial<Info>>;
                const realClient = this.clients[index];
                if (realClient.client !== undefined) {
                    for (const key of keys) {
                        // @ts-ignore
                        realClient.client[key] = payload[key];
                    }
                } else {
                    realClient.client = info;
                }
            }
            if (Object.keys(payload).length > 0 && this.window !== undefined) {
                Logger.log(`emitting ${JSON.stringify(payload)} ${socketID}`);
                this.window.send("info", payload, socketID);
            }
        }
    }

    /**
     * @param socketID
     * @returns A copy of the client with `socketID` equal to `socketID`.
     */
    public get(socketID: string): ClientData | undefined {
        return this.getWithIndex(socketID)[0];
    }

    /**
     * Same as {@link ClientManager.get}, but also returns
     * the index of the client.
     *
     * @param socketID the ID of the client to return
     * @returns A copy of the client with `socketID` equal to `socketID`
     * and its index.
     * @memberof ClientManager
     */
    private getWithIndex(
        socketID: string,
    ): [ClientData | undefined, number | undefined] {
        for (let i = 0; i < this.clients.length; i++) {
            const client = this.clients[i];
            if (client.socketID === socketID) {
                return [{ ...client }, i];
            }
        }
        return [undefined, undefined];
    }
}
