import { IPCManager } from "backend/IPCManager";
import { Server as MulticastServer } from "backend/multicast";
import { WebContents } from "electron";
import { Multicast } from "vr-launcher-common";

export function setupBackend(webContents: WebContents) {
    const multi = new MulticastServer(Multicast.address);
    const manager = new IPCManager(webContents);
}
