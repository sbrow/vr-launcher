import { Info } from "vr-launcher-common";

import { ClientManager } from "backend/ClientManager";
import { someSocketID } from "testUtils";

describe("ClientManager", () => {
    let manager: ClientManager;

    beforeEach(() => {
        manager = new ClientManager();
        manager.add(someSocketID);
    });
    describe("remove", () => {
        it("should remove client when id matches", () => {
            manager.remove(someSocketID);
            // @ts-ignore
            expect(manager.clients.length).toEqual(0);
        });
    });
    describe("get", () => {
        it("should return client", () => {
            const client = manager.get(someSocketID);
            expect(client).not.toBeUndefined();
            // @ts-ignore
            expect(client.client).toBeUndefined();
            // @ts-ignore
            expect(client.socketID).toBe(someSocketID);
        });
    });
    describe("update", () => {
        it("should update existing client", () => {
            const info: Info = {
                localIP: "127.0.0.1",
                id: "1",
                status: "connected",
            };
            manager.update(someSocketID, info);

            const clientData = manager.get(someSocketID);
            expect(clientData).not.toBeUndefined();
            // @ts-ignore
            const got = clientData.client;
            expect(got).toMatchObject(info);
        });
    });
});
