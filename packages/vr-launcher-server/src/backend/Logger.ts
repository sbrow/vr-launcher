import logger from "electron-log";

export const Logger = logger;

if (process.env.NODE_ENV === "test") {
    Logger.transports.console.level = false;
}
