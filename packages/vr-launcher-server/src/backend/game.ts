/**
 * Converts game info passed from the server into commands
 * readable by the client.
 */

/**
 *
 */
import { SpawnOptionsWithoutStdio } from "child_process";
import { readFileSync } from "fs";
import { join } from "path";
import { ClientID, Start } from "vr-launcher-common";
import YAML from "yaml";

import { Logger } from "backend/Logger";

/**
 * @todo SpawnArgs
 */

/**
 * Arguments accepted by `child_process.spawn`.
 */
interface SpawnArgs {
    cmd: string;
    args?: string[];
    options?: SpawnOptionsWithoutStdio;
}

/**
 * @todo move ClientStart to `vr-launcher-common`.
 */
interface ClientStart extends SpawnArgs {
    id: ClientID;
}

/**
 * @returns all available games.
 */
export function getGames() {
    const filePath = join(__dirname, "games.yml");
    const file = readFileSync(filePath);
    const games = YAML.parse(file.toString());
    return Object.keys(games);
}

/**
 * Removes undefined flags from a list of commandline arguments.
 */
export function clean(args?: string[]): string[] {
    const source = clean.name;

    if (args === undefined) {
        return [];
    }
    try {
        const newArgs = [...args];
        Logger.debug("args", { args, source });
        for (let i = 0; i < newArgs.length; i += 2) {
            if (i + 1 < newArgs.length) {
                const name = newArgs[i];
                const value = newArgs[i + 1];
                if (name.match(/^-/) && value.match(/^\$/)) {
                    newArgs[i] = "";
                    newArgs[i + 1] = "";
                }
            }
        }
        Logger.debug("newArgs", { args, source });
        const filtered = newArgs.filter((value: string) => value !== "");
        Logger.debug("filtered", { args, source });
        return filtered;
    } catch (error) {
        Logger.error(error, { source });
        return [];
    }
}

export function getGameInfo(event: Start): ClientStart | undefined {
    try {
        const { id, name, keys } = event;
        const filePath = join(__dirname, "games.yml");
        const file = readFileSync(filePath);
        const games = YAML.parse(file.toString());
        Logger.debug(games);

        const gameInfo: ClientStart = {
            // tslint:disable-next-line: object-literal-sort-keys
            cmd: "",
            args: [],
            id,
        };

        for (const gameName of Object.keys(games)) {
            if (gameName === name) {
                const info = games[name];
                Logger.debug(info);
                if (typeof info === "string") {
                    gameInfo.cmd = info;
                } else if (typeof info === "object") {
                    if ("cmd" in info) {
                        gameInfo.cmd = info.cmd;
                    }
                    let args: string[] = [];
                    if ("args" in info) {
                        args = args.concat(info.args);
                    }
                    if (id === "SERVER" && "serverArgs" in info) {
                        args = args.concat(info.serverArgs);
                    }
                    for (let arg of args) {
                        if (typeof arg !== "string") {
                            arg = JSON.stringify(arg);
                        }
                        if (gameInfo.args) {
                            if (keys.hasOwnProperty(arg)) {
                                gameInfo.args.push(keys[arg]);
                            } else {
                                gameInfo.args.push(arg);
                            }
                        }
                    }
                }
            }
        }
        Logger.debug("cleaning", gameInfo);
        gameInfo.args = clean(gameInfo.args);
        Logger.debug("returning", gameInfo);
        return gameInfo;
    } catch (error) {
        Logger.error(error, { source: getGameInfo.name });
        return undefined;
    }
}
