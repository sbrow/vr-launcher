import { getGameInfo } from "backend/game";
import { Logger } from "backend/Logger";
import { port } from "backend/port";
import { Server } from "backend/Server";
import { Event, ipcMain, WebContents } from "electron";
import { ClientID, Start } from "vr-launcher-common";

const server = new Server(port());

/**
 * Manages communication between the main window and the socket.io server.
 */
export class IPCManager {
    constructor(webContents: WebContents) {
        server.clients.setMain(webContents);
        this.handleEvents();
    }

    public getInfo(event: Event): void {
        server.socket.emit("get info");
    }
    public start(event: Event, payload: Start): void {
        Logger.debug("received", { payload, source: "Server.start" });
        const emitPayload = getGameInfo(payload);
        if (emitPayload !== undefined) {
            Logger.debug("emitting", { payload: emitPayload });
            server.socket.emit("start", emitPayload);
        }
    }

    public stop(event: Event, id: ClientID): void {
        Logger.debug("received", { event: "stop" });
        server.socket.emit("stop", id);
    }

    public getStream(event: Event, socketID: string): void {
        server.socket.to(socketID).emit("get stream");
    }

    public sendKeys(event: Event, args: [string, ClientID]): void {
        const [keys, id] = args;
        Logger.debug("sending", { to: id, event: "sendKeys", payload: keys });
        server.socket.to(id).emit("sendKeys", keys);
    }

    public handleEvents(): void {
        ipcMain.on("start", this.start);
        ipcMain.on("stop", this.stop);
        ipcMain.on("get stream", this.getStream);
        ipcMain.on("sendKeys", this.sendKeys);
        ipcMain.on("get info", this.getInfo);
    }
}
