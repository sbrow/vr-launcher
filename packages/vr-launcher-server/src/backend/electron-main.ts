import {
    app,
    BrowserWindow,
    BrowserWindowConstructorOptions,
    ipcMain,
} from "electron";
import installExtension, {
    REACT_DEVELOPER_TOOLS,
} from "electron-devtools-installer";
import { autoUpdater } from "electron-updater";

import { getGames } from "backend/game";
import { Logger } from "backend/Logger";
import { setupBackend } from "backend/setupBackend";

autoUpdater.logger = Logger;

let mainWindow: Electron.BrowserWindow;

function onReady() {
    let fullscreen = false;
    if (process.env.NODE_ENV === "production") {
        fullscreen = true;
    }
    autoUpdater.checkForUpdatesAndNotify();
    const opts: BrowserWindowConstructorOptions = {
        width: 800,
        height: 600,
        fullscreen,
        fullscreenable: true,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: true,
        },
    };
    mainWindow = new BrowserWindow(opts);
    setupBackend(mainWindow.webContents);

    const fileName = `file://${__dirname}/index.html`;
    mainWindow.loadURL(fileName);
    mainWindow.on("close", () => app.quit());

    if (process.env.NODE_ENV !== "production") {
        installExtension(REACT_DEVELOPER_TOOLS)
            .then((name: string) => {
                Logger.info(`Added Extension:  ${name}`);
            })
            .catch((err: string | Error) => {
                Logger.info("An error occurred: ", err);
            });
    }
}

ipcMain.on("games.request", (event: any) => {
    const games = getGames();
    event.sender.send("games.response", games);
});

app.on("ready", () => onReady());
app.on("window-all-closed", () => app.quit());
Logger.info(`Electron Version ${app.getVersion()}`);
