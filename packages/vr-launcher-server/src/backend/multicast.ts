import { Logger } from "backend/Logger";
import ip from "ip";
import { Multicast } from "vr-launcher-common";

export class Server {
    private address: string;

    constructor(address: string) {
        this.address = address;
        Multicast.socket.bind(Multicast.port);
        Multicast.socket.on("listening", this.listening);
        Multicast.socket.on("message", this.onMessage);
    }

    public listening = () => {
        Multicast.socket.addMembership(this.address, ip.address());
        const address = Multicast.socket.address();
        const addr =
            typeof address === "string"
                ? address
                : `${address.address}:${address.port}`;
        Logger.info(`UDP socket listening on ${addr} pid: ${process.pid}`);
    };

    public onMessage = (message: string, rInfo: any) => {
        try {
            const messageData = JSON.parse(message.toString());
            if ("type" in messageData && messageData.type === "request") {
                const port =
                    process.env.npm_config_port ||
                    process.env.npm_package_config_port ||
                    process.env.PORT ||
                    3000;
                Multicast.send(
                    {
                        type: "response",
                        uri: `ws://${ip.address()}:${port}`,
                    },
                    rInfo,
                );
            }
        } catch (error) {
            console.error(error);
        }
        Logger.info(message.toString());
    };
}
