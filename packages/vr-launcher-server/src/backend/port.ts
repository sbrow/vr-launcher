export function port() {
    return (
        process.env.npm_config_port ||
        process.env.npm_package_config_port ||
        process.env.PORT ||
        3000
    );
}
