/**
 * Backend for the Server application.
 */

/**
 *
 */
import { WebContents } from "electron";
import io from "socket.io";
import { Server as P2PServer } from "socket.io-p2p-server";
import { Info } from "vr-launcher-common";

import { ClientManager } from "backend/ClientManager";
import { Logger } from "backend/Logger";
/**
 * Handles Socket.io Server, and multicast broadcasting of same.
 */
export class Server {
    public socket: io.Server;
    public clients: ClientManager;

    constructor(port: number | string | io.Server, main?: WebContents) {
        switch (typeof port) {
            case "string":
                port = Number(port);
            case "number":
                this.socket = io(port); // , { transports: ["websocket"] });
                break;
            default:
                this.socket = port;
        }
        this.socket.use(P2PServer);
        this.clients = new ClientManager(main);
        this.register();
    }

    private register(): void {
        this.socket.on("connection", socket => {
            this.handleClients(socket);
        });
    }

    private handleClients(socket: io.Socket) {
        Logger.debug("received", { event: "connect", socket: socket.id });
        this.clients.add(socket.id);

        socket.on("disconnect", () => {
            Logger.debug("received", {
                event: "disconnect",
                socket: socket.id,
            });

            if (!this.clients.remove(socket.id)) {
                Logger.error(
                    `socket with id ${socket.id} was not \
removed from ClientManager`,
                );
            }
            this.socket.emit("info");
        });
        Logger.debug("requesting", { event: "info" });
        socket.on("info", (data: Info) => {
            Logger.debug("received", { event: "info", socket: socket.id });
            this.handleInfo(data, socket.id);
        });
        socket.emit("get info");
    }

    private handleInfo(data: Info, socketID: string): void {
        Logger.debug("received", { event: "info", payload: data });
        let { id } = data;

        if (typeof id === "boolean") {
            if (id === true) {
                id = "SERVER";
            } else {
                id = "1";
            }
        }
        const payload = { ...data, id };
        this.clients.update(socketID, payload);
    }
}
