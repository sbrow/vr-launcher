export type RedirectTo =
    | string
    | {
          pathname: string;
          search?: string;
      };
