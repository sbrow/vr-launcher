var extensions = ["ts", "tsx", "js", "jsx"];
var config = {
    collectCoverage: true,
    collectCoverageFrom: [
        "src/**/*.{" + extensions.join(",") + "}",
        "!src/**/*.d.ts",
    ],
    globals: {
        "ts-jest": {
            diagnostics: {
                warnOnly: true,
            },
        },
    },
    moduleFileExtensions: extensions,
    moduleDirectories: ["node_modules", "src", "__test__", "__setup__"],
    moduleNameMapper: {
        "\\.(css|less)$": "<rootDir>/__mocks__/styleMock.js",
    },
    setupFilesAfterEnv: [
        // `<rootDir>/__setup__/enzyme.js`,
    ],
    // testRegex: "(/__tests__/.*(\\.|/)(test|spec))\\.[jt]sx?$",
    transform: {
        ".(ts|tsx)$": "ts-jest",
    },
    verbose: true,
};
module.exports = config;
