const extensions = ["ts", "tsx", "js", "jsx"];

module.exports = {
    collectCoverageFrom: [`src/**/*\.{${extensions.join(",")}}`],
    moduleFileExtensions: extensions,
    moduleDirectories: ["node_modules", "src", "__test__", "__setup__"],
    moduleNameMapper: {
        "\\.(css|less)$": "<rootDir>/__mocks__/styleMock.js",
    },
    setupFilesAfterEnv: [
        // `<rootDir>/__setup__/enzyme.js`,
    ],
    // testRegex: "(/__tests__/.*(\\.|/)(test|spec))\\.[jt]sx?$",
    transform: {
        ".(ts|tsx)$": "ts-jest",
    },
    verbose: true,
};
