// @ts-check
// tslint:disable no-console

const chalk = require("chalk");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const nodeExternals = require("webpack-node-externals");
const path = require("path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const webpack = require("webpack");

process.env.NODE_ENV = process.env.NODE_ENV || "development";
const mode = process.env.NODE_ENV;
const outDir = path.resolve(__dirname, "app");
const templateContent = `<html>
  <body>
    <div id="root"></div>
  </body>
</html>`;

console.log(`NODE_ENV=${chalk.yellow(process.env.NODE_ENV)}`);
console.log(`Building for ${chalk.yellow(mode)}...`);

const baseConfig = {
    mode,
    node: {
        __dirname: false,
    },
    module: {
        rules: [
            {
                exclude: [/app/, /build/, /dist/, /node_modules/],
                test: /\.tsx?$/,
                use: "ts-loader",
            },
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"],
            },
        ],
    },
    plugins: [new webpack.EnvironmentPlugin(["NODE_ENV"])],
    resolve: {
        plugins: [new TsconfigPathsPlugin()],
        extensions: [".js", ".ts", ".tsx", ".jsx", ".json"],
    },
    output: {
        path: outDir,
        filename: "[name].js",
    },
};

module.exports = [
    {
        ...baseConfig,
        target: "electron-main",
        entry: { electron: path.join(__dirname, "src", "electron-main.ts") },
        externals: [
            nodeExternals({
                whitelist: /^((?!public-ip).*)$/,
            }),
        ],
    },
    {
        ...baseConfig,
        target: "electron-renderer",
        entry: { main: path.join(__dirname, "src", "index.tsx") },
        plugins: [
            new HtmlWebpackPlugin({
                filename: "index.html",
                templateContent,
            }),
        ],
        externals: [
            nodeExternals({
                whitelist: /^((?!battery-level).)*$/,
            }),
        ],
    },
    {
        ...baseConfig,
        target: "electron-renderer",
        entry: {
            main: path.join(__dirname, "src", "StreamingApp", "index.tsx"),
        },
        output: {
            ...baseConfig.output,
            path: path.join(outDir, "stream"),
        },
        plugins: [
            new HtmlWebpackPlugin({
                filename: "index.html",
                templateContent,
            }),
        ],
    },
];
