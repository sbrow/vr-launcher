// @ts-check
const fs = require("fs");
const dirname = require("path").dirname;
const join = require("path").join;
const resolve = require("path").resolve;

const rootDir = dirname(resolve(__dirname));
const packageJson = require(join(rootDir, "package.json"));

const outPaths = [
    resolve(rootDir, packageJson.directories.lib, "package.json"),
];

function makeDir(path) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
}

function makePackageJson(path, base) {
    const { name, version, author, description, repository, dependencies } = base;

    if (!fs.existsSync(path)) {
        fs.writeFileSync(path, "{}");
    }
    const appPackageJson = require(path);
    const shortName = name.replace("vr-launcher-", "");
    const newAppPackageJson = {
        ...appPackageJson,
        name: shortName,
        author,
        version,
        description,
        main: "electron.js",
        dependencies,
    };

    fs.writeFileSync(path, JSON.stringify(newAppPackageJson, null, 4));
}

makeDir(resolve(rootDir, "app"));
for (const p of outPaths) {
    makePackageJson(p, packageJson);
}
