declare module "sendkeys" {
    /**
     * Send a string of key press events to the OS.
     * @param keys The keypress(es) to send.
     */
    export default function sendKeys(keys: string);
}
