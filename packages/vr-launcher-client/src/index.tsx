import React from "react";
import ReactDOM from "react-dom";

import { ClientApp } from "ClientApp/ClientApp";
import "index.scss";

ReactDOM.render(<ClientApp />, document.querySelector("#root"));
