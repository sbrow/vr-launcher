let interval: number | undefined;

export function startSearching(callback: (...args: any) => any, int: number) {
    if (interval === undefined) {
        // @ts-ignore
        interval = setInterval(callback, int);
        callback();
    }
}

export function stopSearching() {
    clearInterval(interval);
    interval = undefined;
}
