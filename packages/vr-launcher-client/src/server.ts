import logger from "electron-log";
import { readFileSync, writeFileSync } from "fs";
import ip from "ip";
import { dirname, join } from "path";
import publicIp from "public-ip";
import { ClientID } from "vr-launcher-common";

import { clearServer, getServer, search as searchForServer } from "multicast";

export function search(): void {
    return searchForServer();
}

export function findServer(): string {
    const ret = getServer();
    clearServer();
    return ret;
}

export function localIP(): string {
    return ip.address();
}

export async function externalIP(): Promise<string> {
    return await publicIp.v4();
}

const filename = join(dirname(__dirname), "client-info.json");

function getClientData(): string {
    try {
        return readFileSync(filename).toString();
    } catch (error) {
        logger.error(error);
        return "";
    }
}

export function setID(id: ClientID) {
    let contents = getClientData();
    if (contents === "") {
        contents = JSON.stringify({ id: "1" });
    }
    const data = JSON.parse(contents);
    data.id = id;
    writeFileSync(filename, JSON.stringify(data));
}

export function getID(): ClientID {
    const contents = getClientData();
    if (contents === "") {
        setID("1");
    }
    const data = JSON.parse(getClientData());
    return data.id;
}
