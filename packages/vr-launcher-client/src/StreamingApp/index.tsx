import { Event } from "electron";
import { Logger } from "Logger";
import { ipcOn, ipcSendAsync } from "vr-launcher-common";

import { startStream, stopStream, streamID } from "StreamingApp/p2psocket";

let streaming = false;
async function onStart(event: Event, uri: URL): Promise<void> {
    if (streamID !== undefined) {
        // @ts-ignore
        ipcSendAsync("stream id", streamID);
    }
    if (streaming === false) {
        streaming = true;
        Logger.log(`Starting stream @${uri}`);
        try {
            const streamID = await startStream(uri);
            Logger.info(`Sending id: ${streamID}`);
            // @ts-ignore
            ipcSendAsync("stream id", streamID);
        } catch (err) {
            Logger.error(err);
        }
    }
}

function onStop(event: Event): void {
    stopStream();
}

// @ts-ignore
ipcOn("start stream", onStart);
// @ts-ignore
ipcOn("stop stream", onStop);
