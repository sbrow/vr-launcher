import { desktopCapturer } from "electron";
import io from "socket.io-client";
import P2P from "socket.io-p2p";

import { Logger } from "Logger";

export let streamID: string | undefined;
let socket: SocketIOClient.Socket | undefined;
let p2p: P2P | undefined;

export async function startStream(uri: URL): Promise<string | undefined> {
    try {
        const sources = await desktopCapturer.getSources({ types: ["screen"] });
        const media: MediaStreamConstraints = {
            video: {
                // @ts-ignore
                mandatory: {
                    chromeMediaSource: "desktop",
                    // @ts-ignore
                    chromeMediaSourceId: sources[0].id,
                    maxWidth: 1280,
                    maxHeight: 720,
                },
            },
        };
        try {
            const stream = await navigator.mediaDevices.getUserMedia(media);
            console.log(`uri: ${uri}`);
            socket = io(uri);
            // @ts-ignore
            p2p = new P2P(socket, { peerOpts: { stream } });

            if (p2p !== undefined) {
                p2p.on("ready", function() {
                    console.log('got "ready"');
                    if (p2p !== undefined) {
                        p2p.usePeerConnection = true;
                    }
                });

                // @ts-ignore
                p2p.emit("ready", { peerId: p2p.peerId });
            }
            const tracks = stream.getVideoTracks();
            if (tracks.length > 0) {
                streamID = tracks[0].id;
                return streamID;
            }
        } catch (err) {
            Logger.error(err);
        }
    } catch (err) {
        const error = new Error(err);
        Logger.error(error);
    }
    return;
}

export function stopStream(): void {
    socket = undefined;
    p2p = undefined;
    streamID = undefined;
}
