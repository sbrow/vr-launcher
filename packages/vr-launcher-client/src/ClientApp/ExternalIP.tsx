import React, { useContext, useEffect } from "react";
import { IPString } from "vr-launcher-common";

import { ClientContext } from "ClientApp/ClientStore";
import { externalIP } from "server";

export function ExternalIP(): JSX.Element {
    const [state, dispatch] = useContext(ClientContext);
    useEffect(() => {
        const source = `${ExternalIP.name}.useEffect`;
        (async () => {
            try {
                const ip = await externalIP();
                if (state.externalIP !== ip) {
                    dispatch({ source, type: "setExternalIP", payload: ip });
                }
            } catch (error) {}
        })();
    });
    return <IPString label="External" ip={state.externalIP} />;
}
