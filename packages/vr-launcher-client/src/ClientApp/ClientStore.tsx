import { ChildProcessWithoutNullStreams, execSync, spawn } from "child_process";
import React, { createContext } from "react";
import { Action, ClientID, ClientStatus, Info } from "vr-launcher-common";

import { SetID, UpdateBattery } from "actions";
import { SetStreamID } from "actions/SetStreamID";
import { getID, localIP as getLocalIP, setID } from "server";
import { connect, ConnStatus, EmitInfo } from "socket";

/**
 * Logs a message to the console when $NODE_ENV !== "production"
 *
 * @remarks
 * has to be a local function in order to be removed by
 * Webpack.
 *
 * @param message
 */
function log(message: any) {
    if (process.env.NODE_ENV !== "production") {
        console.log(message);
    }
}

/**
 * Logs an error to the console when $NODE_ENV !== "production"
 *
 * @remarks has to be a local function in order to be removed by
 * Webpack.
 *
 * @param error
 */
function error(err: string | Error) {
    if (process.env.NODE_ENV !== "production") {
        console.error(err);
    }
}

/**
 * Store for the `ClientApp`.
 */
export class ClientStore {
    public id: ClientID;
    public localIP: string;
    public externalIP: string;
    public manualServerIP: string;
    public process: ChildProcessWithoutNullStreams | undefined;
    public conn: string;
    public battery: number | undefined;
    public streamID: string | undefined;

    constructor(
        localIP: string = "",
        externalIP: string = "Calculating...",
        proc?: ChildProcessWithoutNullStreams,
        manualServerIP: string = "192.168.1.136",
        battery?: number,
        streamID?: string,
    ) {
        this.id = "1";
        this.localIP = localIP;
        this.process = proc;
        this.conn = ConnStatus();
        this.externalIP = externalIP;
        this.manualServerIP = manualServerIP;
        this.battery = battery;
        this.localIP = getLocalIP();
        this.id = getID();
        this.streamID = streamID;
    }

    public onInfo(): Info {
        const { localIP, id } = this;
        return {
            localIP,
            id,
            status: this.status(),
            battery: this.batteryLevel(),
            streamID: this.streamID,
        };
    }

    public status(): ClientStatus {
        if (this.process !== undefined) {
            return "running";
        }
        return "ready";
    }

    public batteryLevel(): number | undefined {
        return this.battery;
    }
}

export const ClientContext = createContext<
    [ClientStore, React.Dispatch<ClientStoreAction>]
>([
    new ClientStore(),
    () => {
        throw new Error("no reducer has been set");
    },
]);

export interface TempAction extends Action {
    type: "connect" | "setExternalIP" | "setManualIP" | "spawn" | "stop";
}
export type ClientStoreAction =
    | SetID
    | SetStreamID
    | UpdateBattery
    | TempAction;

export function reducer(
    state: ClientStore,
    action: ClientStoreAction,
): ClientStore {
    const newState = new ClientStore(
        state.localIP,
        state.externalIP,
        state.process,
        state.manualServerIP,
    );
    if (action instanceof UpdateBattery) {
        newState.battery = action.battery;
    } else if (action instanceof SetID) {
        newState.id = action.value;
        setID(newState.id);
        EmitInfo(newState.onInfo());
    } else if (action instanceof SetStreamID) {
        newState.streamID = action.id;
        EmitInfo(newState.onInfo());
    } else {
        switch (action.type) {
            case "setManualIP":
                newState.manualServerIP = action.payload;
                break;
            case "spawn":
                if (newState.process === undefined) {
                    const { cmd, args } = action.payload;
                    if (cmd !== "") {
                        log(`Spawning: ${JSON.stringify(action.payload)}`);
                        newState.process = spawn(cmd, args, {});
                        if (newState.process.pid === undefined) {
                            error(
                                `Could not create cmd ${cmd} with args [${args.join(
                                    ",",
                                )}]`,
                            );
                            newState.process = undefined;
                        }
                    }
                }
                EmitInfo(newState.onInfo());
                break;
            case "setExternalIP":
                newState.externalIP = action.payload;
                break;
            case "stop":
                if (newState.process !== undefined) {
                    try {
                        newState.process.on("error", (err: any) => {
                            error(err);
                        });
                        const { pid } = newState.process;
                        log(pid);
                        newState.process.kill("SIGTERM");
                        if (newState.process.killed) {
                            newState.process = undefined;
                        } else if (process.platform === "win32") {
                            const message = execSync(
                                `tasklist /FI "PID eq ${pid}"`,
                            ).toString();
                            if (message.match("No tasks are running")) {
                                newState.process = undefined;
                            }
                        }
                    } catch (error) {
                        error(error);
                    }
                }
                EmitInfo(newState.onInfo());
                break;
            case "connect":
                connect(`${action.payload}`);
                EmitInfo(newState.onInfo());
                break;
        }
    }
    return newState;
}
