import batteryLevel from "battery-level";
import React, { useContext, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import {
    ClientID,
    ipcOff,
    ipcOn,
    ipcSendAsync,
    IPString,
    StatusBox,
    Title,
    VersionNumber,
} from "vr-launcher-common";

import { SetStreamID, UpdateBattery } from "actions";
import { ClientContext } from "ClientApp/ClientStore";
import { ClientStoreProvider } from "ClientApp/ClientStoreProvider";
import { ExternalIP } from "ClientApp/ExternalIP";
import { offStreamID, onStreamID } from "ClientApp/mainWindowIPC";
import { ManualConnect } from "ClientApp/ManualConnect";
import { PlayerSelection } from "ClientApp/PlayerSelection";
import { Search } from "ClientApp/Search";
import { ipcRenderer } from "electron";
import { Logger } from "Logger";
import packageJson from "package.json";
import { close, EmitInfo, getServerIP, set, unset, Uri } from "socket";

export function ClientApp(): JSX.Element {
    return (
        <ClientStoreProvider>
            <ClientMain />
        </ClientStoreProvider>
    );
}

export function ClientMain(): JSX.Element {
    const [state, dispatch] = useContext(ClientContext);
    const classes = ["text-center"];
    const onStream = (event: Event, id: string) => {
        const source = `${ClientMain.name}.${onStream.name}`;
        dispatch(new SetStreamID(source, id));
    };

    useEffect(() => {
        let source = `${ClientApp.name}.useEffect`;

        set("get info", () => {
            EmitInfo(state.onInfo());
        });
        set(
            "start",
            (payload: { cmd: string; args: string[]; id: ClientID }) => {
                console.log({ event: "start", payload });
                if (payload.id === state.id) {
                    dispatch({ source, type: "spawn", payload });
                }
            },
        );
        set("stop", (id: ClientID) => {
            console.log({ event: "stop", id });
            if (id === state.id) {
                dispatch({ source, type: "stop" });
            }
        });
        onStreamID(onStream);
        set("get stream", () => {
            source = `${source}.onGetStream`;
            if (state.streamID !== undefined) {
                EmitInfo(state.onInfo());
            } else {
                ipcSendAsync("get stream", Uri());
            }
        });

        async function updateBatteryLevel() {
            source = `${source}.${updateBatteryLevel.name}`;
            const battery = await batteryLevel();
            if (state.battery !== battery) {
                const action = new UpdateBattery(source, battery);
                dispatch(action);
            }
        }
        updateBatteryLevel();

        return () => {
            unset("get info");
            // offStreamID(onStream);
            unset("get stream");
            unset("start");
            unset("stop");
            unset("disconnect");
        };
    });

    return (
        <Container className={classes.join(" ")}>
            <Title title="Launcher Client" />
            <Row>
                <Col>
                    <SystemStatus />
                    <ClientID />
                </Col>
                <PlayerSelection />
            </Row>
            <InternalIP />
            <ExternalIP />
            <ServerStatus />
            <ServerIP />
            <Search hide={false} interval={5000} />
            <ManualConnect />
            <VersionNumber version={packageJson.version} />
        </Container>
    );
}

function ClientID(): JSX.Element {
    const [state] = useContext(ClientContext);
    return (
        <StatusBox
            label={"client id".toUpperCase()}
            content={String(state.id)}
        />
    );
}

function SystemStatus(): JSX.Element {
    const [state] = useContext(ClientContext);
    return (
        <StatusBox
            label={"system status".toUpperCase()}
            content={state.status().toUpperCase()}
        />
    );
}

function ServerIP(): JSX.Element {
    return <IPString label="Server" ip={getServerIP()} />;
}

function InternalIP(): JSX.Element {
    const state = useContext(ClientContext)[0];
    return <IPString label="Internal" ip={state.localIP} />;
}

function ServerStatus(): JSX.Element {
    const state = useContext(ClientContext)[0];

    return (
        <Row>
            <Col>
                <StatusBox
                    label={"server status".toUpperCase()}
                    content={state.conn}
                />
            </Col>
        </Row>
    );
}
