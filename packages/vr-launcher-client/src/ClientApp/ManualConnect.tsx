import React, { useContext } from "react";
import { Button, Form, FormControlProps, Row } from "react-bootstrap";
import { BsPrefixProps, ReplaceProps } from "react-bootstrap/helpers";

import { ClientContext, ClientStoreAction } from "ClientApp/ClientStore";
import { port } from "socket";

interface ManualConnectProps {
    disabled?: boolean;
    placeholder?: string;
}

/**
 * Allows the user to manually connect to a server.
 */
export function ManualConnect(props: ManualConnectProps): JSX.Element {
    let source = ManualConnect.name;
    const [state, dispatch] = useContext(ClientContext);
    function onChange(
        event: React.FormEvent<
            ReplaceProps<"input", BsPrefixProps<"input"> & FormControlProps>
        >,
    ) {
        source = `${source}.onChange`;
        // @ts-ignore
        const payload = event.target.value;
        const action: ClientStoreAction = {
            payload,
            source,
            type: "setManualIP",
        };
        dispatch(action);
    }
    function onClick() {
        source = `${source}.${onClick.name}`;
        if (state.manualServerIP === "") {
            return;
        }
        let payload = `ws://${state.manualServerIP.replace(/^\w*:\/\//, "")}`;
        if (!payload.match(/:\d*$/g)) {
            payload = `${payload}:${port()}`;
        }
        const action: ClientStoreAction = {
            payload,
            source,
            type: "connect",
        };
        dispatch(action);
    }
    return (
        <Row>
            <Form.Control
                type="text"
                value={state.manualServerIP}
                onChange={onChange}
                disabled={!!props.disabled}
            />
            <Button variant="primary" type="submit" onClick={onClick}>
                Manual Connect
            </Button>
        </Row>
    );
}
