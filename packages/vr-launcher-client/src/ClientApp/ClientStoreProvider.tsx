import React, { useReducer } from "react";

import { ClientContext, ClientStore, reducer } from "ClientApp/ClientStore";

export function ClientStoreProvider(props: { children: any }): JSX.Element {
    const [state, dispatch] = useReducer(reducer, new ClientStore());
    return (
        <ClientContext.Provider value={[state, dispatch]}>
            {props.children}
        </ClientContext.Provider>
    );
}
