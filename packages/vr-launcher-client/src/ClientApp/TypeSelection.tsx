import React, { useContext } from "react";
import { Col, Dropdown, DropdownButton, Row } from "react-bootstrap";
import { SelectCallback } from "vr-launcher-common";

import { ClientContext } from "ClientApp/ClientStore";

export interface TypeSelectionProps {
    title: string;
    options: Array<string | number>;
    onSelect?: SelectCallback;
}

export function TypeSelection(props: TypeSelectionProps): JSX.Element {
    const [state, dispatch] = useContext(ClientContext);
    const source = TypeSelection.name;
    const size = "lg";
    const m = 1;

    const menuItems = props.options.map((value, index) => {
        const id = props.title.toLowerCase().replace(/ /g, "-");
        return (
            <Dropdown.Item
                href={`#/${id}-${value}`}
                key={index}
                onSelect={props.onSelect}
            >
                {value}
            </Dropdown.Item>
        );
    });

    return (
        <>
            <Row>
                <Col>Select User Type</Col>
            </Row>
            <Row>
                <Col>
                    <DropdownButton id="player-select" size="lg" title="Player">
                        {menuItems}
                    </DropdownButton>
                </Col>
            </Row>
        </>
    );
}
