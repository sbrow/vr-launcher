import React, { useContext } from "react";
import { Col } from "react-bootstrap";
import { ClientID } from "vr-launcher-common";

import { SetID } from "actions";
import { ClientContext } from "ClientApp/ClientStore";
import { TypeSelection } from "ClientApp/TypeSelection";

export function PlayerSelection(): JSX.Element {
    const dispatch = useContext(ClientContext)[1];
    const onSelect = (
        eventKey: string,
        event: React.SyntheticEvent<{}, Event>,
    ) => {
        const source = "onSelect";
        const match = eventKey.match(/[^-]*$/);
        console.debug(match);
        if (match !== null) {
            const toID = (value: string): ClientID => {
                switch (value) {
                    case "SERVER":
                        return "SERVER";
                    case "2":
                        return "2";
                    case "3":
                        return "3";
                    case "4":
                        return "4";
                    default:
                        return "1";
                }
            };
            const payload = toID(match[0]);
            const action = new SetID(source, payload);
            console.log(`caught ${JSON.stringify(action)}`);
            dispatch(action);
        }
    };
    return (
        <Col>
            <TypeSelection
                title="Player"
                options={["SERVER", 1, 2, 3, 4]}
                onSelect={onSelect}
            />
        </Col>
    );
}
