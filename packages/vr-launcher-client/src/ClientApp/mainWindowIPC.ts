import { Event } from "electron";
import { ipcOff, ipcOn } from "vr-launcher-common";
let bound = false;

export function onStreamID(callback: (event: Event, id: string) => void) {
    if (!bound) {
        // @ts-ignore
        ipcOn("stream id", callback);
        bound = true;
    }
}

export function offStreamID(callback: (event: Event, id: string) => void) {
    // @ts-ignore
    ipcOff("stream id", callback);
    // bound = false;
}
