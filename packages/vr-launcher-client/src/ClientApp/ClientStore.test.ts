// tslint:disable no-relative-imports

import { source } from "testUtils";
import { SetStreamID } from "../actions";
import { ClientStore, reducer } from "./ClientStore";

jest.mock("../multicast", () => ({}));
let state: ClientStore = new ClientStore();

beforeEach(() => {
    state = new ClientStore();
});

describe("reducer", () => {
    describe("SetStreamID", () => {
        it("should update streamID", () => {
            const action = new SetStreamID(source, "some-id-001");
            expect(action instanceof SetStreamID).toBe(true);
            const want = action.id;
            const got = reducer(state, action).streamID;
            expect(got).toEqual(want);
        });
    });
});
