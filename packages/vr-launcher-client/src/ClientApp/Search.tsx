import React, { useContext, useEffect } from "react";
import { Button, Col, Row } from "react-bootstrap";

import { ClientContext } from "ClientApp/ClientStore";
import { startSearching, stopSearching } from "searchForServer";
import { findServer } from "server";
import { ConnStatus } from "socket";

const defaultInterval = 1000;

export function Search(
    props: { hide: boolean; interval?: number } = {
        hide: false,
        interval: defaultInterval,
    },
): JSX.Element {
    const [state, dispatch] = useContext(ClientContext);
    function onClick() {
        const address = findServer();
        if (process.env.NODE_ENV !== "production") {
            let message = `No server found`;
            if (address !== "") {
                message = `server found at '${address}'`;
            }
            // tslint:disable-next-line: no-console
            console.log(message);
        }
        if (address !== "" && ConnStatus() === "Not connected to server") {
            dispatch({ source: "onClick", type: "connect", payload: address });
        }
    }
    useEffect(() => {
        switch (ConnStatus()) {
            case "Not connected to server":
                startSearching(onClick, props.interval || defaultInterval);
                break;
            case "Connecting...":
            case "Connected":
                stopSearching();
        }
        return () => {
            stopSearching();
        };
    }, [state.conn]);
    if (props && props.hide) {
        return <></>;
    }
    return (
        <Row>
            <Col>
                <Button onClick={onClick}>Search for Server</Button>
            </Col>
        </Row>
    );
}
