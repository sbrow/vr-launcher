import { app, BrowserWindow, BrowserWindowConstructorOptions } from "electron";
import logger from "electron-log";
import { autoUpdater } from "electron-updater";

import "IPCManager";
import { Logger } from "Logger";

autoUpdater.logger = logger;

export let mainWindow: BrowserWindow;
export let streamingWindow: BrowserWindow;

function onReady() {
    autoUpdater.checkForUpdatesAndNotify();
    const opts: BrowserWindowConstructorOptions = {
        width: 640,
        height: 480,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: true,
        },
    };
    mainWindow = new BrowserWindow(opts);

    const fileName = `file://${__dirname}/index.html`;
    mainWindow.loadURL(fileName);
    mainWindow.on("close", () => app.quit());
    /*
    streamingWindow = new BrowserWindow({
        show: false,
        webPreferences: {
            nodeIntegration: true,
        },
    });
    streamingWindow.loadURL(`file://${__dirname}/stream/index.html`);
    */
}

app.on("ready", onReady);
app.on("window-all-closed", () => app.quit());
Logger.info(`Electron Version ${app.getVersion()}`);
