import ip from "ip";
import { Multicast } from "vr-launcher-common";

Multicast.socket.bind(undefined, ip.address());

export let server: string = "";
let _address: string = "";

export function clearServer(): void {
    server = "";
}

export function Client(addr: string) {
    _address = addr;
    const listening = () => {
        Multicast.socket.addMembership(_address, ip.address());
        const address = Multicast.socket.address();
        const addr =
            typeof address === "string"
                ? address
                : `${address.address}:${address.port}`;
        console.log(`UDP socket listening on ${addr} pid: ${process.pid}`);
    };

    const onMessage = (message: string, rinfo: any) => {
        console.info(
            `Message from: ${rinfo.address}:${rinfo.port} - ${message}`,
        );
        try {
            const messageObject = JSON.parse(message);
            if ("type" in messageObject && messageObject.type === "response") {
                console.log(`got: ${message}`);
                server = messageObject.uri;
            }
        } catch (error) {
            console.error(error);
        }
    };
    Multicast.socket.on("listening", listening);
    Multicast.socket.on("message", onMessage);
}

Client(Multicast.address);

export function getServer(): string {
    if (server === "") {
        search();
    }
    return server;
}

export function search(): void {
    Multicast.send({ type: "request" });
}
