import { Action } from "vr-launcher-common";

export class UpdateBatteryAction implements Action {
    public source: string;
    public type = "updateBattery";
    public battery: number | undefined;
    constructor(source: string, battery?: number) {
        this.source = source;
        this.battery = battery;
    }
}
