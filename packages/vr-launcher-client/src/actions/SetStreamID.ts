import { Action } from "vr-launcher-common";

export class SetStreamID implements Action {
    public source: string;
    public type: "setStreamID";
    public id: string | undefined;

    constructor(source: string, id?: string) {
        this.source = source;
        this.type = "setStreamID";
        this.id = id;
    }
}
