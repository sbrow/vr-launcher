import { Action, ClientID } from "vr-launcher-common";

export class SetIDAction implements Action {
    public source: string;
    public type = "setID";
    public value: ClientID;

    constructor(source: string, value: ClientID = "1") {
        this.source = source;
        this.value = value;
    }
}
