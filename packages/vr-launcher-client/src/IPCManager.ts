import { Event, ipcMain } from "electron";
import { mainWindow, streamingWindow } from "electron-main";

ipcMain.on("get stream", (event: Event, uri: URL) => {
    if (streamingWindow !== undefined) {
        streamingWindow.webContents.send("start stream", uri);
    }
});

ipcMain.on("stream id", (event: Event, id: string) => {
    mainWindow.webContents.send("stream id", id);
});
