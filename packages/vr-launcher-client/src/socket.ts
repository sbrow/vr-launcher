/**
 * Contains the API for interacting with the front-end's Socket.io client.
 */

import sendKeys from "sendkeys";
import io from "socket.io-client";
import P2P from "socket.io-p2p";
import { ClientID, CommonEvent, Info } from "vr-launcher-common";

export type SocketEvent = CommonEvent | "disconnect";

/**
 * @returns the port that socket.io should connect to.
 */
export function port(): string {
    return (
        process.env.npm_config_port ||
        process.env.npm_package_config_port ||
        process.env.PORT ||
        "3000"
    );
}

/**
 * The Socket.io connection to the backend.
 */
let socket: SocketIOClient.Socket = io();
let p2p: P2P;

export function set(event: SocketEvent, callback: (...args: any) => void) {
    unset(event);
    socket.on(event, callback);
}

export function unset(event: SocketEvent) {
    socket.off(event);
}

export function connect(
    uri: string,
    opts?: SocketIOClient.ConnectOpts,
    forceNew: boolean = true,
) {
    if (forceNew || !connected()) {
        socket = io(uri, opts);
        socket.open();
    }
    socket.off("sendKeys");
    socket.on("sendKeys", (keys: string) => sendKeys(keys));
}

export function connected() {
    return socket.connected;
}
export function close() {
    socket.io.uri = "file://";
    socket.close();
}

export function getServerIP(): string {
    if (socket.connected) {
        return socket.io.uri.replace(/(ws|http):\/\//, "").replace(/\/.*$/, "");
    }
    return "";
}

export function ConnStatus() {
    if (socket.connected) {
        return "Connected";
    }
    const address = socket.io.uri;
    if (!address.match(/^(file:\/\/|$)/g)) {
        return "Connecting...";
    }
    return "Not connected to server";
}

export function Uri(): string {
    return socket.io.uri;
}

/**
 * @param data The info to emit.
 */
export function EmitInfo(data: Info): void {
    if (process.env.NODE_ENV !== "production") {
        console.log(`emitting ${JSON.stringify(data)}`);
    }
    if (socket.connected) {
        socket.emit("info", data);
    }
}

export function EmitStop(id?: ClientID): void {
    if (process.env.NODE_ENV !== "production") {
        console.log("Emitting 'stop'");
    }
    if (socket.connected) {
        socket.emit("stop", id);
    }
}

export function EmitGetInfo(): void {
    if (socket.connected) {
        socket.emit("get info");
    }
}
