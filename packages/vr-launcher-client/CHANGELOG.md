# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://github.com/sbrow/vr-launcher-client/compare/v2.0.0...v2.1.0) (2019-07-13)


### Features

* Temporarily disabled streaming ([6f8411b](https://github.com/sbrow/vr-launcher-client/commit/6f8411b))



## [2.0.0](https://github.com/sbrow/vr-launcher-client/compare/v1.0.0...v2.0.0) (2019-06-29)


### Bug Fixes

* **ClientApp:** hotfix for `onStreamID` memory leak. ([aaa23af](https://github.com/sbrow/vr-launcher-client/commit/aaa23af))
* **ExternalIP:** No longer throws errors when ip cannot be found. ([483796d](https://github.com/sbrow/vr-launcher-client/commit/483796d))
* **ExternalIP:** Shows external ip again... ([2332823](https://github.com/sbrow/vr-launcher-client/commit/2332823))
* **multicast:** No more unnecessary messages. ([a9d02ef](https://github.com/sbrow/vr-launcher-client/commit/a9d02ef))
* **p2psocket:** socket now connects to server URI, instead of `localhost`. ([79d8b46](https://github.com/sbrow/vr-launcher-client/commit/79d8b46))
* **p2psocket.ts:** Now asks for correct source id on Mac OS. ([24180cf](https://github.com/sbrow/vr-launcher-client/commit/24180cf))
* **package.json:** Fixed `config.commitizen.path` to work with yarn workspaces. ([edd3a46](https://github.com/sbrow/vr-launcher-client/commit/edd3a46))
* **SetIDAction:** Added imports from "vr-launcher-common". ([7d24291](https://github.com/sbrow/vr-launcher-client/commit/7d24291))
* Added prerelease script to build source. ([b3467cc](https://github.com/sbrow/vr-launcher-client/commit/b3467cc))
* Updated type definitions to pull from `vr-launcher-common`. ([1039060](https://github.com/sbrow/vr-launcher-client/commit/1039060))


### Build System

* Added `prettier`. ([481202a](https://github.com/sbrow/vr-launcher-client/commit/481202a))
* Updated `vr-launcher-common` to `^3.5.0`. ([0c448fe](https://github.com/sbrow/vr-launcher-client/commit/0c448fe))
* **package.json:** Moved `vr-launcher-common` to devDependencies. ([a9541b1](https://github.com/sbrow/vr-launcher-client/commit/a9541b1))
* **socket.ts:** Removed unused import. ([aa8dfd5](https://github.com/sbrow/vr-launcher-client/commit/aa8dfd5))
* **Theme:** Updated to themes from npm. ([3051a99](https://github.com/sbrow/vr-launcher-client/commit/3051a99))
* **tsconfig.json:** Updated target to `es2017`. ([9f7e6ae](https://github.com/sbrow/vr-launcher-client/commit/9f7e6ae))
* **vr-launcher-common:** Updated to `^1.0.0`. ([b0617bf](https://github.com/sbrow/vr-launcher-client/commit/b0617bf))
* Updated `vr-launcher-common` to npm package. ([28d5e53](https://github.com/sbrow/vr-launcher-client/commit/28d5e53))


### Features

* **p2psocket:** Now pulls port number from config. ([0c03804](https://github.com/sbrow/vr-launcher-client/commit/0c03804))
* **p2psockets:** Now retrieves only primary monitor, and at `1280x720` resolution. ([191e46a](https://github.com/sbrow/vr-launcher-client/commit/191e46a))
* **socket:** Added `Uri` function that returns the address of the server. ([c834af8](https://github.com/sbrow/vr-launcher-client/commit/c834af8))
* **socket:** Client can now passthrough key presses from socket connection. ([7e4841d](https://github.com/sbrow/vr-launcher-client/commit/7e4841d))
* Client now sends video data to server. ([ef26770](https://github.com/sbrow/vr-launcher-client/commit/ef26770))
* Now emits "streamID" in info packets. ([1d05fca](https://github.com/sbrow/vr-launcher-client/commit/1d05fca))


### Tests

* Added `SetStreamID` test. ([9275744](https://github.com/sbrow/vr-launcher-client/commit/9275744))
* Moved test for `ClientStore` next to its implementation. ([d582291](https://github.com/sbrow/vr-launcher-client/commit/d582291))
* **ClientStore:** Added test for `reducer(SetStreamID)`. ([d88ada2](https://github.com/sbrow/vr-launcher-client/commit/d88ada2))


### BREAKING CHANGES

* Server no longer hosts `/clients` namespace, instead uses default `/`.



## [1.0.0](https://github.com/sbrow/vr-launcher-client/compare/v0.24.2...v1.0.0) (2019-06-12)


### Features

* **package.json:** Renamed "pub" script to "release". ([b71a3df](https://github.com/sbrow/vr-launcher-client/commit/b71a3df))
* Added preversion script. ([54ac15d](https://github.com/sbrow/vr-launcher-client/commit/54ac15d))



### [0.24.2](https://github.com/sbrow/vr-launcher-client/compare/v0.24.1...v0.24.2) (2019-06-12)


### Bug Fixes

* Changed package name. ([cea4f61](https://github.com/sbrow/vr-launcher-client/commit/cea4f61))
