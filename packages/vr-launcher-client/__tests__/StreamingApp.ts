const electronPath = require.resolve("electron");
import { join } from "path";
import { Application } from "spectron";

const port = Math.floor(Math.random() * (9999 - 9000) + 9000);
const app = new Application({
    path: electronPath,
    args: [join(__dirname, "..", "app", "electron.js")],
    chromeDriverArgs: [`remote-debugging-port=${port}`],
});

describe("StreamingApp", () => {
    it.skip("does things", async done => {
        try {
            await app.start();
            await app.browserWindow.isVisible();
            const URI = "http://localhost:3003";

            app.electron.ipcRenderer.send("start stream", URI);
            app.electron.ipcMain.on(
                "start stream",
                (event: Event, uri: string) => {
                    expect(uri).toBe(URI);
                    done();
                },
            );
        } catch (err) {
            done(err);
        }
        // app.electron.ipcRenderer.on(
        //     "stream id",
        //     async (event: Event, id: string | undefined) => {
        //         expect(id).toBeDefined();
        //         await app.stop();
        //         done();
        //     },
        // );
    }, 30000);
});
