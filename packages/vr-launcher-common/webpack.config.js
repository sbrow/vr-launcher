// @ts-check
// tslint:disable no-console

const chalk = require("chalk");
const nodeExternals = require("webpack-node-externals");
const path = require("path");
const webpack = require("webpack");

process.env.NODE_ENV = process.env.NODE_ENV || "development";
const mode = process.env.NODE_ENV;

console.log(`NODE_ENV=${chalk.yellow(process.env.NODE_ENV)}`);
console.log(`Building for ${chalk.yellow(mode)}...`);

const electronConfig = {
    target: "node",
    externals: [nodeExternals()],
    mode,
    node: {
        __dirname: false,
    },
    module: {
        rules: [
            {
                exclude: [/node_modules/, /build/, /dist/],
                test: /\.tsx?$/,
                use: "ts-loader",
            },
            {
                test: /\.less$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "less-loader",
                ],
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"],
            },
        ],
    },
    plugins: [
        new webpack.EnvironmentPlugin(["NODE_ENV"]),
    ],
    resolve: {
        plugins: [],
        extensions: [".js", ".ts", ".tsx", ".jsx", ".json"],
    },
};

const libraryName = "vr-launcher-common";
const rootDir = path.resolve(__dirname);

module.exports = //[
    {
        ...electronConfig,
        plugins: [
            ...electronConfig.plugins,
        ],
        entry: {
            "common": path.join(__dirname, "src", "index.ts"),
        },
        output: {
            path: path.resolve(__dirname, "lib"),
            filename: "[name].js",
            library: libraryName,
            libraryTarget: "umd",
        },
    };
// ];
