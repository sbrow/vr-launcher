import { sendAsync, send, SendRequest, SendResponse } from "./ipc";
import { ipcRenderer } from "electron";
import { CommonEvent } from "./events";

global.process.type = "renderer";

jest.mock("electron", () => ({
    ipcRenderer: {
        once: jest.fn(),
        send: jest.fn(),
    },
}));

afterEach(() => {
    jest.clearAllMocks();
});

describe("send", () => {
    const request: SendRequest = {
        event: "info",
        args: ["one", 2],
    };
    describe("When response is undefined", () => {
        it("throws error", () => {
            expect(() => send(request)).toThrow();
        });
    });
    describe("When response is defined", () => {
        const response: SendResponse = {
            event: "get info",
            callback: (event: Event, ...args: any) => {},
        };

        it("binds request", () => {
            send(request, response);
            expect(ipcRenderer.once).toBeCalledTimes(1);
            expect(ipcRenderer.once).toBeCalledWith(
                response.event,
                response.callback,
            );
        });
        it("sends request", () => {
            send(request.event, response);
            expect(ipcRenderer.send).toBeCalledTimes(1);
            expect(ipcRenderer.send).toBeCalledWith(request.event, undefined);
        });
        it("sends arguments", () => {
            send(request, response);
            expect(ipcRenderer.send).toBeCalledTimes(1);
            expect(ipcRenderer.send).toBeCalledWith(
                request.event,
                ...request.args,
            );
        });
    });
});

describe("sendAsync", () => {
    describe("multiple arguments", () => {
        it("doesn't send as array", () => {
            const args: [CommonEvent, ...any[]] = ["info", "one", 2];
            const [event, ...cmdArgs] = args;
            sendAsync(event, ...cmdArgs);
            expect(ipcRenderer.send).toBeCalledTimes(1);
            expect(ipcRenderer.send).toBeCalledWith(...args);
        });
    });
});
