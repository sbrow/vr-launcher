export type ClientStatus = "disconnected" | "connected" | "ready" | "running";
export type ClientID = "SERVER" | "1" | "2" | "3" | "4"