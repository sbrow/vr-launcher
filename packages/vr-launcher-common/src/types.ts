export interface Action {
    source: string;
    type: string;
    payload?: any;
}