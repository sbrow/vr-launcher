import { Event, ipcRenderer } from "electron";
import { CommonEvent } from "./events";

export type SendRequest =
    | CommonEvent
    | {
          event: CommonEvent;
          args: any;
      };

export interface SendResponse {
    event: CommonEvent;
    callback: (event: Event, ...args: any) => any;
}

/**
 * Handles message sending to and from the backend,
 * regardless of whether Electron or NW.js is being used.
 */
function runningElectron(): boolean {
    if ("process" in window && window.process && "type" in window.process) {
        return window.process.type === "renderer";
    }
    return false;
}

/**
 * Sends a synchronous event.
 *
 * @remarks
 * Like an RPC call.
 *
 * @param event The type of event to send.
 * @param args Arguments to send for that particular event.
 */
export function sendSync(event: CommonEvent, args?: any): any {
    if (runningElectron()) {
        return ipcRenderer.sendSync(event, args);
    } else {
        if ("mainModule" in process && process.mainModule !== undefined) {
            if (event in process.mainModule.exports) {
                return process.mainModule.exports[event](args);
            }
        }
    }
}

/**
 * Sends an asynchronous event.
 *
 * @remarks
 * The results of the call are ignored.
 *
 * @param event The type of event to send.
 * @param args Arguments to send for that particular event.
 */
export function sendAsync(event: CommonEvent, ...args: any) {
    if (runningElectron()) {
        ipcRenderer.send(event, ...args);
    }
}

/**
 * binds event listener `response.callback` to `response.event` (once only),
 * then emits `request`.
 *
 * @remarks
 * Non-blocking.
 *
 * @param request The event to emit.
 * @param response The event and callback to fire.
 */
export function send(request: SendRequest, response?: SendResponse): any {
    const { event, args } =
        typeof request === "object"
            ? request
            : { event: request, args: undefined };
    if (runningElectron()) {
        if (response === undefined) {
            throw new Error("Response cannot be undefined when using electron");
        }
        ipcRenderer.once(response.event, response.callback);
        ipcRenderer.send(event, ...args);
    } else {
        if (response === undefined) {
            return sendSync(event, ...args);
        }
    }
}

export function set(
    event: CommonEvent,
    callback: (event: Event, ...args: any) => any,
): any {
    if (runningElectron()) {
        ipcRenderer.on(event, callback);
        console.log(`set ${event}`);
    }
}

export function unset(
    event: CommonEvent,
    callback?: (event: Event, ...args: any) => any,
): void {
    if (runningElectron()) {
        if (callback !== undefined) {
            ipcRenderer.removeListener(event, callback);
        } else {
            ipcRenderer.removeAllListeners(event);
        }
    }
}
