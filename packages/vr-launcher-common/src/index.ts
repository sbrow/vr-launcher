import { MULTICAST_ADDR, PORT, sendMessage, socket } from "./multicast";

export const Multicast = {
    address: MULTICAST_ADDR,
    port: PORT,
    send: sendMessage,
    socket,
};

export * from "./Clients";
export * from "./components";
export * from "./events";
export * from "./getStream";
export * from "./types";

export {
    set as ipcOn,
    unset as ipcOff,
    sendAsync as ipcSendAsync,
    sendSync as ipcSendSync,
    send as ipcSend,
} from "./ipc";
