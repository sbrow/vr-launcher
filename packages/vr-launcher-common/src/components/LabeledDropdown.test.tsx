import React from "react";
import { LabeledDropdown } from "./LabeledDropdown";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

const options = ["title", "option"];
const label = "label";

describe("LabeledDropdown", () => {
    it("label points to DropdownButton", () => {
        const { getByLabelText } = render(
            <LabeledDropdown
                title={options[0]}
                options={options}
                label={label}
            />,
        );
        expect(getByLabelText(label)).toMatchInlineSnapshot(`
            <button
              aria-expanded="false"
              aria-haspopup="true"
              class="dropdown-toggle btn btn-primary"
              id="label"
              role="menu"
              type="button"
            >
              title
            </button>
        `);
    });
});
