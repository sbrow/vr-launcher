import Octicon, { ChevronLeft } from "@githubprimer/octicons-react";
import React from "react";
import { Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";

function Back(props: { history: any; children?: any }) {
  const children =
    props.children === undefined ? (
      <Octicon icon={ChevronLeft} />
    ) : (
      props.children
    );

  return <Button onClick={props.history.goBack}>{...children}</Button>;
}

export const BackButton = withRouter(Back);
