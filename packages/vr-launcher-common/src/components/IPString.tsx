import React from "react";
import { Col, Row } from "react-bootstrap";

export function IPString(props: {
    label: string;
    ip: string;
}): JSX.Element {
    return (<Row>
        <Col className="text-justify-right text-right">{props.label} IP Address:</Col>
        <Col className="text-justify-left text-left">{props.ip}</Col>
    </Row>);
}
