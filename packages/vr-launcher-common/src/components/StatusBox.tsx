import React from "react";
import { Col } from "react-bootstrap";

export interface StatusBoxProps {
    label: string;
    content: string;
}

export function StatusBox(props: StatusBoxProps): JSX.Element {
    return (
        <React.Fragment>
            <Col>{props.label}</Col>
            <Col className="border">{props.content}</Col>
        </React.Fragment>
    );
}
