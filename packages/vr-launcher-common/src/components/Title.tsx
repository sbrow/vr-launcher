import React from "react";
import { Col, Row } from "react-bootstrap";

export interface TitleProps {
    className?: string;
    title: string;
    row?: boolean;
    col?: boolean;
}

export function Title(props: TitleProps): JSX.Element {
    let render = <h3 className={props.className}>{props.title}</h3>;

    if (props.col === true) {
        render = <Col>{render}</Col>;
    }
    if (props.row === true) {
        render = <Row>{render}</Row>;
    }

    return render;
}
