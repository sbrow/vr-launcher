import React from "react";
import { Button, Col, Row } from "react-bootstrap";

export function Games(): JSX.Element {
    const size = "lg";
    const margin = 4;
    return (<Row>
        <Col>
            <Button className={`m-${margin}`} size={size}>Game 1</Button>
            <Button className={`m-${margin}`} size={size}>Game 2</Button>
        </Col>
    </Row>);
}
