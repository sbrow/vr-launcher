import Octicon, { Icon } from "@githubprimer/octicons-react";
import React, { ReactElement, ReactNode } from "react";
import { Col, Dropdown, Row, ColProps, ButtonProps } from "react-bootstrap";

export type SelectCallback = (
    eventKey: string,
    event: React.SyntheticEvent<{}, Event>,
) => void;

export interface LabeledDropdownProps {
    title: string | number;
    options: (string | number)[];
    labelSizes?: ColProps;
    dropdownSizes?: ColProps;
    children?: ReactNode;
    className?: string;
    /**
     * the `id` tag to use for the Dropdown button.
     */
    id?: string;
    label?: string;
    icon?: Icon;
    variant?: ButtonProps["variant"];
    onSelect?: SelectCallback;
}

export function LabeledDropdown(props: LabeledDropdownProps): JSX.Element {
    const className = `${props.className} d-flex align-items-center`.trim();
    const variant = props.variant || "primary";

    function getId(): string {
        if (props.id) {
            return props.id;
        }
        if (props.label) {
            return props.label.toLowerCase().replace(/ /g, "-");
        }
        if (props.icon) {
            return props.icon.name;
        }
        return "";
    }
    const id = getId();
    const menuItems = props.options.map((value, index) => {
        return (
            <Dropdown.Item
                eventKey={`${id}-${value}`}
                key={index}
                onSelect={props.onSelect}
                role="menuitem"
            >
                {value}
            </Dropdown.Item>
        );
    });

    let label: ReactElement | undefined;
    if (props.label) {
        label = <label htmlFor={id}>{props.label}</label>;
    }
    if (props.icon) {
        label = (
            <>
                <Octicon icon={props.icon} size={"medium"} />
                {label}
            </>
        );
    }
    if (props.children) {
        label = (
            <>
                {props.children}
                {label}
            </>
        );
    }
    return (
        <Row className={className}>
            <Col className="text-right" {...props.labelSizes}>
                {label}
            </Col>
            <Col className="text-left" {...props.dropdownSizes}>
                <Dropdown>
                    <Dropdown.Toggle id={id} variant={variant} role="menu">
                        {`${props.title}`}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>{menuItems}</Dropdown.Menu>
                </Dropdown>
            </Col>
        </Row>
    );
}
