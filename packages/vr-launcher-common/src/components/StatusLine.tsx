import React from "react";

import { BatteryStatus } from "./BatteryStatus";
import { ClientStatus } from "../Clients";

export function StatusLine(props: { status: ClientStatus; battery?: number; }): JSX.Element {
    if (props.battery !== undefined) {
        return (
            <span>
                {props.status} (<BatteryStatus battery={props.battery} />)
        </span>
        );
    } else {
        return <span>{props.status}</span>;
    }
}
