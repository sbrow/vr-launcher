import React from "react";
import { Col, Navbar } from "react-bootstrap";

export interface VersionNumberProps {
    version: string;
    className?: string;
}

export function VersionNumber(props: VersionNumberProps): JSX.Element {
    const className = `${props.className || ""} text-right`.trimLeft();
    return (
        <Navbar className="fixed-bottom">
            <Col className={className}>{`v${props.version}`}</Col>
        </Navbar>
    );
}
