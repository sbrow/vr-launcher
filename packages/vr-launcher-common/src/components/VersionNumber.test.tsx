import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { VersionNumber } from "./VersionNumber";

const version = "1.0.0";

describe("VersionNumber", () => {
    it("renders correctly", () => {
        const { getByText } = render(<VersionNumber version={version} />);
        expect(getByText(`v${version}`)).toHaveClass("text-right col");
    });
});
