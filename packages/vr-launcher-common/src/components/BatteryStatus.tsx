import React from "react";

export function BatteryStatus(props: { battery?: number; }): JSX.Element {
    const percentage: number | "?" = (props.battery)
        ? Math.round(props.battery * 100)
        : "?";
    const textColor = (percentage !== "?" && percentage <= 0.25) ? "text-danger" : "text-success";
    return <span className={textColor}>{percentage}%</span>;
}
