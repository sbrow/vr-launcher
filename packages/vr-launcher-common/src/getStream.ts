import { desktopCapturer } from "electron";

export async function getStream(): Promise<MediaStream | undefined> {
    const a = await desktopCapturer.getSources({ types: ["screen"] });
    const stream = await navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
            // @ts-ignore
            mandatory: {
                chromeMediaSource: "desktop",
            },
        },
    });
    return stream;
}
