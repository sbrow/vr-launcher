import * as dgram from "dgram";

export const PORT = 20000;
export const MULTICAST_ADDR = "239.255.0.255";

export const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

export function sendMessage(msg: any, rInfo?: NodeJS.RInfo) {
    if (typeof msg !== "string") {
        msg = JSON.stringify(msg);
    }
    const message = Buffer.from(msg);
    const address = (rInfo) ? rInfo.address : MULTICAST_ADDR;
    const port: number = (rInfo) ? Number(rInfo.port) : PORT;

    socket.send(message, 0, message.length, port, address, () => {
        console.info(`Sent to ${address}:${port} -> message "${message}"`);
    });
}

socket.on("error", (err: Error) => {
    console.error(`Socket encountered error: ${err}`);
});

socket.on("connect", () => {
    console.error(`Socket successfully connected`);
});
