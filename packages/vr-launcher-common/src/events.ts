import { ClientID, ClientStatus } from "./Clients";

export type CommonEvent = "disconnect" |
    "games" |
    "games.request" |
    "games.response" |
    "get info" |
    "get stream" |
    "info" |
    "refresh" |
    "start" |
    "stop" |
    "stream";

/**
 * @deprecated
 */
export class Start {
    public static readonly type: CommonEvent = "start";
    public name: string = "game_1";
    public id: ClientID = "SERVER";
    public keys: { [key: string]: string } = {};

    constructor(props?: { name: string, keys: { [key: string]: string }, id: ClientID }) {
        if (props !== undefined) {
            this.name = props.name;
            this.id = props.id;
            this.keys = props.keys;
        }
    }

    public toArgs(): [CommonEvent, Start] {
        return [Start.type, { ...this }];
    }
}


export interface Info {
    localIP: string;
    id: ClientID;
    status: ClientStatus;
    battery?: number;
    streamID?: string;
}