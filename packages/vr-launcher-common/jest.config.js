module.exports = {
    testPathIgnorePatterns: ["node_modules", "lib"],
    transform: {
        ".(ts|tsx)$": "ts-jest",
    },
};
