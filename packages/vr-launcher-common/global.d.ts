declare interface Window {
    process?: {
        type?: string;
    }
}

declare namespace NodeJS {
    interface RInfo {
        address: string;
        family: string;
        port: string;
        size: string;
    }
}