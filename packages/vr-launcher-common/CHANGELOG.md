# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.5.0](https://github.com/sbrow/vr-launcher-common/compare/v3.4.0...v3.5.0) (2019-06-28)


### Bug Fixes

* **ipc:** `sendAsync` now parses args as expected. ([afcce60](https://github.com/sbrow/vr-launcher-common/commit/afcce60))


### Build System

* Now runs tests before building. ([ebe8eae](https://github.com/sbrow/vr-launcher-common/commit/ebe8eae))


### Features

* **ipc:** Send can now accept arguments in requests. ([4ddfdd4](https://github.com/sbrow/vr-launcher-common/commit/4ddfdd4))



## [3.4.0](https://github.com/sbrow/vr-launcher-common/compare/v3.3.0...v3.4.0) (2019-06-27)


### Bug Fixes

* **LabeledDropdown:** `<label>` tag now only appears when `props.label !== undefined`. ([5b573be](https://github.com/sbrow/vr-launcher-common/commit/5b573be))


### Features

* **VersionNumber:** Added `className` prop, to pass classes to `.col`. ([9bfa479](https://github.com/sbrow/vr-launcher-common/commit/9bfa479))



## [3.3.0](https://github.com/sbrow/vr-launcher-common/compare/v3.2.0...v3.3.0) (2019-06-27)


### Build System

* deletes test files after build. ([26fea10](https://github.com/sbrow/vr-launcher-common/commit/26fea10))


### Features

* **LabeledDropdown:** Added "role" attributes. ([6c9f37e](https://github.com/sbrow/vr-launcher-common/commit/6c9f37e))
* **LabeledDropdown:** Added "variant" prop. ([4bd20dd](https://github.com/sbrow/vr-launcher-common/commit/4bd20dd))
* **LabeledDropdown:** Added `<label>` tag around label. ([e3e1338](https://github.com/sbrow/vr-launcher-common/commit/e3e1338))
* **LabeledDropdown:** Added `id` prop. ([5b7bc58](https://github.com/sbrow/vr-launcher-common/commit/5b7bc58))


### Tests

* **LabeledDropdown:** Created. ([aa447df](https://github.com/sbrow/vr-launcher-common/commit/aa447df))



## [3.2.0](https://github.com/sbrow/vr-launcher-common/compare/v3.1.1...v3.2.0) (2019-06-26)


### Build System

* Installed prettier. ([5663e24](https://github.com/sbrow/vr-launcher-common/commit/5663e24))


### Features

* **LabeledDropdown:** Now accept sizes for both columns, child labels and row classes. ([411a225](https://github.com/sbrow/vr-launcher-common/commit/411a225))



### [3.1.1](https://github.com/sbrow/vr-launcher-common/compare/v3.1.0...v3.1.1) (2019-06-24)


### Bug Fixes

* **BackButton:** Should work as stated in previous version. ([fa87bae](https://github.com/sbrow/vr-launcher-common/commit/fa87bae))



## [3.1.0](https://github.com/sbrow/vr-launcher-common/compare/v3.0.0...v3.1.0) (2019-06-24)


### Features

* **BackButton:** Now accepts child props to replace Back icon ([60fd1e2](https://github.com/sbrow/vr-launcher-common/commit/60fd1e2))



## [3.0.0](https://github.com/sbrow/vr-launcher-common/compare/v2.0.0...v3.0.0) (2019-06-19)


### Build System

* **commitizen:** Updated dependencies and config. ([ca36ca7](https://github.com/sbrow/vr-launcher-common/commit/ca36ca7))


### Features

* **CommonEvent:** Added "disconnect" event. ([204fada](https://github.com/sbrow/vr-launcher-common/commit/204fada))


### refactor

* **Clients:** Removed 'Clients' class. ([57e8f68](https://github.com/sbrow/vr-launcher-common/commit/57e8f68))
* **Clients:** Removed `Client`. ([ec4201f](https://github.com/sbrow/vr-launcher-common/commit/ec4201f))


### BREAKING CHANGES

* **Clients:** `Client` is no longer part of `vr-launcher-common`. use `vr-launcher-server`'s
implementation instead.
* **Clients:** `Clients` class no longer exists. Use `Clients` defined in `vr-launcher-server`
instead.



## [2.0.0](https://github.com/sbrow/vr-launcher-common/compare/v1.1.1...v2.0.0) (2019-06-17)


### Features

* **Info:** Now contains `streamID?: string` property. ([7a08482](https://github.com/sbrow/vr-launcher-common/commit/7a08482))


### refactor

* **Info:** Changed type of `Info.status` from `string` to `ClientID`. ([68c6408](https://github.com/sbrow/vr-launcher-common/commit/68c6408))


### BREAKING CHANGES

* **Info:** `Info.status` is now a `ClientID` instead of a `string`.



### [1.1.1](https://github.com/sbrow/vr-launcher-common/compare/v1.1.0...v1.1.1) (2019-06-13)


### Bug Fixes

* **events:** Added "games.response". ([687d2d4](https://github.com/sbrow/vr-launcher-common/commit/687d2d4))


### Build System

* **scripts:** Removed "publish" script. ([784279c](https://github.com/sbrow/vr-launcher-common/commit/784279c))



## [1.1.0](https://github.com/sbrow/vr-launcher-common/compare/v1.0.0...v1.1.0) (2019-06-13)


### Features

* **events:** Added "games.request", "get info", "get stream", and "stream". ([1bc1fd5](https://github.com/sbrow/vr-launcher-common/commit/1bc1fd5))



## [1.0.0](https://github.com/sbrow/vr-launcher-common/compare/v0.2.1...v1.0.0) (2019-06-13)


### Build System

* **scripts:** Added "publish" ([601c46d](https://github.com/sbrow/vr-launcher-common/commit/601c46d))


### Features

* Added 'getStream' function ([4371832](https://github.com/sbrow/vr-launcher-common/commit/4371832))


### refactor

* **ipc:** Renamed exports ([65c2511](https://github.com/sbrow/vr-launcher-common/commit/65c2511))
* **Type:** Renamed to "CommonEvent". ([12a3aa3](https://github.com/sbrow/vr-launcher-common/commit/12a3aa3))


### BREAKING CHANGES

* **ipc:** All `Ipc*` functions have been renamed `ipc*`.
* **Type:** `Type` is now `CommonEvent` and is a **Union type**, not an **enum**.



### [0.2.1](https://github.com/sbrow/vr-launcher-common/compare/v0.2.0...v0.2.1) (2019-06-12)


### Build System

* **lib:** Removed built code from repo. ([314f34a](https://github.com/sbrow/vr-launcher-common/commit/314f34a))
* **scripts:** Added "clean:all". ([4088507](https://github.com/sbrow/vr-launcher-common/commit/4088507))



## [0.2.0](https://github.com/sbrow/vr-launcher-common/compare/v0.1.0...v0.2.0) (2019-06-12)


### Bug Fixes

* **package.json:** Fixed "files" to include everything it should. ([f16458c](https://github.com/sbrow/vr-launcher-common/commit/f16458c))
* Forgot to push build files from last commit. ([9d58ba5](https://github.com/sbrow/vr-launcher-common/commit/9d58ba5))
* See previous commit. ([6cb9a66](https://github.com/sbrow/vr-launcher-common/commit/6cb9a66))
* **.gitignore:** removed `/lib` ([a39f02b](https://github.com/sbrow/vr-launcher-common/commit/a39f02b))
* ***d.t.s:** now work correctly when imported from npm. ([309e75f](https://github.com/sbrow/vr-launcher-common/commit/309e75f))
* **components:** Exported previously unexported modules. ([c87d0bd](https://github.com/sbrow/vr-launcher-common/commit/c87d0bd))
* **package.json:** Fixed "main" ([4f98762](https://github.com/sbrow/vr-launcher-common/commit/4f98762))
* **webpack.config.js:** Now exports as a library. ([76a76ce](https://github.com/sbrow/vr-launcher-common/commit/76a76ce))


### Build System

* Removed unused code ([cb24ea7](https://github.com/sbrow/vr-launcher-common/commit/cb24ea7))
* Trying without webpack. ([a862207](https://github.com/sbrow/vr-launcher-common/commit/a862207))


### Features

* **scripts:** Added "version" script ([29d51b3](https://github.com/sbrow/vr-launcher-common/commit/29d51b3))
* Updated types. ([c9af92c](https://github.com/sbrow/vr-launcher-common/commit/c9af92c))
* **LabeledDropdown:** Added option for icon based labels. ([b7ce5c7](https://github.com/sbrow/vr-launcher-common/commit/b7ce5c7))
* **LabeledDropdown:** Changed variant to "primary" ([df50dca](https://github.com/sbrow/vr-launcher-common/commit/df50dca))
* **LabeledDropdown:** Now aligns Dropdown left. ([afec32a](https://github.com/sbrow/vr-launcher-common/commit/afec32a))
* Added commitizen ([925dc30](https://github.com/sbrow/vr-launcher-common/commit/925dc30))
* Added standard version, switch build to tsc ([2e6b535](https://github.com/sbrow/vr-launcher-common/commit/2e6b535))
* **tsconfig.json:** No longer accepts absolute paths. ([df70f1d](https://github.com/sbrow/vr-launcher-common/commit/df70f1d))
