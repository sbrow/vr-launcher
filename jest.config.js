const packageDir = `<rootDir>/packages`;
module.exports = {
  collectCoverage: true,
  projects: [`${packageDir}/*/jest.config.js`]
};
