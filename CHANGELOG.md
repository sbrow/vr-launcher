# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.25.0](https://github.com/sbrow/game-client/compare/v0.24.0...v0.25.0) (2019-07-18)


### Bug Fixes

* **.gitignore:** removed `/lib` ([366d31f](https://github.com/sbrow/game-client/commit/366d31f))
* ***d.t.s:** now work correctly when imported from npm. ([22c0712](https://github.com/sbrow/game-client/commit/22c0712))
* **BackButton:** Should work as stated in previous version. ([14c7e4b](https://github.com/sbrow/game-client/commit/14c7e4b))
* **ClientApp:** hotfix for `onStreamID` memory leak. ([aa013c2](https://github.com/sbrow/game-client/commit/aa013c2))
* **components:** Exported previously unexported modules. ([5080d3f](https://github.com/sbrow/game-client/commit/5080d3f))
* **events:** Added "games.response". ([4e71145](https://github.com/sbrow/game-client/commit/4e71145))
* **ExternalIP:** No longer throws errors when ip cannot be found. ([53b2c1b](https://github.com/sbrow/game-client/commit/53b2c1b))
* **ExternalIP:** Shows external ip again... ([0fc04c4](https://github.com/sbrow/game-client/commit/0fc04c4))
* **ipc:** `sendAsync` now parses args as expected. ([e4bbeed](https://github.com/sbrow/game-client/commit/e4bbeed))
* **LabeledDropdown:** `<label>` tag now only appears when `props.label !== undefined`. ([ad0ef28](https://github.com/sbrow/game-client/commit/ad0ef28))
* **multicast:** No more unnecessary messages. ([b98ea5a](https://github.com/sbrow/game-client/commit/b98ea5a))
* **p2psocket:** socket now connects to server URI, instead of `localhost`. ([58a6f7f](https://github.com/sbrow/game-client/commit/58a6f7f))
* **p2psocket.ts:** Now asks for correct source id on Mac OS. ([7d2da3d](https://github.com/sbrow/game-client/commit/7d2da3d))
* **package.json:** Fixed "files" to include everything it should. ([1b8429d](https://github.com/sbrow/game-client/commit/1b8429d))
* **package.json:** Fixed "main" ([f9af7b9](https://github.com/sbrow/game-client/commit/f9af7b9))
* **package.json:** Fixed `config.commitizen.path` to work with yarn workspaces. ([d082264](https://github.com/sbrow/game-client/commit/d082264))
* **SetIDAction:** Added imports from "vr-launcher-common". ([9dff03c](https://github.com/sbrow/game-client/commit/9dff03c))
* Added prerelease script to build source. ([c09a1c5](https://github.com/sbrow/game-client/commit/c09a1c5))
* Changed package name. ([067eeda](https://github.com/sbrow/game-client/commit/067eeda))
* Dependency issues. ([b101bf5](https://github.com/sbrow/game-client/commit/b101bf5))
* Forgot to push build files from last commit. ([9ca3772](https://github.com/sbrow/game-client/commit/9ca3772))
* See previous commit. ([9487abc](https://github.com/sbrow/game-client/commit/9487abc))
* Updated imports / types. ([b259890](https://github.com/sbrow/game-client/commit/b259890))
* Updated type definitions to pull from `vr-launcher-common`. ([9f9a704](https://github.com/sbrow/game-client/commit/9f9a704))
* **scripts/prebuild:** Removed server-specific command. ([7d2914c](https://github.com/sbrow/game-client/commit/7d2914c))
* **webpack.config.js:** Added `nodeExternals`. ([2ad8c89](https://github.com/sbrow/game-client/commit/2ad8c89))
* **webpack.config.js:** Corrected paths. ([269e9b3](https://github.com/sbrow/game-client/commit/269e9b3))
* Updated imports to omit `client/` ([a60169d](https://github.com/sbrow/game-client/commit/a60169d))
* **webpack.config.js:** Now exports as a library. ([aba7425](https://github.com/sbrow/game-client/commit/aba7425))


### Build System

* **npm:** Updated `jest-dom`. ([dcab06f](https://github.com/sbrow/game-client/commit/dcab06f))
* **scripts:** renamed `server-exe` to `server-exec`. ([60488d7](https://github.com/sbrow/game-client/commit/60488d7))
* Added `prettier`. ([c47b6e5](https://github.com/sbrow/game-client/commit/c47b6e5))
* Added missing `spectron` dependency. ([6fd6442](https://github.com/sbrow/game-client/commit/6fd6442))
* deletes test files after build. ([9226f06](https://github.com/sbrow/game-client/commit/9226f06))
* Installed prettier. ([efad09b](https://github.com/sbrow/game-client/commit/efad09b))
* Now runs tests before building. ([18d591c](https://github.com/sbrow/game-client/commit/18d591c))
* Updated `jest-dom` dependency ([464f5c2](https://github.com/sbrow/game-client/commit/464f5c2))
* Updated `vr-launcher-common` to `^3.5.0`. Needed to split streaming to separate window. ([cd62512](https://github.com/sbrow/game-client/commit/cd62512))
* **commitizen:** Updated dependencies and config. ([9f89e3e](https://github.com/sbrow/game-client/commit/9f89e3e))
* **lib:** Removed built code from repo. ([80464fe](https://github.com/sbrow/game-client/commit/80464fe))
* **package.json:** Moved `vr-launcher-common` to devDependencies. ([3903a4c](https://github.com/sbrow/game-client/commit/3903a4c))
* **scripts:** Added "clean:all". ([afc642a](https://github.com/sbrow/game-client/commit/afc642a))
* **scripts:** Added "publish" ([94371ae](https://github.com/sbrow/game-client/commit/94371ae))
* **scripts:** Added new scripts. ([f735f7c](https://github.com/sbrow/game-client/commit/f735f7c))
* **scripts:** Removed "publish" script. ([07baa26](https://github.com/sbrow/game-client/commit/07baa26))
* **socket.ts:** Removed unused import. ([c4765ee](https://github.com/sbrow/game-client/commit/c4765ee))
* **Theme:** Updated to themes from npm. ([8efba50](https://github.com/sbrow/game-client/commit/8efba50))
* **tsconfig.json:** Updated target to `es2017`. ([1e38c8b](https://github.com/sbrow/game-client/commit/1e38c8b))
* **vr-launcher-common:** Updated to `^1.0.0`. ([a7cd4a5](https://github.com/sbrow/game-client/commit/a7cd4a5))
* Added missing packages. ([802d969](https://github.com/sbrow/game-client/commit/802d969))
* Removed unused code ([3ac0baf](https://github.com/sbrow/game-client/commit/3ac0baf))
* Trying without webpack. ([17172c9](https://github.com/sbrow/game-client/commit/17172c9))
* Updated `vr-launcher-common` to npm package. ([33dee23](https://github.com/sbrow/game-client/commit/33dee23))


### Features

* Temporarily disabled streaming ([0fcbbcd](https://github.com/sbrow/game-client/commit/0fcbbcd))
* **BackButton:** Now accepts child props to replace Back icon ([dc3a915](https://github.com/sbrow/game-client/commit/dc3a915))
* **CommonEvent:** Added "disconnect" event. ([f38dad8](https://github.com/sbrow/game-client/commit/f38dad8))
* **events:** Added "games.request", "get info", "get stream", and "stream". ([8da513b](https://github.com/sbrow/game-client/commit/8da513b))
* **Info:** Now contains `streamID?: string` property. ([cb5e6d7](https://github.com/sbrow/game-client/commit/cb5e6d7))
* **ipc:** Send can now accept arguments in requests. ([7ec3a0c](https://github.com/sbrow/game-client/commit/7ec3a0c))
* **LabeledDropdown:** Added "role" attributes. ([fb928c6](https://github.com/sbrow/game-client/commit/fb928c6))
* **LabeledDropdown:** Added "variant" prop. ([e5b2e6f](https://github.com/sbrow/game-client/commit/e5b2e6f))
* **LabeledDropdown:** Added `<label>` tag around label. ([248c127](https://github.com/sbrow/game-client/commit/248c127))
* **LabeledDropdown:** Added `id` prop. ([e9b67ba](https://github.com/sbrow/game-client/commit/e9b67ba))
* **LabeledDropdown:** Added option for icon based labels. ([5afdc56](https://github.com/sbrow/game-client/commit/5afdc56))
* **LabeledDropdown:** Changed variant to "primary" ([3ae6e57](https://github.com/sbrow/game-client/commit/3ae6e57))
* **LabeledDropdown:** Now accept sizes for both columns, child labels and row classes. ([4af5019](https://github.com/sbrow/game-client/commit/4af5019))
* **LabeledDropdown:** Now aligns Dropdown left. ([df70a15](https://github.com/sbrow/game-client/commit/df70a15))
* **p2psocket:** Now pulls port number from config. ([3424059](https://github.com/sbrow/game-client/commit/3424059))
* **p2psockets:** Now retrieves only primary monitor, and at `1280x720` resolution. ([ee93e40](https://github.com/sbrow/game-client/commit/ee93e40))
* **package.json:** Renamed "pub" script to "release". ([94d3223](https://github.com/sbrow/game-client/commit/94d3223))
* **scripts:** Added "version" script ([459d78a](https://github.com/sbrow/game-client/commit/459d78a))
* **scripts/clean:** Now removes `client-info.json`. ([5f18973](https://github.com/sbrow/game-client/commit/5f18973))
* **socket:** Added `Uri` function that returns the address of the server. ([b22f257](https://github.com/sbrow/game-client/commit/b22f257))
* **socket:** Client can now passthrough key presses from socket connection. ([9985aa5](https://github.com/sbrow/game-client/commit/9985aa5))
* **VersionNumber:** Added `className` prop, to pass classes to `.col`. ([6a3f289](https://github.com/sbrow/game-client/commit/6a3f289))
* Added 'getStream' function ([6ecca26](https://github.com/sbrow/game-client/commit/6ecca26))
* Added commitizen ([c1b8801](https://github.com/sbrow/game-client/commit/c1b8801))
* Added preversion script. ([26d7da2](https://github.com/sbrow/game-client/commit/26d7da2))
* Added standard version, switch build to tsc ([03af7f4](https://github.com/sbrow/game-client/commit/03af7f4))
* Client now sends video data to server. ([8a1d892](https://github.com/sbrow/game-client/commit/8a1d892))
* Now emits "streamID" in info packets. ([4ed4600](https://github.com/sbrow/game-client/commit/4ed4600))
* Updated types. ([cae70c0](https://github.com/sbrow/game-client/commit/cae70c0))
* **tsconfig.json:** No longer accepts absolute paths. ([7396e13](https://github.com/sbrow/game-client/commit/7396e13))


### refactor

* **Clients:** Removed 'Clients' class. ([28ca1e5](https://github.com/sbrow/game-client/commit/28ca1e5))
* **Clients:** Removed `Client`. ([c64f7eb](https://github.com/sbrow/game-client/commit/c64f7eb))
* **Info:** Changed type of `Info.status` from `string` to `ClientID`. ([a1f80f8](https://github.com/sbrow/game-client/commit/a1f80f8))
* **ipc:** Renamed exports ([9142488](https://github.com/sbrow/game-client/commit/9142488))
* **Type:** Renamed to "CommonEvent". ([d3f113c](https://github.com/sbrow/game-client/commit/d3f113c))


### Tests

* Added `SetStreamID` test. ([e9c9d08](https://github.com/sbrow/game-client/commit/e9c9d08))
* Improved `jest.config.js`. ([29a75c2](https://github.com/sbrow/game-client/commit/29a75c2))
* Moved test for `ClientStore` next to its implementation. ([175bc6f](https://github.com/sbrow/game-client/commit/175bc6f))
* Skipped extraneous test. ([84f9b3f](https://github.com/sbrow/game-client/commit/84f9b3f))
* **ClientStore:** Added test for `reducer(SetStreamID)`. ([79dbd46](https://github.com/sbrow/game-client/commit/79dbd46))
* **LabeledDropdown:** Created. ([1ed7865](https://github.com/sbrow/game-client/commit/1ed7865))


### BREAKING CHANGES

* **Clients:** `Client` is no longer part of `vr-launcher-common`. use `vr-launcher-server`'s
implementation instead.
* **Clients:** `Clients` class no longer exists. Use `Clients` defined in `vr-launcher-server`
instead.
* **Info:** `Info.status` is now a `ClientID` instead of a `string`.
* Server no longer hosts `/clients` namespace, instead uses default `/`.
* **ipc:** All `Ipc*` functions have been renamed `ipc*`.
* **Type:** `Type` is now `CommonEvent` and is a **Union type**, not an **enum**.



## [0.24.0](https://github.com/sbrow/game-client/compare/v0.22.0...v0.24.0) (2019-07-16)


### Bug Fixes

* **Client:** Now searches for new server when original server goes down. ([510a51b](https://github.com/sbrow/game-client/commit/510a51b))
* **CustomCard:** Added battery status back to server card. ([35ccedf](https://github.com/sbrow/game-client/commit/35ccedf))
* **CustomCard:** No longer shows "ready" status when client is not active. ([464ccc1](https://github.com/sbrow/game-client/commit/464ccc1))
* **CustomCard:** set `<Card.text as="h1">` ([71cc9b7](https://github.com/sbrow/game-client/commit/71cc9b7))
* **ReactDevTools:** Added 7zip dependency to fix installation issues on Windows. ([539e343](https://github.com/sbrow/game-client/commit/539e343))
* **ServerApp:** Re-enabled "server" connection. ([2d65aa4](https://github.com/sbrow/game-client/commit/2d65aa4))
* **Start,Stop:** Fixed type of props.variant. ([a9d2efb](https://github.com/sbrow/game-client/commit/a9d2efb))
* Fixed imports. ([701a9f9](https://github.com/sbrow/game-client/commit/701a9f9))


### Build System

* **scripts:** Moved `version` to `version:all`. ([863e598](https://github.com/sbrow/game-client/commit/863e598))
* Added `common` script. ([43182ed](https://github.com/sbrow/game-client/commit/43182ed))
* Added `postinstall` script to run `prepare`. ([ed3b652](https://github.com/sbrow/game-client/commit/ed3b652))
* Added lerna. ([c6e757e](https://github.com/sbrow/game-client/commit/c6e757e))
* Updated dependencies. ([293fff5](https://github.com/sbrow/game-client/commit/293fff5))
* **package.json:** Fixed hoisting issues. ([078a956](https://github.com/sbrow/game-client/commit/078a956))
* **package.json:** Removed unused packages. ([37e2874](https://github.com/sbrow/game-client/commit/37e2874))
* **yarn.lock:** Updated `vr-launcher-common` version ([287c7c3](https://github.com/sbrow/game-client/commit/287c7c3))


### Features

* **scripts:** Added "build:w" command. ([340222d](https://github.com/sbrow/game-client/commit/340222d))
* Made into a workspace. ([b5b6f30](https://github.com/sbrow/game-client/commit/b5b6f30))
* Removed old files. ([47ec0bd](https://github.com/sbrow/game-client/commit/47ec0bd))
* **CustomCard:** Increased size of Player ID ([4411283](https://github.com/sbrow/game-client/commit/4411283))
* **CustomCard:** Now displays "Enter player name" above player names. ([52f4f0f](https://github.com/sbrow/game-client/commit/52f4f0f))
* **Start,Stop:** Added size and variant props. ([4e51745](https://github.com/sbrow/game-client/commit/4e51745))
* Added "icon" props for dropdowns. ([bfb360a](https://github.com/sbrow/game-client/commit/bfb360a))
* **package.json:** Added commitizen. ([c8366a3](https://github.com/sbrow/game-client/commit/c8366a3))
* **Server:** Goes fullscreen in production mode. ([39976c9](https://github.com/sbrow/game-client/commit/39976c9))
* **Server:** Now only loads React DevTools in development build. ([0d89b61](https://github.com/sbrow/game-client/commit/0d89b61))
