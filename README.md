# Installation

[![Greenkeeper badge](https://badges.greenkeeper.io/sbrow/vr-launcher.svg)](https://greenkeeper.io/)

Install [npm and node](https://nodejs.org/en/). Then, `cd` into the root directory and run:

```bash
$ npx yarn install
```

# Setup

From the root directory, run `$ yarn build && yarn start`, and you should be up and running.

# Workspaces

This repository is set up as a monorepo. It contains the following git submodules:

- `./client`: The client application.
- `./server`: The server application.
- `./common`: Code and type definitions shared by the client and server.

## File Structure

Within each submodule:

- `src`: Contains source files.
- `app`: Contains transpiled javascript, runnable in development mode.
- `dist`: Contains "distributable" files: Installers and a packaged electron app that is production ready.

**Note:** in `common`, `app` is renamed `lib`, and `dist` is not present.

## Scripts

[npm scripts](https://docs.npmjs.com/misc/scripts) are used as the as the build system for each package. You can find available scripts in the `"scripts"` key in each repo's `package.json` file.

scripts can be run from the commandline using:

```bash
$ yarn run <scriptname>
```

### Script Names

These scripts appear in each submodule:

- `build`: Compiles `.ts` files into runnnable `.js` files.
- `build:watch`: Run an incremental build.
- `start`: Runs the built application in development, (un-packaged), mode.
- `version`: Runs [standard-version](https://www.npmjs.com/package/standard-version) on the package, incrementing the version number according to [conventional commit](https://www.conventionalcommits.org/en/v1.0.0-beta.4/) specification, and updating CHANGELOG.md with recent changes.

These scripts appear in `client` and `server`:

- `build:production`\*: `build` simply transpiles the source files and concatenates them into one. `build:production` also minifies the code and performs dead code removal and tree shaking.  
  **Note:** Build errors are supressed when running `build:production`.
- `package`: bundles built code into an installer that can install the program on any Windows machine.
- `release`: cleans the entire project, reinstalls dependencies, runs `build:production` and `package`, and then uploads the installer to GitHub for [auto-update](https://www.electron.build/auto-update) functionality.

\* In `build:production`, build errors are suppressed.

### Root package Scripts

Most of the root scripts simply run themselves in each workspace directory, with some exceptions:

- `client`, `common`, and `server` will all forward their arguments to that particular project.

#### Example

Runing any of the following in the root directory will achieve the same result.

- `yarn client build`
- `cd ./client && yarn build`
- `yarn build --scope vr-launcher-common`

# Typescript

This project is written in [Typescript](https://www.typescriptlang.org/). If you're not familiar with it, don't worry. It's a superset of javascript you can write all of your code in javascript and it will work fine, just make sure to save your files with a `.ts` extension.

# React

This project uses [React Hooks](https://reactjs.org/docs/hooks-intro.html) to render DOM elements. Within the `src` folders, the programs are generally split into one folder per App (webpage).

Each App has a store which acts as it's controller for synchronous changes and its model; and a Render function which controls asynchronous changes and the view.

All store changes should be uni-directional, handled by passing `Action` interfaces into the Store's `reducer` method.

# Webpack

Examine the `entry` fields exported by `webpack.config.{ts,js}` files to find the entry files for the programs.

Generally, there is a frontend and a backend. The frontend runs in the `electron renderer` process, and the backend runs in the `electron main` process; read more [here](https://electronjs.org/docs/tutorial/application-architecture).

# Testing

You can move files ending in `.test.ts` from the `src` folder to a sibling `__tests__` folder if you don't want the tests next to what they're testing.
